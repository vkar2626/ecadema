package com.ecadema;

import android.text.InputFilter;
import android.util.Log;

public class EmojiFilter {
    public static InputFilter[] getFilter()
    {
        InputFilter EMOJI_FILTER = (source, start, end, dest, dstart, dend) -> {
            for (int index = start; index < end; index++) {
                int type = Character.getType(source.charAt(index));

                if (type == Character.SURROGATE
                        || type== Character.NON_SPACING_MARK
                        || type== Character.OTHER_SYMBOL
                        || type == Character.DECIMAL_DIGIT_NUMBER
                        || type == Character.CURRENCY_SYMBOL
                        || type == Character.MATH_SYMBOL) {
                    Log.e("Source", String.valueOf(source).replace(String.valueOf(source.charAt(index)),""));
                    return String.valueOf(source).replace(String.valueOf(source.charAt(index)),"");
                }

            }
            return null;
        };
        return new InputFilter[]{EMOJI_FILTER};
    }


}
