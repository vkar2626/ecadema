package com.ecadema;

import static com.android.volley.VolleyLog.TAG;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.IBinder;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.util.Log;

import androidx.annotation.Nullable;

import com.android.volley.VolleyError;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.PostMethod;
import com.ecadema.apiMethods.ResponseData;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import java.util.HashMap;
import java.util.Map;

public class MyService extends Service {

    boolean connected;
    Context mContext;
    SharedPreferences sharedPreferences;
    //ProgressDialogActivity progressDialogActivity;


    FusedLocationProviderClient mFusedLocationClient;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Let it continue running until it is stopped.
        Log.e(TAG, "onStartCommand");
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        try {
            if (mFusedLocationClient!=null) {
                mFusedLocationClient.removeLocationUpdates(locationCallback);
            }
            if (mFusedLocationClient==null)
                mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getApplicationContext());

            locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(60000);

            locationCallback = new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    if (locationResult == null) {
                        return;
                    }
                    for (Location location : locationResult.getLocations()) {
                     sendStatus();
                    }
                }
            };
            mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper());
        } catch (SecurityException | IllegalArgumentException ex) {
            ex.printStackTrace();
        }
        return START_STICKY;
    }


    private void sendStatus() {
        Map<String,String> param = new HashMap<>();
        param.put("user_id",sharedPreferences.getString("userID", ""));

        new PostMethod(Api.UpdateLastActivityOfUser,param,getApplicationContext()).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                //Log.e("userStatus",data);
            }

            @Override
            public void error(VolleyError error) {
                error.printStackTrace();
            }
        });
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mFusedLocationClient!=null)
            mFusedLocationClient.removeLocationUpdates(locationCallback);

        //Toast.makeText(this, "Service Destroyed", Toast.LENGTH_LONG).show();
    }

}