package com.ecadema.adapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ecadema.app.ChatRoom;
import com.ecadema.app.LoginActivity;
import com.ecadema.app.MainActivity;
import com.ecadema.app.R;
import com.ecadema.fragments.CalendarFragment;
import com.ecadema.fragments.ViewProfile;
import com.ecadema.modal.AllTrainersModal;
import com.ecadema.modal.CoursePriceModal;
import com.ecadema.modal.SessionModal;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

public class AllTrainersAdapter extends RecyclerView.Adapter<AllTrainersAdapter.RowHolder> {
    private final Context context;
    private final ArrayList<AllTrainersModal> allTrainersModalArrayList;
    private ArrayList<AllTrainersModal> filterArrayList;
    SharedPreferences sharedPreferences;
    Dialog dialog;
    ArrayList<SessionModal> sessionModalArrayList=new ArrayList<>();
    ArrayList<CoursePriceModal> coursePriceModalArrayList=new ArrayList<>();
    RecyclerView allSessions,priceRecyclerView;
    String courseID="",ticketPrice="";

    public AllTrainersAdapter(Context context, ArrayList<AllTrainersModal> allTrainersModalArrayList) {
        this.context=context;
        this.allTrainersModalArrayList = allTrainersModalArrayList;
        this.filterArrayList = new ArrayList<>();//initiate filter list
        this.filterArrayList.addAll(allTrainersModalArrayList);//add all items of array list to filter list
        context.registerReceiver(courseSelected,new IntentFilter("courseSelected"));
        sharedPreferences= PreferenceManager.getDefaultSharedPreferences(context);
    }

    BroadcastReceiver courseSelected=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                String coursePrice=intent.getStringExtra("coursePrice");
                Log.e("coursePrice",coursePrice);
                courseID=intent.getStringExtra("courseID");
                coursePriceModalArrayList.clear();
                JSONArray jsonArray=new JSONArray(coursePrice);
                for (int ar=0;ar<jsonArray.length();ar++){
                    JSONObject object=jsonArray.optJSONObject(ar);
                    CoursePriceModal coursePriceModal=new CoursePriceModal();
                    coursePriceModal.setHours(object.optString("hours")+context.getResources().getString(R.string.hours));
                    coursePriceModal.setPrice("$ "+object.optString("price_per_hour")+" /"+context.getResources().getString(R.string.hour));

                    coursePriceModalArrayList.add(coursePriceModal);
                }
                priceRecyclerView.setAdapter(new CoursePriceAdapter(context,coursePriceModalArrayList));
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    };

    @NonNull
    @Override
    public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.trainers_adapter,null);
        return new RowHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(@NonNull RowHolder holder, int position) {
        AllTrainersModal allTrainersModal = allTrainersModalArrayList.get(position);

        holder.recyclerView.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));
        holder.recyclerView.setAdapter(new NestedAdapter(context,allTrainersModal.getTrainsFor()));

        holder.tagText.setText(allTrainersModal.getLabel());
        holder.name.setText(allTrainersModal.getName());
        holder.location.setText(allTrainersModal.getLocation());
        holder.languages.setText(allTrainersModal.getLanguage());
        holder.rate.setText("$ "+allTrainersModal.getRate()+"/"+context.getResources().getString(R.string.hour));
        holder.rating.setRating(allTrainersModal.getRating());
        Glide.with(context).load(allTrainersModal.getImage()).into(holder.profile_pic);

        if(allTrainersModal.getIsMentor().equalsIgnoreCase("1"))
            holder.mentor.setVisibility(View.VISIBLE);
        else
            holder.mentor.setVisibility(View.GONE);

        /*if(sharedPreferences.getString("user_type","").equalsIgnoreCase("2")
                ||sharedPreferences.getString("user_type","").equalsIgnoreCase("3"))
            holder.bookSessionCard.setVisibility(View.GONE);*/
        if(sharedPreferences.getString("user_type","").equalsIgnoreCase("3") &&
                allTrainersModal.getTeacherId().equalsIgnoreCase(sharedPreferences.getString("userID","")))
            holder.bookSessionCard.setVisibility(View.GONE);
        else
            holder.bookSessionCard.setVisibility(View.VISIBLE);

        if(allTrainersModal.getTeacherId().equalsIgnoreCase(sharedPreferences.getString("userID",""))) {
            holder.viewProfile.setVisibility(View.GONE);
            holder.chat.setVisibility(View.GONE);
        }
        else {
            holder.viewProfile.setVisibility(View.VISIBLE);
            holder.chat.setVisibility(View.VISIBLE);
        }

        holder.chat.setOnClickListener(v->{
            if (sharedPreferences.getString("userID","").equalsIgnoreCase("-1")){
                Intent intent = new Intent(context, LoginActivity.class);
                context.startActivity(intent);
                return;
            }
            Intent intent=new Intent(context, ChatRoom.class);
            intent.putExtra("chatID","");
            intent.putExtra("userName",allTrainersModal.getName());
            intent.putExtra("receiverID",allTrainersModal.getTeacherId());
            intent.putExtra("userImage",allTrainersModal.getImage());
            if(null!=allTrainersModal.getSessions())
                intent.putExtra("courses",allTrainersModal.getSessions().toString());
            context.startActivity(intent);

           /* Bundle bundle=new Bundle();
            bundle.putString("trainerID",allTrainersModal.getTeacherId());
            ViewProfile viewProfile=new ViewProfile();
            viewProfile.setArguments(bundle);
            ((MainActivity) context).addFragment2(viewProfile, true, false, 0);*/
        });
        holder.viewProfile.setOnClickListener(v->{
            Bundle bundle=new Bundle();
            bundle.putString("trainerID",allTrainersModal.getTeacherId());
            ViewProfile viewProfile=new ViewProfile();
            viewProfile.setArguments(bundle);
            ((MainActivity) context).addFragment2(viewProfile, true, false, 0);
        });
        holder.bookSessions.setOnClickListener(v->{
            if(sharedPreferences.getString("userID","").equalsIgnoreCase("-1")){
                Intent intent=new Intent(context, LoginActivity.class);
                context.startActivity(intent);
                return;
            }

            sessionModalArrayList.clear();
            androidx.appcompat.app.AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
            LayoutInflater inflater1 =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            @SuppressLint("InflateParams") final View dialogView =
                    inflater1.inflate(R.layout.course_dialog, null);
            alertDialog.setView(dialogView);
            alertDialog.setCancelable(false);
            dialog=alertDialog.create();
            allSessions = dialogView.findViewById(R.id.allSessions);
            priceRecyclerView = dialogView.findViewById(R.id.priceRecyclerView);
            TextView bookNow = dialogView.findViewById(R.id.bookNow);
            TextView title = dialogView.findViewById(R.id.title);

            title.setOnClickListener(v2->dialog.dismiss());
            allSessions.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));
            priceRecyclerView.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));
            allSessions.setAdapter(new SessionAdapter(context,sessionModalArrayList));

            try {
                for (int i=0;i<allTrainersModal.getSessions().length();i++){
                    JSONObject Object=allTrainersModal.getSessions().getJSONObject(i);
                    SessionModal sessionModal=new SessionModal();
                    sessionModal.setCourseId(Object.optString("id"));
                    sessionModal.setSubId(Object.optString("subject_id"));
                    sessionModal.setSubject(Object.optString("subject"));
                    sessionModal.setCoursePrices(""+Object.optJSONArray("course_price"));
                    sessionModalArrayList.add(sessionModal);

                    if(i==0){
                        ticketPrice=Object.optJSONArray("course_price").optJSONObject(0).optString("price_per_hour");
                        Intent intent=new Intent("courseSelected");
                        intent.putExtra("coursePrice", ""+Object.optJSONArray("course_price"));
                        intent.putExtra("courseID", Object.optString("id"));
                        context.sendBroadcast(intent);
                    }
                }

            }catch (Exception e){
                e.printStackTrace();
            }
            dialog.show();
            bookNow.setOnClickListener(v1->{
                dialog.dismiss();
                /*Intent intent = new Intent("Calendar");
                intent.putExtra("open","booking");
                intent.putExtra("name",allTrainersModal.getName());
                intent.putExtra("trainerID",allTrainersModal.getTeacherId());
                intent.putExtra("userImage",allTrainersModal.getImage());
                intent.putExtra("courseID",courseID);
                intent.putExtra("ticketPrice",ticketPrice);
                intent.putExtra("from","booking");
                context.sendBroadcast(intent);*/

                Bundle bundle = new Bundle();
                bundle.putString("trainerID", allTrainersModal.getTeacherId());
                bundle.putString("name", allTrainersModal.getName());
                bundle.putString("courseID", courseID);
                bundle.putString("imageurl", allTrainersModal.getImage());
                bundle.putString("ticketPrice", ticketPrice);
                bundle.putString("from", "booking");

                CalendarFragment calendarFragment = new CalendarFragment();
                calendarFragment.setArguments(bundle);
                FragmentManager fragmentManager = ((MainActivity) context).getSupportFragmentManager();
                FragmentTransaction ft = fragmentManager.beginTransaction();
                ft.replace(R.id.frag_container, calendarFragment, calendarFragment.getClass().getSimpleName());
                ft.addToBackStack(null);
                ft.commit();
            });
        });
    }

    @Override
    public int getItemCount() {
        return allTrainersModalArrayList.size();
    }

    public class RowHolder extends RecyclerView.ViewHolder {
        TextView name,location,languages,rate,viewProfile,bookSessions,tagText;
        RatingBar rating;
        RecyclerView recyclerView;
        CardView bookSessionCard,chat;
        ImageView profile_pic,mentor;
        public RowHolder(@NonNull View itemView) {
            super(itemView);

            tagText=itemView.findViewById(R.id.tagText);
            profile_pic=itemView.findViewById(R.id.profile_pic);
            mentor=itemView.findViewById(R.id.mentor);
            name=itemView.findViewById(R.id.name);
            location=itemView.findViewById(R.id.location);
            languages=itemView.findViewById(R.id.languages);
            rate=itemView.findViewById(R.id.rate);
            rating=itemView.findViewById(R.id.ratings);
            viewProfile=itemView.findViewById(R.id.viewProfile);
            bookSessions=itemView.findViewById(R.id.bookSessions);
            chat=itemView.findViewById(R.id.chat);
            recyclerView=itemView.findViewById(R.id.recyclerView);
            bookSessionCard=itemView.findViewById(R.id.bookSessionCard);

        }
    }

    public void filter(String charText) {

        Log.e("filters",charText);
        //If Filter type is NAME and EMAIL then only do lowercase, else in case of NUMBER no need to do lowercase because of number format
        charText = charText.toLowerCase(Locale.getDefault());
        allTrainersModalArrayList.clear();//Clear the main ArrayList

        //If search query is null or length is 0 then add all filterList items back to attachmentList
        if (charText.length() == 0) {
            allTrainersModalArrayList.addAll(filterArrayList);
        } else {

            //Else if search query is not null do a loop to all filterList items
            for (AllTrainersModal model : filterArrayList) {

                //Now check the type of search filter
                if(null==model.getGender()||model.getGender().equalsIgnoreCase("")||model.getGender().equalsIgnoreCase("null")){
                    if (model.getName().toLowerCase(Locale.getDefault()).startsWith(charText)||
                            model.getName().toLowerCase(Locale.getDefault()).contains(charText)||
                            model.getLanguage().toLowerCase(Locale.getDefault()).contains(charText)||
                            model.getLocation().toLowerCase(Locale.getDefault()).contains(charText))
                        allTrainersModalArrayList.add(model);
                }else {
                    if (model.getName().toLowerCase(Locale.getDefault()).startsWith(charText)||
                            model.getName().toLowerCase(Locale.getDefault()).contains(charText)||
                            model.getGender().toLowerCase(Locale.getDefault()).contains(charText)||
                            model.getLanguage().toLowerCase(Locale.getDefault()).contains(charText)||
                            model.getLocation().toLowerCase(Locale.getDefault()).contains(charText))
                        allTrainersModalArrayList.add(model);
                }
            }
        }
        notifyDataSetChanged();
    }
    public void rangeFilter(String charText) {

        //If Filter type is NAME and EMAIL then only do lowercase, else in case of NUMBER no need to do lowercase because of number format
        charText = charText.toLowerCase(Locale.getDefault());
        allTrainersModalArrayList.clear();//Clear the main ArrayList

        //If search query is null or length is 0 then add all filterList items back to attachmentList
        if (charText.length() == 0) {
            allTrainersModalArrayList.addAll(filterArrayList);
        } else {

            //Else if search query is not null do a loop to all filterList items
            for (AllTrainersModal model : filterArrayList) {

                //Now check the type of search filter
                if (Integer.parseInt(model.getRate())<Integer.parseInt(charText))
                    allTrainersModalArrayList.add(model);
            }
        }
        notifyDataSetChanged();
    }

}
