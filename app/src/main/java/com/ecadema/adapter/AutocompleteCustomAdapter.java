package com.ecadema.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.ecadema.app.R;
import com.ecadema.modal.AttendeesModal;

import java.util.ArrayList;
import java.util.List;

public class AutocompleteCustomAdapter extends ArrayAdapter<AttendeesModal> {

    private List<AttendeesModal> dataList;
    private Context mContext;
    private int itemLayout;

    public AutocompleteCustomAdapter(Context context, int resource, ArrayList<AttendeesModal> storeDataLst) {
        super(context, resource, storeDataLst);
        dataList = storeDataLst;
        mContext = context;
        itemLayout = resource;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public AttendeesModal getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public View getView(int position, View view, @NonNull ViewGroup parent) {

        if (view == null) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(itemLayout, parent, false);
        }

        TextView strName = view.findViewById(R.id.textView);
        strName.setText(getItem(position).getName());

        ImageView imageView=view.findViewById(R.id.imageView);
        if(getItem(position).getAttendance().equalsIgnoreCase("5")) {
            imageView.setVisibility(View.VISIBLE);
            imageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_baseline_check_24));
        }
        else if(getItem(position).getAttendance().equalsIgnoreCase("3"))
            imageView.setVisibility(View.GONE);
        else {
            imageView.setVisibility(View.VISIBLE);
            imageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_baseline_close_24));
        }


        return view;
    }
}
