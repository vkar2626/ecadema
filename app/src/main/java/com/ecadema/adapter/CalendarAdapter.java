package com.ecadema.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ecadema.app.R;
import com.ecadema.modal.TimeSlotModal;

import java.util.ArrayList;

public class CalendarAdapter extends RecyclerView.Adapter<CalendarAdapter.ViewHolder> {
    private final Context context;
    private final ArrayList<TimeSlotModal> timeSlotModalArrayList;
    private final ArrayList<String> selectedTimeDate;
    private final String from;

    LayoutInflater inflter;
    ArrayList<String> selectedTime = new ArrayList<>();
    String csv;
    private int selectedPosition=-1;

    public CalendarAdapter(Context context, ArrayList<TimeSlotModal> timeSlotModalArrayList, ArrayList<String> selectedTimeDate, String from) {
        this.context = context;
        this.timeSlotModalArrayList = timeSlotModalArrayList;
        this.selectedTimeDate = selectedTimeDate;
        this.from = from;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.timeslotlayout,null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        holder.tvslot.setText(timeSlotModalArrayList.get(position).getTime());

        Log.e("Position",""+position);
        Log.e("selectedPosition",""+selectedPosition);
        /**/
        final int finalPosition = position;

        if(selectedTimeDate.size()>0){
            for (int ar=0;ar<selectedTimeDate.size();ar++){
                if(selectedTimeDate.contains(timeSlotModalArrayList.get(position).getDate())){
                    holder.tvslot.setBackground(context.getResources().getDrawable(R.drawable.selected_bg));
                    holder.tvslot.setTextColor(context.getResources().getColor(R.color.white));
                    selectedTime.add(timeSlotModalArrayList.get(position).getDate());
                }
            }
        }

        if(!from.equalsIgnoreCase("booking")){
            if (selectedPosition == position) {
                holder.tvslot.setBackground(context.getResources().getDrawable(R.drawable.selected_bg));
                holder.tvslot.setTextColor(context.getResources().getColor(R.color.white));
            }
            else {
                holder.tvslot.setBackground(context.getResources().getDrawable(R.drawable.grey_border_layout));
                holder.tvslot.setTextColor(context.getResources().getColor(R.color.dark_grey));
            }
        }

        holder.tvslot.setOnClickListener(v->{
            selectedPosition= position;
            if(from.equalsIgnoreCase("booking")){
                if (selectedTime.contains(timeSlotModalArrayList.get(position).getDate())){
                    holder.tvslot.setBackground(context.getResources().getDrawable(R.drawable.grey_border_layout));
                    holder.tvslot.setTextColor(context.getResources().getColor(R.color.dark_grey));
                    selectedTime.remove(timeSlotModalArrayList.get(position).getDate());
                    csv = android.text.TextUtils.join(",", selectedTime);
                    Intent intent = new Intent("timeSlotArray");
                    intent.putExtra("type","remove");
                    intent.putExtra("selectedTime",timeSlotModalArrayList.get(position).getDate());
                    context.sendBroadcast(intent);
                    return;
                }
                selectedTime.add(timeSlotModalArrayList.get(position).getDate());
                holder.tvslot.setBackground(context.getResources().getDrawable(R.drawable.selected_bg));
                holder.tvslot.setTextColor(context.getResources().getColor(R.color.white));
                csv = android.text.TextUtils.join(",", selectedTime);
                Intent intent = new Intent("timeSlotArray");
                intent.putExtra("type","add");
                intent.putExtra("selectedTime",timeSlotModalArrayList.get(position).getDate());
                context.sendBroadcast(intent);
            }
            else {
                selectedTime.clear();
                selectedTime.add(timeSlotModalArrayList.get(position).getDate());
                notifyDataSetChanged();
                csv = android.text.TextUtils.join(",", selectedTime);
                Intent intent = new Intent("timeSlotArray");
                intent.putExtra("type","add");
                intent.putExtra("selectedTime",timeSlotModalArrayList.get(position).getDate());
                context.sendBroadcast(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return timeSlotModalArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvslot;
        LinearLayout ll;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ll= itemView.findViewById(R.id.ll);
            tvslot= itemView.findViewById(R.id.tvslot);
            ll.setVisibility(View.VISIBLE);
        }
    }
}
