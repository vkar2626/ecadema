package com.ecadema.adapter;

import static com.blankj.utilcode.util.ActivityUtils.startActivity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.URLUtil;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.androidbuts.multispinnerfilter.KeyPairBoolData;
import com.androidbuts.multispinnerfilter.MultiSpinnerSearch;
import com.bumptech.glide.Glide;
import com.ecadema.MultipartUtility;
import com.ecadema.RealPathUtil;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.GetMethod;
import com.ecadema.apiMethods.PostMethod;
import com.ecadema.apiMethods.ResponseData;
import com.ecadema.app.FilePath;
import com.ecadema.app.LoginActivity;
import com.ecadema.app.MainActivity;
import com.ecadema.app.R;
import com.ecadema.fragments.CommunityFragment;
import com.ecadema.fragments.ViewProfile;
import com.ecadema.modal.BlogModal;
import com.ecadema.modal.TagsModal;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;

import net.dankito.richtexteditor.android.RichTextEditor;
import net.dankito.richtexteditor.android.toolbar.AllCommandsEditorToolbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class CommunityAdapterNew extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context context;
    public final ArrayList<BlogModal> blogModalArrayList;
    private final CommunityFragment communityFragment;
    ArrayList<Integer> isSelected = new ArrayList<>();
    int lineCount=0,selectionType=-1;
    int selectedPosition = -1;
    int scrollToPosition = -1;
    File photoFile;
    SharedPreferences sharedPreferences;
    BlogModal postModal;
    Bundle bundle;
    String idArray [];
    ArrayList<TagsModal> tagsModalArrayList=new ArrayList<>();
    ArrayList<String> tagIds=new ArrayList<>();
    ArrayList<String> tagNames=new ArrayList<>();
    ArrayList<String> tagTempNames=new ArrayList<>();
    List<KeyPairBoolData> listArray1 = new ArrayList<>();

    ProgressDialog progressDialog;
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    public HeaderViewHolder headerViewHolder;
    String postID="", uploadedImage="";
    ItemViewHolder itemViewHolder;
    public ArrayList<BlogModal> filterArrayList;

    public CommunityAdapterNew(CommunityFragment communityFragment,Context context, ArrayList<BlogModal> blogModalArrayList) {
        this.context=context;
        this.blogModalArrayList=blogModalArrayList;
        this.communityFragment=communityFragment;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        this.filterArrayList = new ArrayList<>();//initiate filter list
        this.filterArrayList.addAll(blogModalArrayList);
    }

    @Override
    public int getItemViewType(int position) {
        Resources activityRes = context.getResources();
        Configuration activityConf = activityRes.getConfiguration();
        Locale newLocale = new Locale(sharedPreferences.getString("lang",""));
        activityConf.setLocale(newLocale);
        activityRes.updateConfiguration(activityConf, activityRes.getDisplayMetrics());

        Resources applicationRes = context.getResources();
        Configuration applicationConf = applicationRes.getConfiguration();
        applicationConf.setLocale(newLocale);
        applicationRes.updateConfiguration(applicationConf, applicationRes.getDisplayMetrics());

        if (sharedPreferences.getString("userID","").equalsIgnoreCase("-1")){
            return TYPE_ITEM;
        }
        if (sharedPreferences.getString("user_type","").equalsIgnoreCase("3")){
            if (position == 0) {
                return TYPE_HEADER;
            }else
                return TYPE_ITEM;

        }
        return TYPE_ITEM;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Resources res=context.getResources();
        DisplayMetrics dr=res.getDisplayMetrics();
        Configuration configuration=res.getConfiguration();
        if (Build.VERSION.SDK_INT >=Build.VERSION_CODES.JELLY_BEAN_MR1){
            configuration.setLocale(new Locale(sharedPreferences.getString("lang","").toLowerCase()));
        }
        else
        {
            configuration.locale=new Locale(sharedPreferences.getString("lang","").toLowerCase());
        }
        res.updateConfiguration(configuration,dr);
        if (viewType == TYPE_ITEM) {
            // Here Inflating your recyclerview item layout
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.community_adapter_new, parent, false);
            return new ItemViewHolder(itemView);
        } else if (viewType == TYPE_HEADER) {
            // Here Inflating your header view
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.community_header_layout, parent, false);
            return new HeaderViewHolder(itemView);
        }
        else return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        if (holder instanceof HeaderViewHolder){
            headerViewHolder = (HeaderViewHolder) holder;
            if (communityFragment.getUri()!=null) {
                if (selectionType==3){
                    Uri mImageCaptureUri =communityFragment.getUri();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = context.getContentResolver().query(mImageCaptureUri,
                            filePathColumn, null, null, null);
                    // Move to first row
                    cursor.moveToFirst();
                    String imgDecodableString ="";
                    cursor.close();

                    if (mImageCaptureUri.toString().startsWith("content://")) {
                        try {
                            cursor = context.getContentResolver().query(mImageCaptureUri,
                                    null, null, null, null);
                            if (cursor != null && cursor.moveToFirst()) {
                                imgDecodableString = cursor.getString(cursor.getColumnIndex(filePathColumn[0]));
                                photoFile= new File(imgDecodableString);
                            }
                        }catch (Exception e) {
                            try {
                                // SDK < API11
                                String realPath;
                                if (Build.VERSION.SDK_INT < 11)
                                    realPath = RealPathUtil.getRealPathFromURI_BelowAPI11(context, mImageCaptureUri);

                                    // SDK >= 11 && SDK < 19
                                else if (Build.VERSION.SDK_INT < 19)
                                    realPath = RealPathUtil.getRealPathFromURI_API11to18(context,mImageCaptureUri);

                                    // SDK > 19 (Android 4.4)
                                else
                                    realPath = RealPathUtil.getRealPathFromURI_API19(context,mImageCaptureUri);

                                Log.e("Real Path", "" + realPath);

                                String PdfPathHolder = null;
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                                    try {
                                        PdfPathHolder = FilePath.getPath(context, mImageCaptureUri);
                                    }catch (Exception ew){
                                        ew.printStackTrace();
                                    }
                                }
                                assert PdfPathHolder != null;

                                    photoFile = new File(realPath);

                            }
                            catch (Exception ew){
                                ew.printStackTrace();
                            }

                            //photoFile= new File(mImageCaptureUri.getPath());
                            e.printStackTrace();
                        }finally {
                            cursor.close();
                        }
                    }
                    else if (mImageCaptureUri.toString().startsWith("file://")) {
                        cursor = context.getContentResolver().query(mImageCaptureUri,
                                filePathColumn, null, null, null);
                        imgDecodableString = cursor.getString(cursor.getColumnIndex(filePathColumn[0]));
                        cursor.moveToFirst();
                        cursor.close();
                        photoFile= new File(imgDecodableString);
                    }
                }
                else if (selectionType==1)
                    photoFile = new File(communityFragment.getCameraFile());
            }

            try {
                Glide.with(context).load(sharedPreferences.getString("profile_image", ""))
                        .into(headerViewHolder.profile_pic);
            }
            catch (Exception e){
                e.printStackTrace();
            }
            if (sharedPreferences.getString("userID","").equalsIgnoreCase("-1")){
                headerViewHolder.headerLayout.setVisibility(View.GONE);
            }
            else {
                if (sharedPreferences.getString("user_type","").equalsIgnoreCase("3"))
                    headerViewHolder.headerLayout.setVisibility(View.VISIBLE);
            }

            bundle = communityFragment.getHeader();
            if (bundle!=null){
                if (!bundle.getString("postID").equalsIgnoreCase("")){
                    headerViewHolder.postLayout.setVisibility(View.VISIBLE);
                }
            }
            if (selectionType==-1)
                getTags();
            else
                headerViewHolder.fileName.setText(communityFragment.getFileName());

        }
        else if (holder instanceof ItemViewHolder){
            itemViewHolder = (ItemViewHolder) holder;
            if (sharedPreferences.getString("userID","").equalsIgnoreCase("-1")){
                postModal = blogModalArrayList.get(position);
                itemViewHolder.delete.setTag(position);
                itemViewHolder.edit.setTag(position);
                itemViewHolder.likeImage.setTag(position);
                itemViewHolder.disLikeImage.setTag(position);
                itemViewHolder.youTubePlayerView.setTag(position);
                itemViewHolder.profileLayout.setTag(position);
                itemViewHolder.name.setTag(position);
                itemViewHolder.share.setTag(position);
                itemViewHolder.readMoreTV.setTag(position);
            }
            else {
                if (sharedPreferences.getString("user_type","").equalsIgnoreCase("3")) {
                    itemViewHolder.edit.setTag(position-1);
                    itemViewHolder.delete.setTag(position-1);
                    postModal = blogModalArrayList.get(position-1);
                    itemViewHolder.likeImage.setTag(position-1);
                    itemViewHolder.disLikeImage.setTag(position-1);
                    itemViewHolder.youTubePlayerView.setTag(position-1);
                    itemViewHolder.name.setTag(position-1);
                    itemViewHolder.profileLayout.setTag(position-1);
                    itemViewHolder.share.setTag(position-1);
                    itemViewHolder.readMoreTV.setTag(position-1);
                }
                else {
                    itemViewHolder.edit.setTag(position);
                    itemViewHolder.delete.setTag(position);
                    postModal = blogModalArrayList.get(position);
                    itemViewHolder.likeImage.setTag(position);
                    itemViewHolder.disLikeImage.setTag(position);
                    itemViewHolder.youTubePlayerView.setTag(position);
                    itemViewHolder.profileLayout.setTag(position);
                    itemViewHolder.name.setTag(position);
                    itemViewHolder.share.setTag(position);
                    itemViewHolder.readMoreTV.setTag(position);
                }

            }
            itemViewHolder.name.setText(postModal.getName());
            itemViewHolder.date.setText(postModal.getDate());
            itemViewHolder.views.setText(context.getResources().getString(R.string.views)+" "+postModal.getView());
            itemViewHolder.title.setText(postModal.getTitle());
            String tempDesc = postModal.getShortDecription().trim();
            Log.e("htmlFormat",""+Html.fromHtml(tempDesc));
            itemViewHolder.description.setText(Html.fromHtml(tempDesc));
            itemViewHolder.description.setMovementMethod(LinkMovementMethod.getInstance());
            itemViewHolder.likeCount.setText(postModal.getLikes());
            itemViewHolder.dislikeCount.setText(postModal.getDislike());

            if (postModal.getTagName().equalsIgnoreCase("")) {
                itemViewHolder.catName.setText(postModal.getCategoryName());
                itemViewHolder.tag_cat_text.setText(context.getResources().getString(R.string.categories));
            }
            else {
                itemViewHolder.catName.setText(postModal.getTagName());
                itemViewHolder.tag_cat_text.setText(context.getResources().getString(R.string.tags1));
            }

            if (postModal.getTeacherId().equalsIgnoreCase(sharedPreferences.getString("userID",""))){
                itemViewHolder.delete.setVisibility(View.VISIBLE);
                itemViewHolder.edit.setVisibility(View.VISIBLE);
            }else {
                itemViewHolder.delete.setVisibility(View.GONE);
                itemViewHolder.edit.setVisibility(View.GONE);
            }

            if (postModal.getLikeDislike().equalsIgnoreCase("0")){
                itemViewHolder.disLikeImage.setColorFilter(context.getResources().getColor(R.color.OrangeNew));
                itemViewHolder.likeImage.setColorFilter(context.getResources().getColor(R.color.GrayNew));
            }
            else if (postModal.getLikeDislike().equalsIgnoreCase("1")){
                itemViewHolder.disLikeImage.setColorFilter(context.getResources().getColor(R.color.GrayNew));
                itemViewHolder.likeImage.setColorFilter(context.getResources().getColor(R.color.OrangeNew));
            }else {
                itemViewHolder.disLikeImage.setColorFilter(context.getResources().getColor(R.color.GrayNew));
                itemViewHolder.likeImage.setColorFilter(context.getResources().getColor(R.color.GrayNew));
            }


            lineCount = itemViewHolder.description.getText().toString()
                    .split(System.getProperty("line.separator")).length;
            // Use lineCount here
            if (lineCount>3){
                itemViewHolder.readMoreTV.setVisibility(View.VISIBLE);
            }else
                itemViewHolder.readMoreTV.setVisibility(View.GONE);

            /*itemViewHolder.description.post(() -> {

            });*/

            Log.e("lineCount",""+lineCount);

            if (selectedPosition==position){
                itemViewHolder.readMoreTV.setText(R.string.readLess);
                itemViewHolder.description.setMaxLines(Integer.MAX_VALUE);
                itemViewHolder.description.setEllipsize(null);
            }
            else {
                if (scrollToPosition !=-1){
                    communityFragment.recyclerView.scrollToPosition(scrollToPosition);
                    scrollToPosition=-1;
                }
                itemViewHolder.readMoreTV.setText(context.getResources().getString(R.string.ReadMore));
                if (lineCount>3){
                    itemViewHolder.readMoreTV.setVisibility(View.VISIBLE);
                }else {
                    itemViewHolder.readMoreTV.setVisibility(View.GONE);
                }
                itemViewHolder.description.setMaxLines(3);
                itemViewHolder.readMoreTV.setText(context.getResources().getString(R.string.ReadMore));
                itemViewHolder.description.setEllipsize(TextUtils.TruncateAt.END);
            }

            itemViewHolder.readMoreTV.setOnClickListener(v->{
                TextView textView = v.findViewById(R.id.readMoreTV);
                if (textView.getText().toString().equalsIgnoreCase(context.getResources().getString(R.string.ReadMore))){
                    selectedPosition=position;
                    scrollToPosition=-1;
                    //isSelected.add(position);
                }else {
                    scrollToPosition=position;
                    selectedPosition=-1;
                }

                notifyDataSetChanged();
            });
            itemViewHolder.edit.setOnClickListener(v->{
                selectionType=-1;
                communityFragment.setBlankHeader("","","","","");
                if (headerViewHolder!=null){
                    listArray1.clear();
                    headerViewHolder.multiSelectSpinnerWithSearch.setItems(listArray1, items -> {
                    });
                }
                int pos = (int)v.getTag();
                communityFragment.setHeader( blogModalArrayList.get(pos).getShortDecription()
                        ,blogModalArrayList.get(pos).getTagIDs(),blogModalArrayList.get(pos).getImage(),
                        blogModalArrayList.get(pos).getVideoLink(),blogModalArrayList.get(pos).getId());
            });

            itemViewHolder.share.setOnClickListener(v->{
                int pos = (int)v.getTag();
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,blogModalArrayList.get(pos).getShareUrl());
                sendIntent.setType("text/plain");

                Intent shareIntent = Intent.createChooser(sendIntent, null);
                startActivity(shareIntent);
            });
            itemViewHolder.delete.setOnClickListener(v->{
                AlertDialog.Builder alertbuilder = new AlertDialog.Builder(context);
                alertbuilder.setCancelable(false);
                alertbuilder.setTitle(context.getResources().getString(R.string.deletePostTitle));
                alertbuilder.setIcon(context.getResources().getDrawable(R.mipmap.app_icon));
                alertbuilder.setMessage(context.getResources().getString(R.string.deletePost));
                alertbuilder.setPositiveButton(context.getResources().getString(R.string.yes),
                        (dialog, which) -> {
                            progressDialog = new ProgressDialog(context);
                            progressDialog.show();
                            progressDialog.setMessage(context.getResources().getString(R.string.processing));
                            int pos = (int)v.getTag();
                            deletePost(blogModalArrayList.get(pos).getId());
                        });
                alertbuilder.setNegativeButton(context.getResources().getString(R.string.no),
                        (dialog, which) -> dialog.dismiss());

                AlertDialog alertDialog = alertbuilder.create();
                alertDialog.show();
            });

            itemViewHolder.likeImage.setOnClickListener(v->{
                int pos = (int)v.getTag();
                likeDisLike("1",blogModalArrayList.get(pos).getTeacherId(),
                        blogModalArrayList.get(pos).getId(),itemViewHolder.likeCount,itemViewHolder.dislikeCount,
                        pos);
            });

            itemViewHolder.disLikeImage.setOnClickListener(v->{
                int pos = (int)v.getTag();
                likeDisLike("0",blogModalArrayList.get(pos).getTeacherId(),
                        blogModalArrayList.get(pos).getId(),itemViewHolder.likeCount,itemViewHolder.dislikeCount,
                        pos);
            });
            try {
                Glide.with(context).load(postModal.getImage()).into(itemViewHolder.image);
                //Picasso.with(context).load(postModal.getImage()).into(itemViewHolder.image);
                itemViewHolder.image.setVisibility(View.VISIBLE);
            }
            catch (Exception e){
                itemViewHolder.image.setVisibility(View.GONE);
                e.printStackTrace();
            }

            try {
                Glide.with(context).load(postModal.getProfileImage()).into(itemViewHolder.profile_pic);
            }
            catch (Exception e){
                e.printStackTrace();
            }

            communityFragment.addLifeCycleCallBack(itemViewHolder.youTubePlayerView);
            if (postModal.getVideo().equalsIgnoreCase("")){
                itemViewHolder.youTubePlayerView.setVisibility(View.GONE);
            }else {
                itemViewHolder.youTubePlayerView.setVisibility(View.VISIBLE);
            }

            itemViewHolder.cueVideo(postModal.getVideo());

            itemViewHolder.profileLayout.setOnClickListener(v->{
                int pos = (int)v.getTag();
                Bundle bundle=new Bundle();
                bundle.putString("trainerID",blogModalArrayList.get(pos).getTeacherId());
                ViewProfile viewProfile=new ViewProfile();
                viewProfile.setArguments(bundle);
                ((MainActivity) context).addFragment2(viewProfile, true, false, 0);
            });
            itemViewHolder.name.setOnClickListener(v->{
                int pos = (int)v.getTag();
                Bundle bundle=new Bundle();
                bundle.putString("trainerID",blogModalArrayList.get(pos).getTeacherId());
                ViewProfile viewProfile=new ViewProfile();
                viewProfile.setArguments(bundle);
                ((MainActivity) context).addFragment2(viewProfile, true, false, 0);
            });
            if (communityFragment.progressDialog!=null)
                communityFragment.progressDialog.dismiss();
        }
    }

    private void deletePost(String id) {
        Map<String,String> param = new HashMap<>();
        param.put("removed_id",id);
        param.put("lan",sharedPreferences.getString("lang",""));
        Log.e("deleteCommunity",""+param);
        new PostMethod(Api.deletePost,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                progressDialog.dismiss();
                Log.e("deleteResp",data);
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    if (jsonObject.optBoolean("status")) {
                        communityFragment.getAllBlog();
                        communityFragment.setBlankHeader("","","","","");
                        progressDialog.cancel();
                        tagIds.clear();
                        headerViewHolder.youTubeURL.getText().clear();

                        photoFile = null;
                        headerViewHolder.fileName.getText().clear();
                        headerViewHolder.postLayout.setVisibility(View.GONE);
                        selectionType=-1;
                        postID="";
                        uploadedImage="";
                    }
                    Toast.makeText(context, jsonObject.optString("message"), Toast.LENGTH_SHORT).show();

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void error(VolleyError error) {
                progressDialog.dismiss();
                error.printStackTrace();
            }
        });
    }

    private void getTags() {
        new GetMethod(Api.allTags+sharedPreferences.getString("lang",""), context).startmethod(new ResponseData() {
            @Override
            public void response(String data) {
                Log.e("allTags",data);
                try {
                    JSONObject dataObject = new JSONObject(data);
                    JSONArray tag = dataObject.optJSONArray("data");
                    tagsModalArrayList.clear();
                    tagTempNames.clear();
                    listArray1.clear();
                    tagIds.clear();
                    idArray = null;
                    uploadedImage="";
                    bundle = communityFragment.getHeader();
                    if (bundle!=null){
                        if (!bundle.getString("postID").equalsIgnoreCase("")){
                            headerViewHolder.postLayout.setVisibility(View.VISIBLE);
                            idArray = bundle.getString("tagIDs").split(",");
                            postID = bundle.getString("postID");
                            if (!bundle.getString("imageName").equalsIgnoreCase(""))
                                uploadedImage = URLUtil.guessFileName(bundle.getString("imageName"), null, null);
                            headerViewHolder.youTubeURL.setText(bundle.getString("youtubeLink"));

                            tagIds.addAll(Arrays.asList(idArray));
                            if (selectionType==-1) {
                                headerViewHolder.fileName.setText(bundle.getString("html"));
                                headerViewHolder.htmlContent=headerViewHolder.fileName.getText().toString();
                                headerViewHolder.editorDescription.setHtml(headerViewHolder.htmlContent);
                                headerViewHolder.editorDescription.setHtml(bundle.getString("html"));
                                headerViewHolder.fileName.setText(uploadedImage);
                                headerViewHolder.htmlContent="";
                            }
                            else
                                headerViewHolder.fileName.setText(communityFragment.getFileName());

                        }
                    }else
                        headerViewHolder.fileName.setText(communityFragment.getFileName());


                    for (int me=0; me<tag.length(); me++){

                        TagsModal tagsModal = new TagsModal();
                        JSONObject jsonObject = tag.getJSONObject(me);
                        tagsModal.setTagId(jsonObject.optString("id"));
                        tagsModal.setTagName(jsonObject.optString("tag"));
                        tagsModalArrayList.add(tagsModal);
                        tagTempNames.add(jsonObject.optString("tag"));

                        KeyPairBoolData h = new KeyPairBoolData();
                        h.setId(Long.parseLong(jsonObject.optString("id")));
                        h.setName(jsonObject.optString("tag"));
                        if (idArray!=null){
                            for (String s : idArray) {
                                if (jsonObject.optString("id").equalsIgnoreCase(s))
                                    h.setSelected(true);
                            }
                        }
                        //h.setSelected(me < 5);
                        listArray1.add(h);
                    }
                    headerViewHolder.multiSelectSpinnerWithSearch.setItems(listArray1, items -> {
                        tagIds.clear();
                        for (int i = 0; i < items.size(); i++) {
                            if (items.get(i).isSelected()) {
                                if (!tagIds.contains(""+items.get(i).getId()))
                                    tagIds.add(""+items.get(i).getId());
                            }
                        }
                        Log.e("SelectedTags",""+tagIds);
                    });
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                error.printStackTrace();
            }
        });

    }

    private void likeDisLike(final String like_dislike, String teacher_id, String community_id,
                             TextView likeCount, TextView disLikeCount, int pos) {
        if (sharedPreferences.getString("userID", "").equalsIgnoreCase("-1")) {
            Intent intent = new Intent(context, LoginActivity.class);
            startActivity(intent);
            return;
        }
        progressDialog = new ProgressDialog(context);
        progressDialog.show();

        Map<String,String> param = new HashMap<>();
        param.put("trainer_id",teacher_id);
        param.put("user_id",sharedPreferences.getString("userID",""));
        param.put("community_id",community_id);
        param.put("like_dislike_value",like_dislike);
        param.put("lang",sharedPreferences.getString("lang",""));

        Log.e("GetLikeDislikeParam",""+param);
        new PostMethod(Api.GetLikeDislike, param, context).startPostMethod(new ResponseData() {
            @Override
            public void response(final String data) {
                Log.e("GetLikeDislike",data);
                progressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    //Toast.makeText(context, jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                    blogModalArrayList.get(pos).setLikes(jsonObject.optString("like"));
                    blogModalArrayList.get(pos).setDislike(jsonObject.optString("dislike"));
                    blogModalArrayList.get(pos).setLikeDislike(like_dislike);
                    notifyDataSetChanged();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(final VolleyError error) {
                progressDialog.dismiss();
                error.printStackTrace();
            }
        });
    }

    @Override
    public int getItemCount() {
        if (sharedPreferences.getString("userID","").equalsIgnoreCase("-1")){
            return blogModalArrayList.size();
        }

        if (sharedPreferences.getString("user_type","").equalsIgnoreCase("3")){
            return blogModalArrayList.size()+1;

        }else
            return blogModalArrayList.size();

    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        CircleImageView profile_pic;
        WebView mWebView;
        FrameLayout flAction;
        ImageView iv_action_insert_link;
        ProgressDialog progressDialog;
        android.app.AlertDialog dialog;
        EditText fileName,title,keywords,youTubeURL;
        TextView submitPost,blogError,tagError,chooseFile,text,youtubeText,uploadImageOptional,tagText;
        RelativeLayout headerLayout;

        public FrameLayout postLayout;
        private String htmlContent = "";
        MultiSpinnerSearch multiSelectSpinnerWithSearch;

        public RichTextEditor editorDescription;
        private AllCommandsEditorToolbar editorToolbarDescription;

        public HeaderViewHolder(View root) {
            super(root);
            sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            profile_pic= root.findViewById(R.id.profile_pic);

            youtubeText = root.findViewById(R.id.youtubeText);
            uploadImageOptional = root.findViewById(R.id.uploadImageOptional);
            tagText = root.findViewById(R.id.tagText);
            iv_action_insert_link = root.findViewById(R.id.iv_action_insert_link);
            text = root.findViewById(R.id.text);
            postLayout = root.findViewById(R.id.postLayout);
            headerLayout = root.findViewById(R.id.headerLayout);
            blogError = root.findViewById(R.id.blogError);
            tagError = root.findViewById(R.id.tagError);
            chooseFile = root.findViewById(R.id.chooseFile);
            fileName = root.findViewById(R.id.fileName);
            youTubeURL = root.findViewById(R.id.youTubeURL);
            flAction = root.findViewById(R.id.fl_action);
            submitPost = root.findViewById(R.id.submitPost);
            //llActionBarContainer = root.findViewById(R.id.ll_action_bar_container);

            editorToolbarDescription = root.findViewById(R.id.editorToolbarDescription);
            editorDescription = root.findViewById(R.id.editorDescription);

            editorToolbarDescription.setEditor(editorDescription);

            editorDescription.setEditorFontSize(14);
            editorDescription.setPadding((int) (4 * context.getResources().getDisplayMetrics().density));

            multiSelectSpinnerWithSearch = root.findViewById(R.id.multipleItemSelectionSpinner);
            multiSelectSpinnerWithSearch.setSearchHint(context.getResources().getString(R.string.search));
            multiSelectSpinnerWithSearch.setSearchEnabled(true);
            multiSelectSpinnerWithSearch.setShowSelectAllButton(true);

            Resources activityRes = context.getResources();
            Configuration activityConf = activityRes.getConfiguration();
            Locale newLocale = new Locale(sharedPreferences.getString("lang",""));
            activityConf.setLocale(newLocale);
            activityRes.updateConfiguration(activityConf, activityRes.getDisplayMetrics());

            Resources applicationRes = context.getResources();
            Configuration applicationConf = applicationRes.getConfiguration();
            applicationConf.setLocale(newLocale);
            applicationRes.updateConfiguration(applicationConf, applicationRes.getDisplayMetrics());

            text.setOnClickListener(v->{
                if (postLayout.getVisibility()==View.VISIBLE) {
                    postLayout.setVisibility(View.GONE);
                    youTubeURL.setFocusableInTouchMode(true);
                    youTubeURL.setFocusable(true);
                    youTubeURL.requestFocus();

                    listArray1.clear();
                    tagIds.clear();
                    communityFragment.setBlankHeader("","","","","");
                    tagIds.clear();
                    youTubeURL.getText().clear();
                    photoFile = null;
                    fileName.getText().clear();
                    selectionType=-1;
                    postID="";
                    uploadedImage="";
                    multiSelectSpinnerWithSearch.setHintText(context.getResources().getString(R.string.selectTagsForPost));

                    headerViewHolder.multiSelectSpinnerWithSearch.setItems(listArray1, items -> {
                    });

                    getTags();
                    editorDescription.setHtml("");
                    new Handler().postDelayed(()->{
                        InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(youTubeURL.getWindowToken(), 0);
                    },500);
                }
                else
                {
                    postLayout.setVisibility(View.VISIBLE);
                    communityFragment.searchMainLayout.setVisibility(View.GONE);
                    communityFragment.searchTrainerAndCourse.getText().clear();
                    getTags();
                }
            });

            new Handler().postDelayed(()->{
                tagText.setText(context.getResources().getString(R.string.tags));
                uploadImageOptional.setText(context.getResources().getString(R.string.uploadImage));
                chooseFile.setText(context.getResources().getString(R.string.chooseFile));
                youtubeText.setText(context.getResources().getString(R.string.youtube));
                submitPost.setText(context.getResources().getString(R.string.post1));
                blogError.setText(context.getResources().getString(R.string.fieldRequired1));
                tagError.setText(context.getResources().getString(R.string.fieldRequired1));
                multiSelectSpinnerWithSearch.setHintText(context.getResources().getString(R.string.selectTagsForPost));
                multiSelectSpinnerWithSearch.setSearchHint(context.getResources().getString(R.string.search));
                multiSelectSpinnerWithSearch.setEmptyTitle(context.getResources().getString(R.string.noMatchFound));
                multiSelectSpinnerWithSearch.setClearText(context.getResources().getString(R.string.close));
                multiSelectSpinnerWithSearch.setLimit(3, data -> Toast.makeText(context,
                        context.getResources().getString(R.string.selectOnlyTHree), Toast.LENGTH_LONG).show());
                captureImageInitialization();
            },2000);


            chooseFile.setOnClickListener(v->{
                dialog.show();
            });
            submitPost.setOnClickListener(v->{
                progressDialog = new ProgressDialog(context);
                progressDialog.show();
                progressDialog.setMessage(context.getResources().getString(R.string.processing));

                htmlContent = editorDescription.getCachedHtml();

                new Handler().postDelayed(() -> {
                    progressDialog.dismiss();

                    if (htmlContent.trim().matches("")){
                        profile_pic.requestFocus();
                        tagError.setVisibility(View.GONE);
                        blogError.setVisibility(View.VISIBLE);
                        return;
                    }

                    if (htmlContent.trim().matches("<p><br></p>")){
                        profile_pic.requestFocus();
                        tagError.setVisibility(View.GONE);
                        blogError.setVisibility(View.VISIBLE);
                        return;
                    }

                    if (tagIds.size()==0){
                        tagError.requestFocus();
                        tagError.setVisibility(View.VISIBLE);
                        return;
                    }
                    if (headerViewHolder.multiSelectSpinnerWithSearch.getSelectedItems().size()==0){
                        tagError.requestFocus();
                        tagError.setVisibility(View.VISIBLE);
                        tagIds.clear();
                        return;
                    }
                    tagError.setVisibility(View.GONE);
                    blogError.setVisibility(View.GONE);

                    youTubeURL.setFocusableInTouchMode(true);
                    youTubeURL.setFocusable(true);
                    youTubeURL.requestFocus();
                    new Handler().postDelayed(()->{
                        InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(youTubeURL.getWindowToken(), 0);
                    },500);
                    submitPostDetails();
                    // close this activity
                }, 2000);

            });
        }

        private void captureImageInitialization() {
            /**
             * a selector dialog to display two image source options, from camera
             * ‘Take from camera’ and from existing files ‘Select from gallery’
             */
            final String[] items = new String[]{context.getResources().getString(R.string.Takefromcamera)
                    ,context.getResources().getString(R.string.Selectfromgallery)};
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.select_dialog_item, items);
            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);

            builder.setTitle(context.getResources().getString(R.string.SelectImage));
            builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) { // pick from
                    // camera
                    if (item == 0) {
                        selectionType=1;
                        communityFragment.takePic(1);

                    } else {
                        selectionType=3;
                        communityFragment.takePic(3);

                    }
                }
            });

            dialog = builder.create();
        }

        private void submitPostDetails() {
            Log.e("photoFile","===>"+photoFile);
            progressDialog.setTitle(context.getResources().getString(R.string.processing));
            progressDialog.show();
            new Thread(() -> {
                try {
                    //first_name,last_name,contact_no,country,trainee_id,profile_photo_edit
                    MultipartUtility multipart = new MultipartUtility(Api.AddCommunity, "UTF-8");
                    multipart.addFormField("trainer_id",sharedPreferences.getString("userID",""));
                    multipart.addFormField("blog",htmlContent);
                    multipart.addFormField("tags",TextUtils.join(",", tagIds));
                    multipart.addFormField("video_link",youTubeURL.getText().toString());
                    multipart.addFormField("community_id",postID);
                    multipart.addFormField("upload_image_edit",uploadedImage);
                    if(photoFile!=null) {
                        multipart.addFilePart("upload_image", photoFile);
                    }else
                        multipart.addFormField("upload_image", "");


                    String response = multipart.finish();// response from server
                    try {
                        final JSONObject jsonObject1 = new JSONObject(response);
                        boolean status = jsonObject1.optBoolean("status");
                        ((MainActivity)context).runOnUiThread(() -> {
                            progressDialog.cancel();

                            // Stuff that updates the UI
                            if(status){
                                communityFragment.setBlankHeader("","","","","");
                                progressDialog.cancel();
                                tagIds.clear();
                                youTubeURL.getText().clear();

                                photoFile = null;
                                fileName.getText().clear();
                                communityFragment.getAllBlog();
                                postLayout.setVisibility(View.GONE);
                                selectionType=-1;
                                postID="";
                                uploadedImage="";
                            }
                            Toast.makeText(context, jsonObject1.optString("message"), Toast.LENGTH_SHORT).show();

                        });

                    } catch (JSONException e) {
                        ((MainActivity)context).runOnUiThread(() -> {
                            progressDialog.cancel();
                            // Stuff that updates the UI
                        });
                        e.printStackTrace();
                    }
                } catch (IOException e) {
                    ((MainActivity)context).runOnUiThread(() -> {
                        progressDialog.cancel();
                        // Stuff that updates the UI
                    });
                    e.printStackTrace();
                }
            }).start();

        }
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        public String teacher_id="";

        CircleImageView profile_pic;
        ImageView image,share,likeImage,disLikeImage;
        TextView name, date, views, title, description, likeCount, dislikeCount, catName,readMoreTV
                ,tag_cat_text;
        LinearLayout likeLayout,disLikeLayout,delete,edit,profileLayout;
        YouTubePlayerView youTubePlayerView;
        private YouTubePlayer youTubePlayer;
        private String currentVideoId;
        View itemRowViews;
        public ItemViewHolder(View root) {
            super(root);

            itemRowViews=root;
            Resources activityRes = context.getResources();
            Configuration activityConf = activityRes.getConfiguration();
            Locale newLocale = new Locale(sharedPreferences.getString("lang",""));
            activityConf.setLocale(newLocale);
            activityRes.updateConfiguration(activityConf, activityRes.getDisplayMetrics());

            Resources applicationRes = context.getResources();
            Configuration applicationConf = applicationRes.getConfiguration();
            applicationConf.setLocale(newLocale);
            applicationRes.updateConfiguration(applicationConf, applicationRes.getDisplayMetrics());

            profileLayout = root.findViewById(R.id.profileLayout);
            readMoreTV = root.findViewById(R.id.readMoreTV);
            profile_pic = root.findViewById(R.id.profile_pic);
            youTubePlayerView = root.findViewById(R.id.youTubePlayerView);

            tag_cat_text = root.findViewById(R.id.tag_cat_text);
            delete = root.findViewById(R.id.delete);
            edit = root.findViewById(R.id.edit);
            disLikeLayout = root.findViewById(R.id.disLikeLayout);
            likeLayout = root.findViewById(R.id.likeLayout);
            share = root.findViewById(R.id.share);
            image = root.findViewById(R.id.image);
            name = root.findViewById(R.id.name);
            date = root.findViewById(R.id.date);
            views = root.findViewById(R.id.view);
            title = root.findViewById(R.id.title);
            description = root.findViewById(R.id.description);
            likeCount = root.findViewById(R.id.likeCount);
            disLikeImage = root.findViewById(R.id.disLikeImage);
            likeImage = root.findViewById(R.id.likeImage);
            dislikeCount = root.findViewById(R.id.dislikeCount);
            catName = root.findViewById(R.id.catName);

            youTubePlayerView.addYouTubePlayerListener(new AbstractYouTubePlayerListener() {
                @Override
                public void onReady(@NonNull YouTubePlayer initializedYouTubePlayer) {
                    youTubePlayer = initializedYouTubePlayer;
                    youTubePlayer.cueVideo(currentVideoId, 0);
                }
            });

        }
        void cueVideo(String videoId) {
            currentVideoId = videoId;

            if(youTubePlayer == null)
                return;

            youTubePlayer.cueVideo(videoId, 0);
        }

    }

    public void filter(String charText) {

        Log.e("filters",charText);
        //If Filter type is NAME and EMAIL then only do lowercase, else in case of NUMBER no need to do lowercase because of number format
        charText = charText.toLowerCase(Locale.getDefault());
        blogModalArrayList.clear();//Clear the main ArrayList

        //If search query is null or length is 0 then add all filterList items back to attachmentList
        if (charText.length() == 0) {
            blogModalArrayList.addAll(filterArrayList);
        } else {

            //Else if search query is not null do a loop to all filterList items
            for (BlogModal model : filterArrayList) {

                //Now check the type of search filter
                if (model.getShortDecription().toLowerCase(Locale.getDefault()).contains(charText)||
                        model.getTagIDs().toLowerCase(Locale.getDefault()).contains(charText))
                    blogModalArrayList.add(model);

            }
        }
        notifyDataSetChanged();
    }
}
