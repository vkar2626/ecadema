package com.ecadema.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ecadema.app.R;
import com.ecadema.modal.CoursePriceModal;

import java.util.ArrayList;

public class CoursePriceAdapter extends RecyclerView.Adapter <CoursePriceAdapter.RowHolder>{
    private final Context context;
    private final ArrayList<CoursePriceModal> coursePriceModalArrayList;

    public CoursePriceAdapter(Context context, ArrayList<CoursePriceModal> coursePriceModalArrayList) {
        this.context=context;
        this.coursePriceModalArrayList=coursePriceModalArrayList;
    }

    @NonNull
    @Override
    public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.price_layout,null);
        return new RowHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RowHolder holder, int position) {
        CoursePriceModal coursePriceModal=coursePriceModalArrayList.get(position);

        holder.hours.setText(coursePriceModal.getHours());
        holder.rate.setText(coursePriceModal.getPrice());
    }

    @Override
    public int getItemCount() {
        return coursePriceModalArrayList.size();
    }

    public class RowHolder extends RecyclerView.ViewHolder {
        TextView hours,rate;
        public RowHolder(@NonNull View itemView) {
            super(itemView);

            rate=itemView.findViewById(R.id.rate);
            hours=itemView.findViewById(R.id.hours);
        }
    }
}
