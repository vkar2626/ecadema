package com.ecadema.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ecadema.app.R;
import com.ecadema.modal.CreditModal;

import java.util.ArrayList;

public class CreditAdapter extends RecyclerView.Adapter<CreditAdapter.RowHolder> {
    private final Context context;
    private final ArrayList<CreditModal> creditModalArrayList;

    public CreditAdapter(Context context, ArrayList<CreditModal> creditModalArrayList) {
        this.context=context;
        this.creditModalArrayList=creditModalArrayList;
    }

    @NonNull
    @Override
    public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.credit_adapter,null);
        return new RowHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RowHolder holder, int position) {
        CreditModal creditModal = creditModalArrayList.get(position);
        holder.dateTime.setText(creditModal.getDate());

        if(creditModal.getCreditDebit().equalsIgnoreCase("C")) {
            holder.price.setText("+ $ "+creditModal.getPrice());
            holder.price.setTextColor(context.getResources().getColor(R.color.green));
        }
        else {
            holder.price.setText("- $ "+creditModal.getPrice());
            holder.price.setTextColor(context.getResources().getColor(R.color.red));
        }
    }

    @Override
    public int getItemCount() {
        return creditModalArrayList.size();
    }

    public class RowHolder extends RecyclerView.ViewHolder {
        TextView dateTime,price;
        public RowHolder(@NonNull View itemView) {
            super(itemView);

            dateTime=itemView.findViewById(R.id.dateTime);
            price=itemView.findViewById(R.id.price);
        }
    }
}
