package com.ecadema.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.ecadema.app.R;

import java.util.ArrayList;

public class CustomAdapter extends ArrayAdapter<String> {
    private final Context context;
    private final int itemLayout;
    private final ArrayList<String> dataList;
    ArrayList<String> arrayList =new ArrayList<>();

    public CustomAdapter(Context context, int custom_adapter, ArrayList<String> emailID) {
        super(context, custom_adapter, emailID);
        this.context=context;
        this.itemLayout=custom_adapter;
        this.dataList=emailID;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public String getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public View getView(int position, View view, @NonNull ViewGroup parent) {

        if (view == null) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(itemLayout, parent, false);
        }

        CheckBox checkbox = view.findViewById(R.id.checkbox);
        TextView textView = view.findViewById(R.id.text);
        if (checkbox.getText().toString().equalsIgnoreCase(context.getResources().getString(R.string.allReadyBookedForAllMembers))) {
            textView.setVisibility(View.VISIBLE);
            checkbox.setVisibility(View.GONE);
            textView.setText(getItem(position));
        }else {
            textView.setVisibility(View.GONE);
            checkbox.setVisibility(View.VISIBLE);
            checkbox.setText(getItem(position));
        }

        textView.setOnClickListener(view1 -> {
            Intent intent = new Intent("CheckBox");
            intent.putExtra("csv",textView.getText().toString());
            context.sendBroadcast(intent);
        });
        checkbox.setChecked(arrayList.contains(checkbox.getText().toString()));

        checkbox.setOnClickListener(view1 -> {
            if(arrayList.contains(checkbox.getText().toString())){
                arrayList.remove(checkbox.getText().toString());
            }
            else
                arrayList.add(checkbox.getText().toString());
            Intent intent = new Intent("CheckBox");
            intent.putExtra("csv",android.text.TextUtils.join(",\n", arrayList));
            context.sendBroadcast(intent);
        });
/*
        checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                    arrayList.add(checkbox.getText().toString());
                else
                    arrayList.remove(checkbox.getText().toString());

                Intent intent = new Intent("CheckBox");
                intent.putExtra("csv",android.text.TextUtils.join(",", arrayList));
                context.sendBroadcast(intent);
            }
        });
*/

        return view;
    }
}
