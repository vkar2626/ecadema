package com.ecadema.adapter;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ecadema.app.R;
import com.ecadema.modal.ExpModal;

import java.util.ArrayList;

public class ExpAdapter extends RecyclerView.Adapter<ExpAdapter.RowHolder> {
    private final Context context;
    private final ArrayList<ExpModal> expModalArrayList;

    public ExpAdapter(Context context, ArrayList<ExpModal> expModalArrayList) {
        this.context=context;
        this.expModalArrayList=expModalArrayList;
    }

    @NonNull
    @Override
    public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.exp_adapter,null);
        return new RowHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RowHolder holder, int position) {
        ExpModal expModal=expModalArrayList.get(position);
        holder.compName.setText(expModal.getOrganisation());
        holder.year.setText(expModal.getYear());
        holder.designation.setText(expModal.getDesignation());
        holder.view.setOnClickListener(v->{
            openFile(expModal.getURL());
        });

        holder.edit.setOnClickListener(v->{
            Intent intent=new Intent("updateExperience");
            intent.putExtra("id",expModal.getId());
            intent.putExtra("designation",expModal.getDesignation());
            intent.putExtra("url",expModal.getURL());
            intent.putExtra("certificate",expModal.getCertificate());
            intent.putExtra("organisation",expModal.getOrganisation());

            intent.putExtra("from",expModal.getStart());
            intent.putExtra("to",expModal.getTo());

            context.sendBroadcast(intent);
        });
    }

    @Override
    public int getItemCount() {
        return expModalArrayList.size();
    }

    public class RowHolder extends RecyclerView.ViewHolder {
        TextView compName,year,view,designation;
        ImageView edit;
        public RowHolder(@NonNull View itemView) {
            super(itemView);
            edit=itemView.findViewById(R.id.edit);
            view=itemView.findViewById(R.id.view);
            compName=itemView.findViewById(R.id.compName);
            year=itemView.findViewById(R.id.year);
            designation=itemView.findViewById(R.id.designation);
        }
    }

    private void openFile(String url) {

        try {

            Uri uri = Uri.parse(url);

            Intent intent = new Intent(Intent.ACTION_VIEW);
            if (url.toString().contains(".doc") || url.toString().contains(".docx")) {
                // Word document
                intent.setDataAndType(uri, "application/msword");
            } else if (url.toString().contains(".pdf")) {
                // PDF file
                intent.setDataAndType(uri, "application/pdf");
            } else if (url.toString().contains(".ppt") || url.toString().contains(".pptx")) {
                // Powerpoint file
                intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
            } else if (url.toString().contains(".xls") || url.toString().contains(".xlsx")) {
                // Excel file
                intent.setDataAndType(uri, "application/vnd.ms-excel");
            } else if (url.toString().contains(".zip")) {
                // ZIP file
                intent.setDataAndType(uri, "application/zip");
            } else if (url.toString().contains(".rar")){
                // RAR file
                intent.setDataAndType(uri, "application/x-rar-compressed");
            } else if (url.toString().contains(".rtf")) {
                // RTF file
                intent.setDataAndType(uri, "application/rtf");
            } else if (url.toString().contains(".wav") || url.toString().contains(".mp3")) {
                // WAV audio file
                intent.setDataAndType(uri, "audio/x-wav");
            } else if (url.toString().contains(".gif")) {
                // GIF file
                intent.setDataAndType(uri, "image/gif");
            } else if (url.toString().contains(".jpg") || url.toString().contains(".jpeg") || url.toString().contains(".png")) {
                // JPG file
                intent.setDataAndType(uri, "image/jpeg");
            } else if (url.toString().contains(".txt")) {
                // Text file
                intent.setDataAndType(uri, "text/plain");
            } else if (url.toString().contains(".3gp") || url.toString().contains(".mpg") ||
                    url.toString().contains(".mpeg") || url.toString().contains(".mpe") || url.toString().contains(".mp4") || url.toString().contains(".avi")) {
                // Video files
                intent.setDataAndType(uri, "video/*");
            } else {
                intent.setDataAndType(uri, "*/*");
            }

            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW);
            browserIntent.setDataAndType(Uri.parse(url), "text/html");

            Intent chooser = Intent.createChooser(browserIntent, context.getResources().getString(R.string.chooseApp));
            chooser.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // optional

            context.startActivity(chooser);

            Toast.makeText(context, context.getResources().getString(R.string.noApp), Toast.LENGTH_SHORT).show();
        }
    }

}
