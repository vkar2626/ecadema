package com.ecadema.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ecadema.app.R;
import com.ecadema.modal.LanguageModal;

import java.util.ArrayList;

public class LanguageAdapter extends RecyclerView.Adapter<LanguageAdapter.RowHolder> {

    private final Context context;
    private final ArrayList<LanguageModal> languageList;
    private ArrayList<String> checkedValues= new ArrayList<>();
    private String check_value;

    public LanguageAdapter(Context context, ArrayList<LanguageModal> languageList) {
        this.context=context;
        this.languageList=languageList;
    }

    @NonNull
    @Override
    public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.checkbox_layout,null);
        return new RowHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RowHolder holder, int position) {
        LanguageModal languageModal=languageList.get(position);
        holder.checkbox.setText(languageModal.getLanguage());

        holder.checkbox.setChecked(languageList.get(position).isSelected());
        holder.checkbox.setTag(languageList.get(position));

        holder.checkbox.setOnClickListener(v -> {
            CheckBox cb = (CheckBox) v;
            LanguageModal languageModal1 = (LanguageModal) cb.getTag();

            languageModal1.setSelected(cb.isChecked());

            check_value=languageModal1.getId();
            Intent intent=new Intent("langLocBroadCast");
            intent.putExtra("filterType","lang");
            if(cb.isChecked()) {
                checkedValues.add(check_value);
                String csv = android.text.TextUtils.join(",", checkedValues);
                Log.e("checkedValues", String.valueOf(csv));
                intent.putExtra("langLoc",csv);
            }
            else {
                checkedValues.remove(check_value);
                String csv = android.text.TextUtils.join(",", checkedValues);
                Log.e("checkedValuesRem", String.valueOf(csv));
                intent.putExtra("langLoc",csv);
            }
            context.sendBroadcast(intent);

        });

        holder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Intent intent=new Intent("langLocBroadCast");
                intent.putExtra("filterType","lang");
                if(b){
                    intent.putExtra("langLoc",languageModal.getId());
                    intent.putExtra("type","add");
                }else {
                    intent.putExtra("langLoc",languageModal.getId());
                    intent.putExtra("type","remove");
                }

                context.sendBroadcast(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return languageList.size();
    }

    public class RowHolder extends RecyclerView.ViewHolder {
        CheckBox checkbox;
        public RowHolder(@NonNull View itemView) {
            super(itemView);

            checkbox=itemView.findViewById(R.id.checkbox);
        }
    }
}
