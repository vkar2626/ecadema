package com.ecadema.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ecadema.app.R;
import com.ecadema.modal.LocationModal;

import java.util.ArrayList;

public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.RowHolder> {
    private final Context context;
    private final ArrayList<LocationModal> locationList;
    private ArrayList<String> checkedValues= new ArrayList<>();
    private String check_value;

    public LocationAdapter(Context context, ArrayList<LocationModal> locationList) {
        this.context=context;
        this.locationList=locationList;
    }

    @NonNull
    @Override
    public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.checkbox_layout,null);
        return new RowHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RowHolder holder, int position) {
        LocationModal locationModal=locationList.get(position);
        holder.checkbox.setText(locationModal.getLanguage());

        holder.checkbox.setChecked(locationList.get(position).isSelected());
        holder.checkbox.setTag(locationList.get(position));

        holder.checkbox.setTag(position);

    }

    @Override
    public int getItemCount() {
        return locationList.size();
    }

    public class RowHolder extends RecyclerView.ViewHolder {
        CheckBox checkbox;
        public RowHolder(@NonNull View itemView) {
            super(itemView);

            checkbox=itemView.findViewById(R.id.checkbox);
            checkbox.setOnClickListener(v -> {
                CheckBox cb = (CheckBox) v;
                int pos=(int)v.getTag();
                LocationModal locationModal1 = locationList.get(pos);

                locationModal1.setSelected(cb.isChecked());

                check_value=locationModal1.getId();
                Intent intent=new Intent("langLocBroadCast");
                intent.putExtra("filterType","loc");
                if(cb.isChecked()) {
                    checkedValues.add(check_value);
                    String csv = android.text.TextUtils.join(",", checkedValues);
                    Log.e("checkedValues", String.valueOf(csv));
                    intent.putExtra("langLoc",csv);
                }
                else {
                    checkedValues.remove(check_value);
                    String csv = android.text.TextUtils.join(",", checkedValues);
                    Log.e("checkedValuesRem", String.valueOf(csv));
                    intent.putExtra("langLoc",csv);
                }
                context.sendBroadcast(intent);
            });
        }
    }
}
