package com.ecadema.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.PostMethod;
import com.ecadema.apiMethods.ResponseData;
import com.ecadema.app.R;
import com.ecadema.app.Web_View;
import com.ecadema.fragments.MyClassesFragment;
import com.ecadema.modal.UpcomingModal;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class MyUpcomingAdapter extends RecyclerView.Adapter<MyUpcomingAdapter.RowHolder> {
    private final Context context;
    private final ArrayList<UpcomingModal> upcomingModalArrayList;
    private final MyClassesFragment myClassesFragment;
    SharedPreferences sharedPreferences;

    public MyUpcomingAdapter(Context context, ArrayList<UpcomingModal> upcomingModalArrayList, MyClassesFragment myClassesFragment) {
        this.context=context;
        this.upcomingModalArrayList=upcomingModalArrayList;
        this.myClassesFragment=myClassesFragment;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    @NonNull
    @Override
    public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.my_session_adapter,null);
        return new RowHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(@NonNull RowHolder holder, int position) {
        UpcomingModal upcomingModal=upcomingModalArrayList.get(position);

        String text = "<font color=#f16667>"+ context.getResources().getString(R.string.with) +" </font> <font color=#757575>"+upcomingModal.getName()+"</font>";
        holder.name.setText(Html.fromHtml(text));

        holder.sessionType.setText(upcomingModal.getSessionType());
        holder.about.setText(upcomingModal.getSessionName());
        holder.dateTime.setText(upcomingModal.getDateTime());
        holder.totalHours.setText(upcomingModal.getDuration()+" "+context.getResources().getString(R.string.hours) );
        holder.languages.setText(upcomingModal.getLanguage());

        try {
            Picasso.with(context).load(upcomingModal.getBannerImage()).into(holder.headerPic);

        }catch (Exception e){
            e.printStackTrace();
        }

        try {
            Picasso.with(context).load(upcomingModal.getImage()).into(holder.profilePic);

        }catch (Exception e){
            e.printStackTrace();
        }

        new CountDownTimer(upcomingModal.getTimer(), 1000){
            @Override
            public void onTick(long millisUntilFinished) {

                //holder.rate.setText("$ "+upcomingModal.getRate()+" /Hour");

                /* converting the milliseconds into days, hours, minutes and seconds and displaying it in textviews */
                holder.days.setText(TimeUnit.HOURS.toDays(TimeUnit.MILLISECONDS.toHours(millisUntilFinished))+" "+context.getResources().getString(R.string.D));
                holder.hours.setText((TimeUnit.MILLISECONDS.toHours(millisUntilFinished) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millisUntilFinished)))+" "+context.getResources().getString(R.string.H));
                holder.mins.setText((TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)))+" "+context.getResources().getString(R.string.M));
                holder.seconds.setText((TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)))+" "+context.getResources().getString(R.string.S));
            }

            @Override

            public void onFinish() {
                /* clearing all fields and displaying countdown finished message  */
                holder.days.setText("00 "+context.getResources().getString(R.string.D));
                holder.hours.setText("00 "+context.getResources().getString(R.string.H));
                holder.mins.setText("00 "+context.getResources().getString(R.string.M));
                holder.seconds.setText("00 "+context.getResources().getString(R.string.S));
                myClassesFragment.RemoveGroupSession(upcomingModal.getBookedDateId());
            }
        }.start();

        if(!upcomingModal.getIsReschedule())
            holder.attend.setVisibility(View.VISIBLE);
        else
            holder.attend.setVisibility(View.GONE);

        holder.attend.setOnClickListener(v->{
            if(sharedPreferences.getString("user_type","").equalsIgnoreCase("1")){
                if(!upcomingModal.getWizIQ().equalsIgnoreCase("null")){
                    Intent intent = new Intent(context, Web_View.class);
                    intent.putExtra("url",upcomingModal.getWizIQ());
                    context.startActivity(intent);
                    /*Intent intent = new Intent(context, ClassroomActivity.class);
                    intent.putExtra(ClassroomActivity.URL, upcomingModal.getWizIQ());
                    context.startActivity(intent);*/

                    if(upcomingModal.getIsSendPDF()){
                        Map<String,String> param = new HashMap<>();
                        param.put("booking_id",upcomingModal.getBookingId());
                        param.put("lang_token",sharedPreferences.getString("lang",""));

                        Log.e("sendPdfPara",""+param);
                        new PostMethod(Api.CheckingToSendPdfStudent,param,context).startPostMethod(new ResponseData() {
                            @Override
                            public void response(String data) {
                                Log.e("sendPdfResult",data);
                            }

                            @Override
                            public void error(VolleyError error) {
                                error.printStackTrace();
                            }
                        });
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return upcomingModalArrayList.size();
    }

    public class RowHolder extends RecyclerView.ViewHolder {
        ImageView profilePic,headerPic;
        TextView name,sessionType,about,days,hours,mins,seconds,dateTime,totalHours,languages;
        CardView parentCardView,attend;

        public RowHolder(@NonNull View itemView) {
            super(itemView);
            attend=itemView.findViewById(R.id.attend);
            headerPic=itemView.findViewById(R.id.headerPic);
            profilePic=itemView.findViewById(R.id.profile_pic);
            name=itemView.findViewById(R.id.name);
            sessionType=itemView.findViewById(R.id.sessionType);
            about=itemView.findViewById(R.id.about);
            dateTime=itemView.findViewById(R.id.dateTime);
            totalHours=itemView.findViewById(R.id.totalHours);
            languages=itemView.findViewById(R.id.languages);

            parentCardView = itemView.findViewById(R.id.parentCardView);
            days = itemView.findViewById(R.id.days);
            hours = itemView.findViewById(R.id.hours);
            mins = itemView.findViewById(R.id.minutes);
            seconds = itemView.findViewById(R.id.seconds);
        }
    }
}
