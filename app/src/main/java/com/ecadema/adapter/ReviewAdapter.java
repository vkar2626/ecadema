package com.ecadema.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ecadema.app.R;
import com.ecadema.modal.ReviewModal;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.RowHolder> {
    private final Context context;
    private final ArrayList<ReviewModal> reviewModalArrayList;

    public ReviewAdapter(Context context, ArrayList<ReviewModal> reviewModalArrayList) {
        this.context=context;
        this.reviewModalArrayList=reviewModalArrayList;
    }

    @NonNull
    @Override
    public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.review_layout,null);
        return new RowHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RowHolder holder, int position) {
        ReviewModal reviewModal=reviewModalArrayList.get(position);
        Glide.with(context).load(reviewModal.getImage()).into(holder.image);
        holder.userName.setText(reviewModal.getName());
        holder.comment.setText(reviewModal.getReview());
    }

    @Override
    public int getItemCount() {
        return reviewModalArrayList.size();
    }

    public class RowHolder extends RecyclerView.ViewHolder {
        CircleImageView image;
        TextView userName,comment;
        public RowHolder(@NonNull View itemView) {
            super(itemView);
            image=itemView.findViewById(R.id.image);
            userName=itemView.findViewById(R.id.userName);
            comment=itemView.findViewById(R.id.comment);
        }
    }
}
