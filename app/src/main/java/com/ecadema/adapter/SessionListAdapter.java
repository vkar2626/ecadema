
package com.ecadema.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ecadema.app.MainActivity;
import com.ecadema.app.R;
import com.ecadema.fragments.SessionDetails;
import com.ecadema.modal.UpcomingModal;

import java.util.ArrayList;

public class SessionListAdapter extends RecyclerView.Adapter<SessionListAdapter.RowHolder> {
    private final Context context;
    private final ArrayList<UpcomingModal> educationArrayList;

    public SessionListAdapter(Context context, ArrayList<UpcomingModal> educationArrayList) {
        this.context=context;
        this.educationArrayList=educationArrayList;
    }

    @NonNull
    @Override
    public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.education_layout,null);
        return new RowHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RowHolder holder, int position) {
        holder.education.setText(educationArrayList.get(position).getAbout());
        //holder.imageView.setVisibility(View.VISIBLE);
        UpcomingModal gsModal = educationArrayList.get(position);
        holder.education.setOnClickListener(v->{
            Bundle bundle=new Bundle();
            bundle.putString("sessionId",gsModal.getID());
            bundle.putString("trainerID",gsModal.getTeacherID());
            SessionDetails sessionDetails=new SessionDetails();
            sessionDetails.setArguments(bundle);
            ((MainActivity) context).addFragment2(sessionDetails, true, false, 0);
        });
    }

    @Override
    public int getItemCount() {
        return educationArrayList.size();
    }

    public class RowHolder extends RecyclerView.ViewHolder {
        TextView education;
        ImageView imageView;
        public RowHolder(@NonNull View itemView) {
            super(itemView);
            education=itemView.findViewById(R.id.education);
            imageView=itemView.findViewById(R.id.imageView);
        }
    }
}
