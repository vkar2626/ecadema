package com.ecadema.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ecadema.app.R;
import com.ecadema.modal.IconModal;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class SkillAdapter extends RecyclerView.Adapter<SkillAdapter.RowHolder> {
    private final Context context;
    private final ArrayList<IconModal> iconModalArrayList;

    public SkillAdapter(Context context, ArrayList<IconModal> iconModalArrayList) {
        this.context=context;
        this.iconModalArrayList=iconModalArrayList;
    }

    @NonNull
    @Override
    public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.icons,null);
        return new RowHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RowHolder holder, int position) {
        IconModal iconModal = iconModalArrayList.get(position);

        Log.e("Image",""+iconModal.getImage());
        try{
            Picasso.with(context).load(iconModal.getImage()).into(holder.icon);
            holder.count.setText(iconModal.getCount());

        }catch (Exception e){
            e.printStackTrace();
        }
        holder.name.setText(iconModal.getName());

    }

    @Override
    public int getItemCount() {
        return iconModalArrayList.size();
    }

    public class RowHolder extends RecyclerView.ViewHolder {
        ImageView icon;
        TextView name,count;
        RelativeLayout mainLayout;
        public RowHolder(@NonNull View itemView) {
            super(itemView);
            mainLayout=itemView.findViewById(R.id.mainLayout);
            icon=itemView.findViewById(R.id.icon);
            name=itemView.findViewById(R.id.name);
            count=itemView.findViewById(R.id.count);

        }
    }
}
