package com.ecadema.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ecadema.app.R;
import com.ecadema.modal.TagsModal;

import java.util.ArrayList;

public class TagsAdapterNew extends RecyclerView.Adapter<TagsAdapterNew.RowHolder> {

    private final Context context;
    private final ArrayList<TagsModal> tagsList;
    private ArrayList<String> checkedValues= new ArrayList<>();
    private String check_value;

    public TagsAdapterNew(Context context, ArrayList<TagsModal> tagsList) {
        this.context=context;
        this.tagsList=tagsList;
    }

    @NonNull
    @Override
    public TagsAdapterNew.RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.checkbox_layout,null);
        return new TagsAdapterNew.RowHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TagsAdapterNew.RowHolder holder, int position) {
        TagsModal languageModal=tagsList.get(position);
        holder.checkbox.setText(languageModal.getTagName());

        holder.checkbox.setChecked(tagsList.get(position).isSelected());
        holder.checkbox.setTag(tagsList.get(position));

        holder.checkbox.setOnClickListener(v -> {
            CheckBox cb = (CheckBox) v;
            TagsModal languageModal1 = (TagsModal) cb.getTag();

            languageModal1.setSelected(cb.isChecked());

            check_value=languageModal1.getTagId();
            Intent intent=new Intent("tagBroadCastNew");
            intent.putExtra("filterType","tag");
            if(cb.isChecked()) {
                checkedValues.add(check_value);
                String csv = android.text.TextUtils.join(",", checkedValues);
                Log.e("checkedValues", String.valueOf(csv));
                intent.putExtra("tagLoc",csv);
            }
            else {
                checkedValues.remove(check_value);
                String csv = android.text.TextUtils.join(",", checkedValues);
                Log.e("checkedValuesRem", String.valueOf(csv));
                intent.putExtra("tagLoc",csv);
            }
            context.sendBroadcast(intent);

        });

        holder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Intent intent=new Intent("tagBroadCastNew");
                intent.putExtra("filterType","tag");
                if(b){
                    intent.putExtra("tagLoc",languageModal.getTagId());
                    intent.putExtra("type","add");
                }else {
                    intent.putExtra("tagLoc",languageModal.getTagId());
                    intent.putExtra("type","remove");
                }

                context.sendBroadcast(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return tagsList.size();
    }

    public class RowHolder extends RecyclerView.ViewHolder {
        CheckBox checkbox;
        public RowHolder(@NonNull View itemView) {
            super(itemView);

            checkbox=itemView.findViewById(R.id.checkbox);
        }
    }
}