package com.ecadema.adapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.PostMethod;
import com.ecadema.apiMethods.ResponseData;
import com.ecadema.app.ChatRoom;
import com.ecadema.app.LoginActivity;
import com.ecadema.app.R;
import com.ecadema.modal.TeammateModal;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class TeammateAdapter extends RecyclerView.Adapter<TeammateAdapter.RowHolder> {
    private final Context context;
    private final ArrayList<TeammateModal> teammateModalArrayList;

    public TeammateAdapter(Context context, ArrayList<TeammateModal> teammateModalArrayList) {
        this.context=context;
        this.teammateModalArrayList=teammateModalArrayList;
    }

    @NonNull
    @Override
    public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.teammate_adapter,null);
        return new RowHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(@NonNull RowHolder holder, int position) {
        TeammateModal teammateModal=teammateModalArrayList.get(position);

        holder.name.setText(teammateModal.getName());
        holder.email.setText(teammateModal.getEmail());

        if (teammateModal.getMobile().equalsIgnoreCase(""))
            holder.mobile.setText("-");
        else
            holder.mobile.setText(teammateModal.getMobile());

        if (teammateModal.getDepartment().equalsIgnoreCase(""))
            holder.department.setText("-");
        else
            holder.department.setText(teammateModal.getDepartment());

        if (teammateModal.getSeniority().equalsIgnoreCase(""))
            holder.seniority.setText("-");
        else
            holder.seniority.setText(teammateModal.getSeniority());

        Picasso.with(context).load(teammateModal.getImage()).into(holder.profile_pic);

        holder.chat.setOnClickListener(v->{
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            if (sharedPreferences.getString("userID","").equalsIgnoreCase("-1")){
                Intent intent = new Intent(context, LoginActivity.class);
                context.startActivity(intent);
                return;
            }

            Intent intent=new Intent(context, ChatRoom.class);
            intent.putExtra("chatID","");
            intent.putExtra("userName",teammateModal.getName());
            intent.putExtra("receiverID",teammateModal.getId());
            intent.putExtra("userImage",teammateModal.getImage());
            intent.putExtra("type",teammateModal.getUserType());
            if(null!=teammateModal.getCourse())
                intent.putExtra("courses",teammateModal.getCourse().toString());
            context.startActivity(intent);
        });
        holder.remove.setOnClickListener(v->{
            androidx.appcompat.app.AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
            LayoutInflater inflater1 = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            @SuppressLint("InflateParams") final View dialogView = inflater1.inflate(R.layout.teammate_dialog, null);
            alertDialog.setView(dialogView);
            alertDialog.setCancelable(false);
            Dialog dialog=alertDialog.create();
            ImageView close=dialogView.findViewById(R.id.close);
            TextView cancel=dialogView.findViewById(R.id.cancel);
            CardView deactivate=dialogView.findViewById(R.id.deactivate);

            deactivate.setOnClickListener(v1->{
                removeTeammate(dialog,teammateModal.getTeammateId(),holder.getAbsoluteAdapterPosition());
                dialog.dismiss();

            });

            close.setOnClickListener(v1->dialog.dismiss());
            cancel.setOnClickListener(v1->dialog.dismiss());

            dialog.create();
            dialog.show();
        });
    }

    private void removeTeammate(Dialog dialog, String id, int absoluteAdapterPosition) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();
        Map<String,String> param=new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));
        param.put("removed_id",id);
        param.put("status","3");
        Log.e("removeTeammatePara",""+param);

        new PostMethod(Api.RemoveTeammates,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                progressDialog.cancel();;
                Log.e("removeTeammate",data);
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    if(jsonObject.optBoolean("status")){
                        dialog.dismiss();
                        teammateModalArrayList.remove(absoluteAdapterPosition);
                        notifyDataSetChanged();
                    }
                    Toast.makeText(context, jsonObject.optString("message"), Toast.LENGTH_SHORT).show();

                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();;
                error.printStackTrace();
            }
        });
    }

    @Override
    public int getItemCount() {
        return teammateModalArrayList.size();
    }

    public class RowHolder extends RecyclerView.ViewHolder {
        TextView remove,chat,name,email,mobile,department,seniority;
        CircleImageView profile_pic;

        public RowHolder(@NonNull View itemView) {
            super(itemView);

            seniority=itemView.findViewById(R.id.seniority);
            department=itemView.findViewById(R.id.department);
            remove=itemView.findViewById(R.id.remove);
            chat=itemView.findViewById(R.id.chat);
            name=itemView.findViewById(R.id.name);
            email=itemView.findViewById(R.id.email);
            mobile=itemView.findViewById(R.id.mobile);
            profile_pic=itemView.findViewById(R.id.profile_pic);
        }
    }
}
