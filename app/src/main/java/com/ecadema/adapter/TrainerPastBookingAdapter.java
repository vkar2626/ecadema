package com.ecadema.adapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.PostMethod;
import com.ecadema.apiMethods.ResponseData;
import com.ecadema.app.R;
import com.ecadema.modal.IconModal;
import com.ecadema.modal.PastModal;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TrainerPastBookingAdapter extends RecyclerView.Adapter<TrainerPastBookingAdapter.RowHolder> {
    private final Context context;
    private final ArrayList<PastModal> pastModalArrayList;


    ArrayList<IconModal> iconModalArrayList = new ArrayList<>();
    ArrayList<String> selectedSkills= new ArrayList<>();
    SharedPreferences sharedPreferences;
    String starRating="", teacherID="",bookingID="";

    BroadcastReceiver skills =new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(selectedSkills.contains(intent.getStringExtra("id")))
                selectedSkills.remove(intent.getStringExtra("id"));
            else
                selectedSkills.add(intent.getStringExtra("id"));

            Log.e("TAG", "classAdapter: "+ selectedSkills.toString());
        }
    };

    public TrainerPastBookingAdapter(Context context, ArrayList<PastModal> pastModalArrayList) {
        this.context=context;
        this.pastModalArrayList=pastModalArrayList;

        context.registerReceiver(skills,new IntentFilter("myClass1"));
        sharedPreferences= PreferenceManager.getDefaultSharedPreferences(context);
        getAllRatings();
    }

    private void AddRatings(Dialog dialog, String comment, String starRating, String teacherID, String id) {
        String csv = android.text.TextUtils.join(",", selectedSkills);

        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();
        Map<String,String > param = new HashMap<>();
        param.put("teacher_id",teacherID);
        param.put("student_id",sharedPreferences.getString("userID",""));
        param.put("rating",starRating);
        param.put("comment",comment);
        param.put("review_type","one-to-one");
        param.put("skill",csv);
        param.put("booking_id",id);

        Log.e("addRatingParam",""+param);
        new PostMethod(Api.AddReviews,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                progressDialog.cancel();;
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    if(jsonObject.optBoolean("status")){
                        Intent intent = new Intent("updateMyClassPast");
                        context.sendBroadcast(intent);
                        dialog.dismiss();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();;
                error.printStackTrace();
            }
        });
    }

    private void getAllRatings() {
        Map<String,String> param=new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));

        new PostMethod(Api.TraineerSkills,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    JSONArray dataArray = jsonObject.optJSONArray("data");
                    for (int ar=0;ar<dataArray.length();ar++){
                        JSONObject object = dataArray.getJSONObject(ar);
                        IconModal iconModal = new IconModal();
                        iconModal.setCount(object.optString("id"));
                        iconModal.setName(object.optString("skill"));
                        iconModal.setId(object.optString("id"));
                        iconModal.setImage(object.optString("icon_url"));
                        iconModalArrayList.add(iconModal);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                error.printStackTrace();
            }
        });
    }

    @NonNull
    @Override
    public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.trainer_booking_adapter,null);
        return new RowHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RowHolder holder, int position) {
        PastModal pastModal=pastModalArrayList.get(position);
        String text = "<font color=#f16667>"+context.getResources().getString(R.string.For)+" </font> <font color=#757575>"+pastModal.getName()+"</font>";

        if (pastModal.getBookingType().equalsIgnoreCase("for_other")){
            holder.sessionType.setText(pastModal.getSessionType());
            holder.dateTime.setText(pastModal.getDateTime());
            holder.rate.setText("$ "+pastModal.getRate()+" /"+context.getResources().getString(R.string.hour));
            holder.duration.setText(pastModal.getDuration()+" "+context.getResources().getString(R.string.hours));

            holder.name.setText(Html.fromHtml(text));
            holder.sessionName.setText(pastModal.getSessionName());
            holder.ll1.setVisibility(View.GONE);
            holder.ll2.setVisibility(View.GONE);
            holder.rating.setVisibility(View.VISIBLE);
            holder.forMe.setVisibility(View.GONE);
            holder.forOthers.setVisibility(View.VISIBLE);
            //Glide.with(context).load(pastModal.getTagUrl()).into(holder.forOthersTag);
        }else {
            /** for me**/
            //Glide.with(context).load(pastModal.getTagUrl()).into(holder.forMeTag);
            holder.forMe.setVisibility(View.VISIBLE);
            holder.forOthers.setVisibility(View.GONE);

            holder.sessionType1.setText(pastModal.getSessionType());
            holder.dateTime1.setText(pastModal.getDateTime());
            holder.rate1.setText("$ "+pastModal.getRate()+" /"+context.getResources().getString(R.string.hour));
            holder.duration1.setText(pastModal.getDuration()+" "+context.getResources().getString(R.string.hours));

            holder.name1.setText(Html.fromHtml(text));
            holder.ll11.setVisibility(View.GONE);
            holder.ll21.setVisibility(View.GONE);
            holder.rating1.setVisibility(View.VISIBLE);

            if(pastModal.getRateByTrainee().equalsIgnoreCase("1")){
                holder.rating1.setText(context.getResources().getString(R.string.rated));
            }

            holder.rating1.setTag(position);
            holder.rating1.setOnClickListener(v->{
                int pos = (int)v.getTag();
                teacherID= pastModalArrayList.get(pos).getTeacherID();
                bookingID= pastModalArrayList.get(pos).getID();

                if (!holder.rating1.getText().toString().equalsIgnoreCase(context.getResources().getString(R.string.rated))){
                    androidx.appcompat.app.AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
                    LayoutInflater inflater1 =
                            (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    @SuppressLint("InflateParams") final View dialogView =
                            inflater1.inflate(R.layout.feedback_dialog, null);
                    alertDialog.setView(dialogView);
                    alertDialog.setCancelable(false);
                    Dialog dialog=alertDialog.create();
                    ImageView close=dialogView.findViewById(R.id.close);
                    RecyclerView skills=dialogView.findViewById(R.id.skills);
                    CardView update=dialogView.findViewById(R.id.update);
                    RatingBar ratings = dialogView.findViewById(R.id.ratings);
                    EditText comment = dialogView.findViewById(R.id.comment);


                    skills.setLayoutManager(new GridLayoutManager(context,3));
                    IconAdapter iconAdapter= new IconAdapter(context,iconModalArrayList,"myClass1");
                    skills.setAdapter(iconAdapter);
                    update.setOnClickListener(v1->{
                        starRating=""+ratings.getRating();
                    /*if(comment.getText().toString().trim().matches("")){
                        comment.setError(context.getResources().getString(R.string.fieldRequired));
                        return;
                    }*/
                        if(selectedSkills.size()==0){
                            Toast.makeText(context, context.getResources().getString(R.string.selectOneSkill), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        AddRatings(dialog,comment.getText().toString(),starRating,teacherID,bookingID);
                    });

                    close.setOnClickListener(v1->dialog.dismiss());
                    //cancel.setOnClickListener(v1->dialog.dismiss());

                    dialog.create();
                    dialog.show();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return pastModalArrayList.size();
    }

    public class RowHolder extends RecyclerView.ViewHolder {
        ImageView profilePic;
        TextView name,sessionType,sessionName,rating,dateTime,duration,rate;
        LinearLayout ll1,ll2;

        ImageView profilePic1;
        TextView name1,sessionType1,rating1,dateTime1,duration1,rate1;
        LinearLayout ll11,ll21;

        LinearLayout forMe,forOthers;
        ImageView forMeTag,forOthersTag;

        public RowHolder(@NonNull View itemView) {
            super(itemView);

            forMeTag=itemView.findViewById(R.id.forMeTag);
            forOthersTag=itemView.findViewById(R.id.forOthersTag);
            forOthers=itemView.findViewById(R.id.forOthers);
            forMe=itemView.findViewById(R.id.forMe);

            dateTime=itemView.findViewById(R.id.dateTime);
            rate =itemView.findViewById(R.id.rate);
            duration=itemView.findViewById(R.id.duration);
            profilePic=itemView.findViewById(R.id.profile_pic);
            name=itemView.findViewById(R.id.name);
            sessionType=itemView.findViewById(R.id.sessionType);
            ll1=itemView.findViewById(R.id.ll1);
            ll2=itemView.findViewById(R.id.ll2);
            sessionName=itemView.findViewById(R.id.sessionName);
            rating=itemView.findViewById(R.id.rating);

            dateTime1=itemView.findViewById(R.id.dateTime1);
            rate1 =itemView.findViewById(R.id.rate1);
            duration1=itemView.findViewById(R.id.duration1);
            profilePic1=itemView.findViewById(R.id.profile_pic1);
            name1=itemView.findViewById(R.id.name1);
            sessionType1=itemView.findViewById(R.id.sessionType1);
            ll11=itemView.findViewById(R.id.ll11);
            ll21=itemView.findViewById(R.id.ll21);
            rating1=itemView.findViewById(R.id.rating1);
        }
    }
}
