package com.ecadema.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ecadema.app.R;
import com.ecadema.modal.SubmittedModal;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class TrainerSubmittedAdapter extends RecyclerView.Adapter<TrainerSubmittedAdapter.RowHolder> {
    private final Context context;
    private final ArrayList<SubmittedModal> submittedModalArrayList;

    public TrainerSubmittedAdapter(Context context, ArrayList<SubmittedModal> submittedModalArrayList) {
        this.context=context;
        this.submittedModalArrayList=submittedModalArrayList;
    }

    @NonNull
    @Override
    public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.trainer_session_adapter,null);
        return new RowHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RowHolder holder, int position) {
        SubmittedModal submittedModal=submittedModalArrayList.get(position);
        String text = "<font color=#f16667>"+context.getResources().getString(R.string.with)+" </font> <font color=#757575>"+submittedModal.getName()+"</font>";

        holder.sessionType.setText(submittedModal.getSessionType());
        holder.name.setText(Html.fromHtml(text));
        holder.sessionName.setText(submittedModal.getSessionName());

        holder.duration.setText(submittedModal.getDuration()+" "+context.getResources().getString(R.string.hours));
        holder.date_time.setText(submittedModal.getDateTime());
        holder.rate.setText("$ "+submittedModal.getRate());

        try {
            Picasso.with(context).load(submittedModal.getBanner()).into(holder.headerPic);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return submittedModalArrayList.size();
    }

    public class RowHolder extends RecyclerView.ViewHolder {
        ImageView profilePic,headerPic;
        TextView name,sessionType,sessionName,rate,duration,date_time;
        public RowHolder(@NonNull View itemView) {
            super(itemView);

            headerPic=itemView.findViewById(R.id.headerPic);
            date_time=itemView.findViewById(R.id.date_time);
            duration=itemView.findViewById(R.id.duration);
            rate=itemView.findViewById(R.id.rate);
            profilePic=itemView.findViewById(R.id.profile_pic);
            name=itemView.findViewById(R.id.name);
            sessionType=itemView.findViewById(R.id.sessionType);
            sessionName=itemView.findViewById(R.id.sessionName);
        }
    }
}
