package com.ecadema.apiMethods;

import com.android.volley.VolleyError;

public interface ResponseData {

    void response(String data);

    void error(VolleyError error);
}
