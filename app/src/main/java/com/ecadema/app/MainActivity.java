package com.ecadema.app;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.VolleyError;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.PostMethod;
import com.ecadema.apiMethods.ResponseData;
import com.ecadema.fragments.CalendarFragment;
import com.ecadema.fragments.ChatFragment;
import com.ecadema.fragments.CommunityFragment;
import com.ecadema.fragments.HomeFragment;
import com.ecadema.fragments.MyClassesFragment;
import com.ecadema.fragments.SettingsFragment;
import com.ecadema.fragments.SupportFragment;
import com.ecadema.fragments.TrainerProfileFragment;
import com.ecadema.fragments.ViewProfile;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.InstallStateUpdatedListener;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.InstallStatus;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.review.ReviewInfo;
import com.google.android.play.core.review.ReviewManager;
import com.google.android.play.core.review.ReviewManagerFactory;
import com.google.android.play.core.tasks.Task;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity {
    public BottomNavigationView bottom;
    SharedPreferences sharedPreferences;
    Context context;
    ImageView profile_pic, homeIcon;
    CircleImageView profile_image;
    String currentVersion = "", latestversion = "";

    private ReviewManager reviewManager;

    private AppUpdateManager appUpdateManager;
    private InstallStateUpdatedListener installStateUpdatedListener;
    private static final int FLEXIBLE_APP_UPDATE_REQ_CODE = 123;

    @SuppressLint("HardwareIds")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = this;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        setAppLocale(sharedPreferences.getString("lang", ""));

        setContentView(R.layout.activity_main);
        getTime("12:00:00");
        String android_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.e("device_id",android_id);
        sharedPreferences.edit().putString("device_id",android_id).apply();

        reviewManager = ReviewManagerFactory.create(this);


        homeIcon = findViewById(R.id.homeIcon);
        bottom = findViewById(R.id.nav_view);
        profile_image = findViewById(R.id.profile_image);
        profile_pic = findViewById(R.id.profile_pic);
        bottom.setOnNavigationItemSelectedListener(bottomNavigationItemSelectedListener);

        profile_image.setOnClickListener(v -> {
            /*Intent intent=new Intent(context,SettingsActivity.class);
            startActivity(intent);*/
            addFragment2(new SettingsFragment(), true, false, 0);
        });
        homeIcon.setOnClickListener(v -> bottom.setSelectedItemId(R.id.navigation_home));

        addFragment2(new HomeFragment(), true, false, 0);

        try {
            if (getIntent().getStringExtra("open").equalsIgnoreCase("profile")) {
                Bundle bundle = new Bundle();
                bundle.putString("trainerID", getIntent().getStringExtra("trainerID"));
                ViewProfile viewProfile = new ViewProfile();
                viewProfile.setArguments(bundle);
                addFragment2(viewProfile, true, false, 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (getIntent().getStringExtra("type").equalsIgnoreCase("Reschedule")) {
                bottom.setSelectedItemId(R.id.navigation_classes);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (getIntent().getStringExtra("open").equalsIgnoreCase("booking")) {
                Bundle bundle = new Bundle();
                bundle.putString("trainerID", getIntent().getStringExtra("trainerID"));
                bundle.putString("name", getIntent().getStringExtra("name"));
                bundle.putString("courseID", getIntent().getStringExtra("courseID"));
                bundle.putString("imageurl", getIntent().getStringExtra("userImage"));
                bundle.putString("ticketPrice", getIntent().getStringExtra("ticketPrice"));
                bundle.putString("from", "booking");
                CalendarFragment calendarFragment = new CalendarFragment();
                calendarFragment.setArguments(bundle);
                FragmentManager fragmentManager = ((MainActivity) context).getSupportFragmentManager();
                FragmentTransaction ft = fragmentManager.beginTransaction();
                ft.replace(R.id.frag_container, calendarFragment, calendarFragment.getClass().getSimpleName());
                ft.addToBackStack(null);
                ft.commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

       /* PendingIntent pendingResult = createPendingResult(0, new Intent(), 0);
        Intent intent = new Intent(getApplicationContext(), GetVersionCode.class);
        intent.putExtra("pending_result", pendingResult);
        startService(intent);*/

    }

    private void setAppLocale(String LocaleCode) {
        Resources res = getResources();
        DisplayMetrics dr = res.getDisplayMetrics();
        Configuration configuration = res.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            configuration.setLocale(new Locale(LocaleCode.toLowerCase()));
        } else {
            configuration.locale = new Locale(LocaleCode.toLowerCase());
        }
        res.updateConfiguration(configuration, dr);
    }

    public void showRateApp() {
        Task<ReviewInfo> request = reviewManager.requestReviewFlow();
        request.addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                // Getting the ReviewInfo object
                ReviewInfo reviewInfo = task.getResult();

                Task<Void> flow = reviewManager.launchReviewFlow(this, reviewInfo);
                flow.addOnCompleteListener(task1 -> {
                    //Toast.makeText(context, "flow finished", Toast.LENGTH_SHORT).show();
                    // The flow has finished. The API does not indicate whether the user
                    // reviewed or not, or even whether the review dialog was shown.
                });
            }else {
                // There was some problem, continue regardless of the result.
                // show native rate app dialog on error
                showRateAppFallbackDialog();
            }
        });
    }

    private void showRateAppFallbackDialog() {
        new MaterialAlertDialogBuilder(this)
            .setTitle(R.string.rate_app_title)
            .setMessage(R.string.rate_app_message)
            .setPositiveButton(R.string.rate_btn_pos, (dialog, which) -> {
                try {
                    Intent viewIntent =
                        new Intent("android.intent.action.VIEW",
                            Uri.parse("https://play.google.com/store/apps/details?id=com.ecadema.app"));
                    startActivity(viewIntent);
                }catch(Exception e) {
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.unableToConnect),
                        Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            })
            .setNegativeButton(R.string.rate_btn_neg,
                (dialog, which) -> {
                })
            .setNeutralButton(R.string.rate_btn_nut,
                (dialog, which) -> {
                })
            .setOnDismissListener(dialog -> {
            })
            .show();
    }
    public void getChatCount() {

        Map<String, String> param = new HashMap<>();
        param.put("lang_token", sharedPreferences.getString("lang", ""));
        param.put("user_id", sharedPreferences.getString("userID", ""));

        new PostMethod(Api.UserUnReadMsgCount, param, context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                Log.e("ureadMsgs", data);
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    if (!jsonObject.optString("data").equalsIgnoreCase("0")) {
                        bottom.getOrCreateBadge(R.id.navigation_chat).setNumber(jsonObject.optInt("data"));
                        bottom.getOrCreateBadge(R.id.navigation_chat).setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                        bottom.getOrCreateBadge(R.id.navigation_chat).setBadgeTextColor(getResources().getColor(R.color.white));
                    } else
                        bottom.removeBadge(R.id.navigation_chat);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                error.printStackTrace();
            }
        });
    }

    public void showProfilePic() {
        if (!sharedPreferences.getString("profile_image", "").equalsIgnoreCase("")) {
            //Picasso.with(context).load(sharedPreferences.getString("profile_image","")).into(profile_pic);
            Picasso.with(context).load(sharedPreferences.getString("profile_image", "")).noFade().into(profile_image);
        } else {
            //profile_pic.setImageDrawable(getResources().getDrawable(R.drawable.guest_user));
            profile_image.setImageDrawable(getResources().getDrawable(R.drawable.guest_user));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        showProfilePic();
        if (sharedPreferences.getString("user_type", "").equalsIgnoreCase("2"))
            bottom.getMenu().getItem(1).setTitle(getResources().getString(R.string.sessions));
        else if (sharedPreferences.getString("user_type", "").equalsIgnoreCase("3"))
            bottom.getMenu().getItem(1).setTitle(getResources().getString(R.string.sessions));

        getChatCount();
        context.registerReceiver(rating, new IntentFilter("rating"));
        context.registerReceiver(update, new IntentFilter("mainActivity"));
        context.registerReceiver(CalendarBroadcast, new IntentFilter("Calendar"));

        try {
            if (latestversion != null && !latestversion.isEmpty()) {
                if (Float.parseFloat(currentVersion) < Float.parseFloat(latestversion)) {
                    AlertDialog.Builder alertbuilder = new AlertDialog.Builder(context);
                    alertbuilder.setCancelable(false);
                    alertbuilder.setTitle("Recommendation !!!");
                    alertbuilder.setIcon(getResources().getDrawable(R.mipmap.app_icon));
                    alertbuilder.setMessage("Please update the app for better experience.");
                    alertbuilder.setPositiveButton("Update",
                            (dialog, which) -> {
                                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                try {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                } catch (android.content.ActivityNotFoundException anfe) {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                }

                            });
                                    /*alertbuilder.setNegativeButton("No Thanks",
                                            (dialog, which) -> dialog.dismiss());*/
                    AlertDialog alertDialog = alertbuilder.create();
                    alertDialog.show();
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    BroadcastReceiver rating = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            showRateApp();
        }
    };
    BroadcastReceiver update = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            getChatCount();
        }
    };
    BroadcastReceiver CalendarBroadcast = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = new Bundle();
            bundle.putString("trainerID", intent.getStringExtra("trainerID"));
            bundle.putString("name", intent.getStringExtra("name"));
            bundle.putString("courseID", intent.getStringExtra("courseID"));
            bundle.putString("imageurl", intent.getStringExtra("userImage"));
            bundle.putString("ticketPrice", intent.getStringExtra("ticketPrice"));
            bundle.putString("from", "booking");
            unregisterReceiver(CalendarBroadcast);
            CalendarFragment calendarFragment = new CalendarFragment();
            calendarFragment.setArguments(bundle);
            FragmentManager fragmentManager = ((MainActivity) context).getSupportFragmentManager();
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.replace(R.id.frag_container, calendarFragment, calendarFragment.getClass().getSimpleName());
            ft.addToBackStack(null);
            ft.commit();
        }
    };

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frag_container);
        if (fragment instanceof HomeFragment && fragment.isVisible()) {
            finish();
        } else if (fragment instanceof ChatFragment && fragment.isVisible()) {
            bottom.setSelectedItemId(R.id.navigation_home);
        } else if (fragment instanceof CommunityFragment && fragment.isVisible()) {
            bottom.setSelectedItemId(R.id.navigation_home);
        } else if (fragment instanceof MyClassesFragment && fragment.isVisible()) {
            bottom.setSelectedItemId(R.id.navigation_home);
        } else if (fragment instanceof SupportFragment && fragment.isVisible()) {
            bottom.setSelectedItemId(R.id.navigation_home);
        } else if (fragment instanceof SettingsFragment && fragment.isVisible()) {
            bottom.setSelectedItemId(R.id.navigation_home);
            showBottomMenu();
        } else if (fragment instanceof TrainerProfileFragment && fragment.isVisible()) {
            Intent intent = new Intent("exit");
            sendBroadcast(intent);
        } else
            super.onBackPressed();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener bottomNavigationItemSelectedListener = item -> {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frag_container);

        switch (item.getItemId()) {
            case R.id.navigation_home:
                if (fragment instanceof HomeFragment && fragment.isVisible()) {
                    return true;
                }
                addFragment2(new HomeFragment(), true, false, 0);
                return true;
            case R.id.navigation_classes:
                if (sharedPreferences.getString("userID", "").equalsIgnoreCase("-1")) {
                    bottom.setSelectedItemId(R.id.navigation_home);
                    Intent intent = new Intent(context, LoginActivity.class);
                    startActivity(intent);
                    return false;
                }

                if (fragment instanceof MyClassesFragment && fragment.isVisible()) {
                    return true;
                }
                addFragment2(new MyClassesFragment(), true, false, 0);

                return true;
            case R.id.navigation_chat:
                if (sharedPreferences.getString("userID", "").equalsIgnoreCase("-1")) {
                    Intent intent = new Intent(context, LoginActivity.class);
                    startActivity(intent);
                    bottom.setSelectedItemId(R.id.navigation_home);
                    return false;
                }

                if (fragment instanceof ChatFragment && fragment.isVisible()) {
                    return true;
                }
                addFragment2(new ChatFragment(), false, false, 0);

                return true;
            case R.id.navigation_blog:

                /*if(sharedPreferences.getString("userID","").equalsIgnoreCase("-1")){
                    Intent intent=new Intent(context, LoginActivity.class);
                    startActivity(intent);
                    return true;
                }*/
                if (fragment instanceof CommunityFragment && fragment.isVisible()) {
                    return true;
                }

                addFragment2(new CommunityFragment(), true, false, 0);

                return true;
            case R.id.navigation_support:

                if (fragment instanceof SupportFragment && fragment.isVisible()) {
                    return true;
                }
                addFragment2(new SupportFragment(), true, false, 0);
                return true;
        }
        return false;
    };

    public void addFragment2(Fragment f, boolean addBackstack, boolean clearAll, int pos) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.frag_container, f, f.getClass().getSimpleName());
        ft.addToBackStack(f.getClass().getSimpleName());
        ft.commit();
    }

    public void showBottomMenu() {
        bottom.setVisibility(View.VISIBLE);
    }

    public void hideBottomMenu() {
        bottom.setVisibility(View.GONE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }

        if (requestCode == FLEXIBLE_APP_UPDATE_REQ_CODE) {
            if (resultCode == RESULT_CANCELED) {
                //Toast.makeText(getApplicationContext(), "Update canceled by user! Result Code: " + resultCode, Toast.LENGTH_LONG).show();
            } else if (resultCode == RESULT_OK) {
                //Toast.makeText(getApplicationContext(),"Update success! Result Code: " + resultCode, Toast.LENGTH_LONG).show();
            } else {
                //Toast.makeText(getApplicationContext(), "Update Failed! Result Code: " + resultCode, Toast.LENGTH_LONG).show();
                checkUpdate();
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        checkUpdate();
    }

    private void checkUpdate() {

        appUpdateManager = AppUpdateManagerFactory.create(getApplicationContext());
        installStateUpdatedListener = state -> {
            if (state.installStatus() == InstallStatus.DOWNLOADED) {
                popupSnackBarForCompleteUpdate();
            } else if (state.installStatus() == InstallStatus.INSTALLED) {
                removeInstallStateUpdateListener();
            } else {
                //Toast.makeText(getApplicationContext(), "InstallStateUpdatedListener: state: " + state.installStatus(), Toast.LENGTH_LONG).show();
            }
        };
        appUpdateManager.registerListener(installStateUpdatedListener);

        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                    && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {
                startUpdateFlow(appUpdateInfo);
            } else if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED) {
                popupSnackBarForCompleteUpdate();
            }
        });
    }

    private void startUpdateFlow(AppUpdateInfo appUpdateInfo) {
        try {
            appUpdateManager.startUpdateFlowForResult(appUpdateInfo, AppUpdateType.IMMEDIATE,
                    this, FLEXIBLE_APP_UPDATE_REQ_CODE);
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }
    }

    private void popupSnackBarForCompleteUpdate() {
        AlertDialog.Builder alertbuilder = new AlertDialog.Builder(context);
        alertbuilder.setCancelable(false);
        alertbuilder.setTitle("Recommendation !!!");
        alertbuilder.setIcon(getResources().getDrawable(R.mipmap.app_icon));
        alertbuilder.setMessage("Please Install the new version of app for better experience.");
        alertbuilder.setPositiveButton("Install", (dialog, which) -> {
            if (appUpdateManager != null) {
                appUpdateManager.completeUpdate();
            }
        });
        AlertDialog alertDialog = alertbuilder.create();
        alertDialog.show();
    }

    private void removeInstallStateUpdateListener() {
        if (appUpdateManager != null) {
            appUpdateManager.unregisterListener(installStateUpdatedListener);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        removeInstallStateUpdateListener();
    }

    public String getTime(String time){
        String dateStr = time;
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = df.parse(dateStr);
            Log.e("Date",""+date);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        df.setTimeZone(TimeZone.getDefault());
        String formattedDate = df.format(date);
        Log.e("formattedDate",formattedDate);

        return formattedDate;
    }
}