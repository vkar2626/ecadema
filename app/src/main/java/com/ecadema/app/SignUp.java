package com.ecadema.app;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.WindowManager;
import android.webkit.URLUtil;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import com.android.volley.VolleyError;
import com.ecadema.EmojiFilter;
import com.ecadema.EmojiFilter1;
import com.ecadema.MultipartUtility;
import com.ecadema.RealPathUtil;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.PostMethod;
import com.ecadema.apiMethods.ResponseData;
import com.ecadema.modal.CountryModal;
import com.google.android.material.snackbar.Snackbar;
import com.yalantis.ucrop.UCrop;
import de.hdodenhof.circleimageview.CircleImageView;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SignUp extends AppCompatActivity {
    ImageView close;
    EditText firstName,lastName,email,password,reEnterPassword,contactNumber;
    AutoCompleteTextView country;
    TextView login,terms_policy_text;
    CardView submit;
    CheckBox checkbox;
    Context context;
    String countyID="-1";
    ArrayList<CountryModal> countryModalArrayList=new ArrayList<>();
    ProgressDialog progressDialog;
    SharedPreferences sharedPreferences;
    Uri mImageCaptureUri;
    File photoFile;
    private static final int PICK_FROM_CAMERA = 1;
    private static final int CROP_IMAGE = 2;
    private static final int PICK_FROM_FILE = 3;
    private android.app.AlertDialog dialog1;

    public String photoFileName="image_" + System.currentTimeMillis()+ ".jpg",profilePicName="",add_edit="add",edit_resume,edit_id_proof;
    private File file;
    private Bitmap thumbnail;
    private File profileFile;
    private String realPath;
    private Bitmap rotatedBitmap;
    CircleImageView profile_image;
    RelativeLayout editProfilePic;
    private String takenFrom;

    @RequiresApi(api = VERSION_CODES.M)
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_sign_up);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        context=this;
        progressDialog=new ProgressDialog(context);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        captureImageInitialization();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkAndRequestPermissions();
        }

        editProfilePic=findViewById(R.id.editProfilePic);
        profile_image=findViewById(R.id.profile_image);
        terms_policy_text=findViewById(R.id.terms_policy_text);
        close=findViewById(R.id.close);
        login=findViewById(R.id.login);
        firstName=findViewById(R.id.firstName);
        checkbox=findViewById(R.id.checkbox);
        lastName=findViewById(R.id.lastName);
        email =findViewById(R.id.email);
        password=findViewById(R.id.password);
        reEnterPassword=findViewById(R.id.reEnterPassword);
        contactNumber=findViewById(R.id.contactNumber);
        country=findViewById(R.id.country);
        submit=findViewById(R.id.submit);
        final String emailPattern = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z]+(\\.[A-Za-z]+)*(\\.[A-Za-z]{2,})$";
;

        String text = "<font color=#8d908f>"+getResources().getString(R.string.Bysigningupyouagreetoour)+"  </font><a href=https://ecadema.com/terms> <font color=#f16667> "
                +getResources().getString(R.string.termss)+"</font></a><font color=#8d908f> "+getResources().getString(R.string.and)+" </font>" +
                "<a href=https://ecadema.com/privacy><font color=#f16667> "+getResources().getString(R.string.privacyPolicyy)+"</font></a>";

        terms_policy_text.setText(Html.fromHtml(text));
        terms_policy_text.setMovementMethod(LinkMovementMethod.getInstance());

        close.setOnClickListener(v-> finish());
        login.setOnClickListener(v-> finish());

        firstName.setFilters(EmojiFilter.getFilter());
        lastName.setFilters(EmojiFilter.getFilter());
        email.setFilters(EmojiFilter1.getFilter());
        password.setFilters(EmojiFilter1.getFilter());
        reEnterPassword.setFilters(EmojiFilter1.getFilter());

        getCountryList();

        editProfilePic.setOnClickListener(v->{
            if(checkAndRequestPermissions()) {
                dialog1.show();
            }
        });

        submit.setOnClickListener(view -> {
            if (firstName.getText().toString().trim().matches("")){
                firstName.setError(getResources().getString(R.string.fieldRequired));
                return;
            }
            if (lastName.getText().toString().trim().matches("")){
                lastName.setError(getResources().getString(R.string.fieldRequired));
                return;
            }
            if (country.getText().toString().trim().matches("")){
                country.setError(getResources().getString(R.string.fieldRequired));
                return;
            }
            if (countyID.equalsIgnoreCase("-1")){
                country.setError(getResources().getString(R.string.fieldRequired));
                return;
            }
            if (email.getText().toString().trim().matches("")){
                email.setError(getResources().getString(R.string.fieldRequired));
                return;
            }
            if (!email.getText().toString().matches(emailPattern)){
                email.setError(getResources().getString(R.string.entervalidemail));
                return;

            }
/*
            else if (!Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()){
                email.setError(getResources().getString(R.string.entervalidemail));
                return;
            }
*/
            if (password.getText().toString().trim().matches("")){
                password.setError(getResources().getString(R.string.fieldRequired));
                return;
            }
            if (password.getText().toString().trim().length()<5){
                password.setError(getResources().getString(R.string.mustBeOf));
                return;
            }
            if (reEnterPassword.getText().toString().trim().matches("")){
                reEnterPassword.setError(getResources().getString(R.string.fieldRequired));
                return;
            }
            if (!password.getText().toString().equalsIgnoreCase(reEnterPassword.getText().toString())){
                reEnterPassword.setError(getResources().getString(R.string.reEnteredPasswordNotMatched));
                return;
            }
            if (!contactNumber.getText().toString().trim().matches("")){
                if(contactNumber.getText().toString().trim().length()<10)
                    contactNumber.setError(getResources().getString(R.string.numberMustBe));
                return;
            }
            if (!checkbox.isChecked()){
                Snackbar.make(checkbox,getResources().getString(R.string.terms), Snackbar.LENGTH_LONG).show();
                return;
            }
            register();

        });

        country.setOnTouchListener((v, event) -> {
            country.showDropDown();
            return true;
        });

        country.setOnItemClickListener((parent, view, position, id) -> {
            String selection = (String) parent.getItemAtPosition(position);
            int pos = -1;
            for (int i = 0; i < countryModalArrayList.size(); i++) {
                if (selection.contains(countryModalArrayList.get(i).getCountryName())) {
                    pos = i;
                    break;
                }
            }
            country.setError("",null);
           countyID=countryModalArrayList.get(pos).getId();
            Log.e("countyID",countyID);
        });
    }

    private void getCountryList() {
        Map<String, String> param=new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));

        Log.e("countryParam",""+param.toString());

        new PostMethod(Api.CountriesList,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                Log.e("countryList",data);
                try {
                    JSONObject jsonObject=new JSONObject(data);
                    ArrayList<String> countryArray=new ArrayList<>();
                    if (jsonObject.optBoolean("status")) {
                        JSONArray countryList=jsonObject.optJSONArray("data");
                        for (int ar=0;ar<countryList.length();ar++){
                            JSONObject jsonObject1=countryList.getJSONObject(ar);
                            CountryModal countryModal=new CountryModal();
                            countryModal.setId(jsonObject1.optString("id"));
                            countryModal.setCountryName(jsonObject1.optString("country_name"));

                            countryArray.add(jsonObject1.optString("country_name"));
                            countryModalArrayList.add(countryModal);
                        }
                        country.setAdapter(new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, countryArray));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }

            @Override
            public void error(VolleyError error) {
                error.printStackTrace();
            }
        });
    }

    private void register() {

        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();

        new Thread(() -> {
            try {
                //first_name,last_name,contact_no,country,trainee_id,profile_photo_edit
                MultipartUtility multipart = new MultipartUtility(Api.TraineeSignup, "UTF-8");
                multipart.addFormField("first_name",firstName.getText().toString());
                multipart.addFormField("last_name",lastName.getText().toString());
                multipart.addFormField("password",password.getText().toString());
                multipart.addFormField("email",email.getText().toString());
                multipart.addFormField("contact_no",contactNumber.getText().toString());
                multipart.addFormField("country",countyID);
                if(photoFile==null)
                    multipart.addFormField("profile_photo", "");
                else
                    multipart.addFilePart("profile_photo", photoFile);

                String response = multipart.finish();// response from server
                Log.e("Multipart Response",response);
                try {
                    final JSONObject jsonObject1 = new JSONObject(response);
                    boolean status = jsonObject1.optBoolean("status");
                    runOnUiThread(() -> {
                        progressDialog.cancel();;
                        if(status){
                            Toast.makeText(context, context.getResources().getString(R.string.attachmentUploaded), Toast.LENGTH_SHORT).show();
                            finish();
                        }
                        else {
                            try {
                                JSONObject jsonObject=jsonObject1.getJSONObject("message");
                                if (jsonObject.has("email"))
                                    email.setError(jsonObject.optString("email"));
                                if (jsonObject.has("password"))
                                    password.setError(jsonObject.optString("password"));

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });


                } catch (JSONException e) {
                    ((MainActivity)context).runOnUiThread(() -> {
                        progressDialog.cancel();;
                        // Stuff that updates the UI
                    });
                    e.printStackTrace();
                }
            } catch (IOException e) {
                ((MainActivity)context).runOnUiThread(() -> {
                    progressDialog.cancel();;
                    // Stuff that updates the UI
                });
                e.printStackTrace();
            }
        }).start();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean checkAndRequestPermissions() {
        int cameraPermission = ContextCompat.checkSelfPermission(context, android.Manifest.permission.CAMERA);
        int gps = ContextCompat.checkSelfPermission(context, android.Manifest.permission.READ_EXTERNAL_STORAGE);
        int writeExternal = ContextCompat.checkSelfPermission(context,
            Manifest.permission.WRITE_EXTERNAL_STORAGE);

        List<String> listPermissionsNeeded = new ArrayList<>();
        if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.CAMERA);
        }

        if (gps != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (writeExternal != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            requestPermissions(listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 123);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 123) {
            if (grantResults.length > 0) {
                for (int i = 0; i < permissions.length; i++) {
                    if (permissions[i].equals( android.Manifest.permission.CAMERA)) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {}
                    }
                    else if (permissions[i].equals(android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {}
                    }
                    else if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {}
                    }
                }
            }
        }
    }

    private void captureImageInitialization() {
        /**
         * a selector dialog to display two image source options, from camera
         * ‘Take from camera’ and from existing files ‘Select from gallery’
         */
        final String[] items = new String[]{"Take from camera", "Select from gallery"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.select_dialog_item, items);
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);

        builder.setTitle(getResources().getString(R.string.uploadProfilePic));

        builder.setAdapter(adapter, (dialog, item) -> { // pick from
            // camera
            if (item == 0) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                photoFile= getPhotoFileUri(photoFileName);
                mImageCaptureUri= FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", photoFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT,mImageCaptureUri);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                //mImageCaptureUri = Uri.fromFile(new File(Environment.getExternalStorageDirectory(), "tmp_avatar_" + String.valueOf(System.currentTimeMillis()) + ".jpg"));
                //intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mImageCaptureUri);

                try {
                    intent.putExtra("return-data", true);
                    startActivityForResult(intent, PICK_FROM_CAMERA);

                } catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
            } else {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                // Start the Intent
                startActivityForResult(galleryIntent, PICK_FROM_FILE);
            }
        });

        dialog1 = builder.create();
    }

    public File getPhotoFileUri(String fileName) {
        File mediaStorageDir = new File(String.valueOf(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)));

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()){
            Log.d("Error", "failed to create directory");
        }

        // Return the file target for the photo based on filename
        this.file = new File(mediaStorageDir.getPath() + File.separator + fileName);

        return this.file;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode !=RESULT_OK)
            return;

        switch (requestCode) {
            case UCrop.REQUEST_CROP:
                handleUCropResult(data);
                break;
            case UCrop.RESULT_ERROR:
                final Throwable cropError = UCrop.getError(data);
                Log.e("TAG", "Crop error: " + cropError);
                setResultCancelled();
                break;
            case PICK_FROM_CAMERA:
                //uri=data.getData();
                /**
                 * After taking a picture, do the crop
                 */
                try {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    thumbnail = BitmapFactory.decodeStream(context.getContentResolver().openInputStream(mImageCaptureUri), null, options);
                    Log.e("mImageCaptureUri",""+mImageCaptureUri);

                    profilePicName=URLUtil.guessFileName(mImageCaptureUri.toString(), null, null);
                    //profileFile=photoFile;
                    //getPicOrientation();
                    takenFrom="camera";
                    CropImage();;

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                break;

            case PICK_FROM_FILE:
                /**
                 * After selecting image from files, save the selected path
                 */

                mImageCaptureUri = data.getData();
                takenFrom="gallery";
                CropImage();
                Log.e("mImageCaptureUri",""+mImageCaptureUri);
                //doCrop();
                break;

        }
    }

    private void handleUCropResult(Intent data) {
        if (data == null) {
            setResultCancelled();
            return;
        }
        final Uri resultUri = UCrop.getOutput(data);
        setResultOk(resultUri);
    }

    private void setResultOk(Uri imagePath) {
        Intent intent = new Intent();
        intent.putExtra("path", imagePath);
        setResult(Activity.RESULT_OK, intent);
        mImageCaptureUri=imagePath;
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imagePath);
            profile_image.setImageBitmap(bitmap);

            try {
                // SDK < API11
                if (Build.VERSION.SDK_INT < 11)
                    realPath = RealPathUtil.getRealPathFromURI_BelowAPI11(context, imagePath);

                    // SDK >= 11 && SDK < 19
                else if (Build.VERSION.SDK_INT < 19)
                    realPath = RealPathUtil.getRealPathFromURI_API11to18(context, imagePath);

                    // SDK > 19 (Android 4.4)
                else
                    realPath = RealPathUtil.getRealPathFromURI_API19(context,imagePath);

                Log.e("Real Path", "" + realPath);

                String PdfPathHolder = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                    try {
                        PdfPathHolder = FilePath.getPath(context, mImageCaptureUri);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
                assert PdfPathHolder != null;
                if (realPath == null)
                    photoFile = new File(PdfPathHolder);
                else
                    photoFile = new File(realPath);

                profileFile=photoFile;

            }
            catch (Exception e){
                e.printStackTrace();
            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setResultCancelled() {
        Intent intent = new Intent();
        setResult(Activity.RESULT_CANCELED, intent);
        finish();
    }
    private static String queryName(ContentResolver resolver, Uri uri) {
        Cursor returnCursor =
            resolver.query(uri, null, null, null, null);
        assert returnCursor != null;
        int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        returnCursor.moveToFirst();
        String name = returnCursor.getString(nameIndex);
        returnCursor.close();
        return name;
    }
    private void CropImage() {
        try {
            Uri destinationUri = Uri.fromFile(new File(getCacheDir(), queryName(getContentResolver(), mImageCaptureUri)));
            UCrop.Options options = new UCrop.Options();
            options.setCompressionQuality(80);
            options.setToolbarColor(ContextCompat.getColor(this, R.color.colorPrimary));
            options.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
            options.setActiveWidgetColor(ContextCompat.getColor(this, R.color.colorPrimary));
            options.withAspectRatio(4, 4);
            options.withMaxResultSize(500, 500);

            UCrop.of(mImageCaptureUri, destinationUri)
                .withOptions(options)
                .start(this);

        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(context, "Your device doesn't support the crop action!", Toast.LENGTH_SHORT).show();
        }
    }

    private void getPicOrientation() {
        ExifInterface ei = null;
        try {
            ei = new ExifInterface(String.valueOf(getPhotoFileUri(photoFileName)));
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            Log.e("orientation",""+orientation);
            switch(orientation) {

                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotatedBitmap = rotateImage(thumbnail, 90);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotatedBitmap = rotateImage(thumbnail, 180);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotatedBitmap = rotateImage(thumbnail, 270);
                    break;

                case ExifInterface.ORIENTATION_NORMAL:
                    rotatedBitmap = rotateImage(thumbnail, 0);
                    break;
                default:
                    rotatedBitmap = thumbnail;
            }

            profile_image.setImageBitmap(rotatedBitmap);
            profileFile = new File(context.getCacheDir(), photoFileName);
            profileFile.createNewFile();

//Convert bitmap to byte array
            Bitmap bitmap =rotatedBitmap;
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 20 /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();

//write the bytes in file
            FileOutputStream fos = new FileOutputStream(profileFile);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }
}
