package com.ecadema.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.ecadema.adapter.CommunityAdapterNew;
import com.ecadema.adapter.TagsAdapterNew;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.GetMethod;
import com.ecadema.apiMethods.ResponseData;
import com.ecadema.app.BuildConfig;
import com.ecadema.app.R;
import com.ecadema.interfaces.AddLifecycleCallbackListener;
import com.ecadema.modal.BlogModal;
import com.ecadema.modal.TagsModal;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Locale;

public class CommunityFragment extends Fragment implements AddLifecycleCallbackListener {

    Context context;
    SharedPreferences sharedPreferences;
    public ProgressDialog progressDialog;

    public RecyclerView recyclerView,tagsRecyclerView;
    public TextView noData,done,clearFilter;
    ArrayList<BlogModal> blogModalArrayList=new ArrayList<>();

    RelativeLayout communityLayout;
    Uri mImageCaptureUri;
    public String photoFileName="image_" + System.currentTimeMillis()+ ".jpg",profilePicName="";
    String fileName="",CameraFile="";
    Bundle bundle;
    public LinearLayout searchLayout,tagsLayout,searchMainLayout;
    public EditText searchTrainerAndCourse;
    CommunityAdapterNew communityAdapterNew;
    public Dialog dialog;
    private ArrayList<TagsModal> tagsModalArrayList = new ArrayList<>();
    TagsAdapterNew tagsAdapter;
    ImageView closeSearchTrainer;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        context= getContext();

        sharedPreferences= PreferenceManager.getDefaultSharedPreferences(context);
        progressDialog=new ProgressDialog(context);

        Resources res=getResources();
        DisplayMetrics dr=res.getDisplayMetrics();
        Configuration configuration=res.getConfiguration();
        if (Build.VERSION.SDK_INT >=Build.VERSION_CODES.JELLY_BEAN_MR1){
            configuration.setLocale(new Locale(sharedPreferences.getString("lang","").toLowerCase()));
        }
        else
        {
            configuration.locale=new Locale(sharedPreferences.getString("lang","").toLowerCase());
        }
        res.updateConfiguration(configuration,dr);
        View root = inflater.inflate(R.layout.fragment_blog, container, false);


        closeSearchTrainer=root.findViewById(R.id.closeSearchTrainer);
        searchTrainerAndCourse=root.findViewById(R.id.searchTrainerAndCourse);
        searchMainLayout=root.findViewById(R.id.searchMainLayout);
        tagsLayout=root.findViewById(R.id.tagsLayout);
        searchLayout=root.findViewById(R.id.searchLayout);
        communityLayout=root.findViewById(R.id.communityLayout);
        noData=root.findViewById(R.id.noData);
        recyclerView=root.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));

        closeSearchTrainer.setOnClickListener(v->{
            searchMainLayout.setVisibility(View.GONE);
            searchTrainerAndCourse.getText().clear();
        });
        AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
        LayoutInflater inflater1 = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("InflateParams") final View dialogView = inflater1.inflate(R.layout.tags_dialog, null);
        alertDialog.setView(dialogView);
        alertDialog.setCancelable(false);
        dialog=alertDialog.create();
        clearFilter=dialogView.findViewById(R.id.clearFilter);
        tagsRecyclerView=dialogView.findViewById(R.id.tagsRecyclerView);
        done=dialogView.findViewById(R.id.done);
        done.setOnClickListener(v-> {
            if (!tagFilter.equalsIgnoreCase("")) {
                searchMainLayout.setVisibility(View.GONE);
                searchTrainerAndCourse.getText().clear();
                ApplyTagFilter(tagFilter);
            }
            dialog.dismiss();
        });
        clearFilter.setOnClickListener(v->{
            getAllBlog();
            dialog.dismiss();
            searchMainLayout.setVisibility(View.GONE);
            searchTrainerAndCourse.getText().clear();
        });

        AskPermission();
        getAllBlog();

        searchLayout.setOnClickListener(v->{
            searchMainLayout.setVisibility(View.VISIBLE);
            if (communityAdapterNew.headerViewHolder!=null)
                communityAdapterNew.headerViewHolder.postLayout.setVisibility(View.GONE);
            //searchMainLayout.setVisibility(View.VISIBLE);
        });

        tagsLayout.setOnClickListener(v->{
            if (tagsAdapter!=null) {
                dialog.show();
            }
        });
        searchTrainerAndCourse.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(communityAdapterNew!=null) {

                    communityAdapterNew.filter(charSequence.toString());
                    if(communityAdapterNew.getItemCount()==0) {
                        recyclerView.setVisibility(View.GONE);
                        noData.setText(context.getResources().getString(R.string.noMatchFound));
                        noData.setVisibility(View.VISIBLE);
                        return;
                    }

                    if (sharedPreferences.getString("user_type","").equalsIgnoreCase("3")){
                        if (communityAdapterNew.blogModalArrayList.size()==0){
                            noData.setVisibility(View.VISIBLE);
                            noData.setText(context.getResources().getString(R.string.noMatchFound));
                            return;
                        }
                    }
                    noData.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                }else{
                    noData.setVisibility(View.VISIBLE);
                    noData.setText(context.getResources().getString(R.string.noMatchFound));
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {}
        });

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        context.registerReceiver(tagBroadCast,new IntentFilter("tagBroadCastNew"));
    }

    private String tagFilter="";
    BroadcastReceiver tagBroadCast =new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getStringExtra("filterType").equalsIgnoreCase("tag")){
                tagFilter = intent.getStringExtra("tagLoc");
            }
        }
    };

    private void ApplyTagFilter(String tagLoc) {
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();
        new GetMethod(Api.CommunityList+sharedPreferences.getString("lang","")+"&user_id="+
                sharedPreferences.getString("userID","")+"&tags="+tagLoc,context).startmethod(
                new ResponseData() {
                    @Override
                    public void response(final String data) {
                        progressDialog.dismiss();
                        Log.e("filterData",data);
                        try {
                            blogModalArrayList.clear();
                            JSONObject jsonObject = new JSONObject(data);
                            if(jsonObject.optBoolean("status")){
                                noData.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.VISIBLE);

                                JSONArray jsonArray = jsonObject.optJSONArray("data");
                                for (int i=0; i<jsonArray.length(); i++){
                                    JSONObject object = jsonArray.getJSONObject(i);
                                    BlogModal blogModal = new BlogModal();
                                    blogModal.setId(object.optString("id"));
                                    blogModal.setName(object.optString("teacher_name"));
                                    blogModal.setTeacherId(object.optString("teacher_id"));
                                    blogModal.setImage(object.optString("image"));
                                    blogModal.setProfileImage(object.optString("profile_image"));
                                    blogModal.setTitle(object.optString("title"));
                                    blogModal.setDate(object.optString("create_date"));
                                    blogModal.setShortDecription(object.optString("short_description"));
                                    blogModal.setView(object.optString("views"));
                                    blogModal.setLikes(object.optString("likes"));
                                    blogModal.setDislike(object.optString("dislikes"));
                                    blogModal.setVideo(object.optString("youtube_code"));
                                    blogModal.setVideoLink(object.optString("url"));
                                    blogModal.setShareUrl(object.optString("share_url"));
                                    blogModal.setLikeDislike(object.optString("UserLikeDislike"));
                                    blogModal.setCategoryName(object.optString("categories_name"));
                                    blogModal.setTagIDs(object.optString("tags"));
                                    blogModal.setTagName(object.optString("tags_name"));

                                    blogModalArrayList.add(blogModal);
                                }
                                communityAdapterNew = new CommunityAdapterNew(CommunityFragment.this,context,blogModalArrayList);
                                recyclerView.setAdapter(communityAdapterNew);
                            }else {
                                noData.setVisibility(View.VISIBLE);
                                recyclerView.setVisibility(View.GONE);
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void error(final VolleyError error) {
                        progressDialog.dismiss();
                        error.printStackTrace();
                    }
                });

    }

    private void getTrainersTag() {
        new GetMethod(Api.getAllFilterTagsTeachersCommunity+sharedPreferences.getString("lang",""),context).startmethod(
                new ResponseData() {
                    @Override
                    public void response(final String data) {
                        Log.e("blog",data);
                        try {
                            JSONObject dataObject = new JSONObject(data);
                            JSONArray tag = dataObject.optJSONArray("data");
                            tagsModalArrayList.clear();
                            for (int me=0; me<tag.length(); me++){
                                TagsModal tagsModal = new TagsModal();
                                JSONObject jsonObject = tag.getJSONObject(me);
                                tagsModal.setTagId(jsonObject.optString("id"));
                                tagsModal.setTagName(jsonObject.optString("tag"));
                                tagsModalArrayList.add(tagsModal);
                            }
                            tagsAdapter = new TagsAdapterNew(context,tagsModalArrayList);
                            tagsRecyclerView.setAdapter(tagsAdapter);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void error(final VolleyError error) {
                        error.printStackTrace();
                    }
                });

    }

    private void AskPermission() {
        askMultiplePermissions.launch(new String[]{ Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE});
    }

    ActivityResultLauncher<String[]> askMultiplePermissions = registerForActivityResult(new ActivityResultContracts.RequestMultiplePermissions(),
            result -> {
                // Handle the returned Uri
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    if (result.getOrDefault(Manifest.permission.CAMERA,false)== true)
                        Log.e("Permission","Access");
                    else {
                        AskPermission();
                        Log.e("Permission","Denied");
                    }
                }
            });
    ActivityResultLauncher<String> someActivityResultLauncher = registerForActivityResult(new ActivityResultContracts.GetContent(),
            uri -> {
                // Handle the returned Uri
                try {
                    mImageCaptureUri = uri;
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    // Get the cursor
                    Cursor cursor = context.getContentResolver().query(mImageCaptureUri,
                            filePathColumn, null, null, null);
                    // Move to first row
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String imgDecodableString = cursor.getString(columnIndex);
                    cursor.close();

                    //filePath = imgDecodableString;
                    if (mImageCaptureUri.toString().startsWith("content://")) {
                        try {
                            cursor = context.getContentResolver().query(mImageCaptureUri, null, null, null, null);
                            if (cursor != null && cursor.moveToFirst()) {

                                String displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                                fileName=displayName;
                            }
                        } finally {
                            cursor.close();
                        }
                    }else if (mImageCaptureUri.toString().startsWith("file://")) {
                        File myFile = new File(mImageCaptureUri.toString());
                        String displayName = myFile.getName();
                        fileName=displayName;
                    }
                    recyclerView.getAdapter().notifyDataSetChanged();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            });

    ActivityResultLauncher<Uri> cameraImage = registerForActivityResult(new ActivityResultContracts.TakePicture(),
            bitmap->{
                try {
                    fileName = URLUtil.guessFileName(mImageCaptureUri.toString(), null, null);
                    recyclerView.getAdapter().notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });


    public String getFileName(){
        return fileName;
    }
    public String getCameraFile(){
        return CameraFile;
    }
    public Uri getUri(){
        return mImageCaptureUri;
    }

    public void takePic(int type){
        if (type==3)
            someActivityResultLauncher.launch("image/*");
        else {

            mImageCaptureUri= FileProvider.getUriForFile(context,
                    BuildConfig.APPLICATION_ID + ".provider", getPhotoFileUri(photoFileName));
            cameraImage.launch(mImageCaptureUri);
        }
    }

    public File getPhotoFileUri(String fileName) {
        File mediaStorageDir = new File(String.valueOf(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)));
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()){
            Log.d("Error", "failed to create directory");
        }
        // Return the file target for the photo based on filename
        CameraFile=mediaStorageDir.getPath() + File.separator + fileName;
        return new File(mediaStorageDir.getPath() + File.separator + fileName);
    }

    public void getAllBlog() {
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();
        getTrainersTag();

        new GetMethod(Api.CommunityList+sharedPreferences.getString("lang","")+"&user_id="+
                sharedPreferences.getString("userID",""),context).startmethod(
            new ResponseData() {
                @Override
                public void response(final String data) {
                    Log.e("blog",data);
                    try {
                        blogModalArrayList.clear();
                        JSONObject jsonObject = new JSONObject(data);
                        if(jsonObject.optBoolean("status")){
                            noData.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);

                            JSONArray jsonArray = jsonObject.optJSONArray("data");
                            for (int i=0; i<jsonArray.length(); i++){
                                JSONObject object = jsonArray.getJSONObject(i);
                                BlogModal blogModal = new BlogModal();
                                blogModal.setId(object.optString("id"));
                                blogModal.setName(object.optString("teacher_name"));
                                blogModal.setTeacherId(object.optString("teacher_id"));
                                blogModal.setImage(object.optString("image"));
                                blogModal.setProfileImage(object.optString("profile_image"));
                                blogModal.setTitle(object.optString("title"));
                                blogModal.setDate(object.optString("create_date"));
                                blogModal.setShortDecription(object.optString("short_description"));
                                blogModal.setView(object.optString("views"));
                                blogModal.setLikes(object.optString("likes"));
                                blogModal.setDislike(object.optString("dislikes"));
                                blogModal.setVideo(object.optString("youtube_code"));
                                blogModal.setVideoLink(object.optString("url"));
                                blogModal.setShareUrl(object.optString("share_url"));
                                blogModal.setLikeDislike(object.optString("UserLikeDislike"));
                                blogModal.setCategoryName(object.optString("categories_name"));
                                blogModal.setTagIDs(object.optString("tags"));
                                blogModal.setTagName(object.optString("tags_name"));

                                blogModalArrayList.add(blogModal);
                            }
                            communityAdapterNew = new CommunityAdapterNew(CommunityFragment.this,context,blogModalArrayList);
                            recyclerView.setAdapter(communityAdapterNew);
                        }else {
                            noData.setText(jsonObject.optString("message"));
                            noData.setVisibility(View.VISIBLE);
                            recyclerView.setVisibility(View.GONE);
                        }

                        /*new Handler().postDelayed(()->{
                            progressDialog.dismiss();
                        },5000);*/
                    }catch (Exception e){
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void error(final VolleyError error) {
                    progressDialog.dismiss();
                    error.printStackTrace();
                }
            });
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setHeader(String html, String tagIDs, String imageName, String youtubeLink, String postID){
        bundle = new Bundle();
        bundle.putString("html",html);
        bundle.putString("tagIDs",tagIDs);
        bundle.putString("imageName",imageName);
        bundle.putString("postID",postID);
        bundle.putString("youtubeLink",youtubeLink);
        communityAdapterNew.notifyDataSetChanged();
        recyclerView.scrollToPosition(0);
    }

    public void setBlankHeader(String html, String tagIDs, String imageName, String youtubeLink, String postID){
        bundle = new Bundle();
        bundle.putString("html",html);
        bundle.putString("tagIDs",tagIDs);
        bundle.putString("imageName",imageName);
        bundle.putString("postID",postID);
        bundle.putString("youtubeLink",youtubeLink);
    }
    public Bundle getHeader(){
        return bundle;
    }
    @Override
    public void addLifeCycleCallBack(YouTubePlayerView youTubePlayerView) {
        getLifecycle().addObserver(youTubePlayerView);
    }

}
