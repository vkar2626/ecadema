package com.ecadema.fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.ecadema.Utility;
import com.ecadema.adapter.AvailabilityAdapter;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.PostMethod;
import com.ecadema.apiMethods.ResponseData;
import com.ecadema.app.MainActivity;
import com.ecadema.app.R;
import com.ecadema.modal.AvailabilityModal;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.HorizontalCalendarListener;

public class MyAvailabilityFragment extends Fragment {

    Context context;
    SharedPreferences sharedPreferences;
    ProgressDialog progressDialog;

    RecyclerView recyclerView;
    ArrayList<AvailabilityModal> availabilityModalArrayList = new ArrayList<>();
    HorizontalCalendar horizontalCalendar;
    TextView tvdate,edit;
    String dayOfTheWeek,trainerID,courseID,selectedDate,type="set",bookingSlotID,bookingID;
    ArrayList<String> selectedTimeDate = new ArrayList<>();
    ImageView close;

    @SuppressLint("SimpleDateFormat")
    SimpleDateFormat format5 = new SimpleDateFormat("EEE, dd MMM, yyyy");
    @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    @SuppressLint("SimpleDateFormat") SimpleDateFormat timeFormat1 = new SimpleDateFormat("yyyy-MM-dd");
    @SuppressLint("SimpleDateFormat") SimpleDateFormat timeFormat2 = new SimpleDateFormat("hh:mm a");

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_availability, container, false);

        context = getContext();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        progressDialog = new ProgressDialog(context);
        ((MainActivity) context).hideBottomMenu();

        edit=root.findViewById(R.id.edit);
        close=root.findViewById(R.id.close);
        tvdate=root.findViewById(R.id.tvdate);
        recyclerView=root.findViewById(R.id.recyclerView);
        int mNoOfColumns = Utility.calculateNoOfColumns(context,120);
        recyclerView.setLayoutManager(new GridLayoutManager(context,mNoOfColumns));
        //recyclerView.addItemDecoration(new VerticalSpaceItemDecoration(0,0,0,15));

        Calendar calendar = Calendar.getInstance();
        /** end after 1 month from now */
        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.MONTH, 1);

        /** start before 1 month from now */
        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, 0);
        horizontalCalendar = new HorizontalCalendar.Builder(root, R.id.calendarView)
                .startDate(startDate.getTime())
                .endDate(endDate.getTime())
                .datesNumberOnScreen(5)
                .dayFormat("EEE")
                .dayNumberFormat("dd")
                .textColor(Color.BLACK, getResources().getColor(R.color.colorAccent))
                .selectedDateBackground(Color.TRANSPARENT)
                .build();

        tvdate.setText(format5.format(calendar.getTime()));
        selectedDate= timeFormat1.format(calendar.getTime());
        dayOfTheWeek = (String) DateFormat.format("EEE", calendar.getTime()); // Thursday
        Log.e("dayOfTheWeek",dayOfTheWeek);
        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Date date, int position) {
                progressDialog.show();
                progressDialog.setCancelable(false);
                Handler splashHandler=new Handler();
                splashHandler.postDelayed(() -> {
                    progressDialog.dismiss();
                }, 500);
                tvdate.setText(format5.format(date));
                dayOfTheWeek = (String) DateFormat.format("EEE", date); // Thursday
                Log.e("dayOfTheWeek",dayOfTheWeek);
                selectedDate= timeFormat1.format(date);
                recyclerView.setAdapter(new AvailabilityAdapter(context,availabilityModalArrayList,
                        selectedTimeDate,type,dayOfTheWeek));
                //getAvailability(0);
                // Toast.makeText(getApplicationContext(), DateFormat.getDateInstance().format(date) + " is selected!", Toast.LENGTH_SHORT).show();
            }
        });


        edit.setOnClickListener(v1->{
            //Log.e("selectedTimeDate",""+selectedTimeDate);
            if(edit.getText().toString().equalsIgnoreCase(context.getResources().getString(R.string.edit))){
                edit.setText(context.getResources().getString(R.string.update));
                edit.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                type="edit";
                recyclerView.setAdapter(new AvailabilityAdapter(context,availabilityModalArrayList,
                        selectedTimeDate,type,dayOfTheWeek));
            }else {
                saveBooking();
                /*if(selectedTimeDate.size()>0)
                    saveBooking();
                else
                    Snackbar.make(v1,context.getResources().getString(R.string.pleaseSelectTImeSlot),Snackbar.LENGTH_LONG).show();
*/
            }
        });
        getAvailability(0);
        close.setOnClickListener(v-> getParentFragmentManager().popBackStackImmediate());
        return root;
    }

    private void saveBooking() {
        String selectedTimeSlots = android.text.TextUtils.join(",", selectedTimeDate);
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();
        /*Param: ,,,bookedslots,,:2020-12-26 09:30:00*/
        Map<String,String> param=new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));
        param.put("timezone", TimeZone.getDefault().getID());
        param.put("slotes",selectedTimeSlots);
        param.put("trainer_id",sharedPreferences.getString("userID",""));


        Log.e("updateParam",""+param);

        new PostMethod(Api.TrainerUpdateSlotes,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                progressDialog.cancel();;
                Log.e("updateResponse",data);

                try {
                    JSONObject jsonObject = new JSONObject(data);
                    if(jsonObject.optBoolean("status")){
                        edit.setText(context.getResources().getString(R.string.edit));
                        type="set";
                        //recyclerView.setAdapter(new AvailabilityAdapter(context,availabilityModalArrayList,selectedTimeDate,type));
                        selectedTimeDate.clear();
                        edit.setBackgroundColor(getResources().getColor(R.color.dark_grey));
                        getAvailability(1);
                        Toast.makeText(context, context.getResources().getString(R.string.availabilityUpdated), Toast.LENGTH_LONG).show();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();;
                error.printStackTrace();
            }
        });
    }

    private void getAvailability(int i) {
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();
        Map<String,String> param=new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));
        param.put("timezone", TimeZone.getDefault().getID());
        param.put("trainer_id",sharedPreferences.getString("userID",""));
        param.put("day_of_week",dayOfTheWeek);

        Log.e("AvailParam",""+param);

        new PostMethod(Api.TrainerAvailability,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                progressDialog.cancel();;
                Log.e("AvailResponse",data);

                try {
                    JSONObject jsonObject = new JSONObject(data);
                    if(jsonObject.optBoolean("status")){
                        availabilityModalArrayList.clear();
                        //selectedTimeDate.clear();
                        JSONArray dataArray = jsonObject.optJSONArray("slots_android");
                        for (int ar=0;ar<dataArray.length();ar++){
                            JSONObject object = dataArray.getJSONObject(ar);
                            AvailabilityModal availabilityModal = new AvailabilityModal();
                            availabilityModal.setID(object.optString("id"));
                            availabilityModal.setTimings(object.optString("timings"));
                            availabilityModal.setSlots(object.optString("select_slote_id"));
                            availabilityModal.setDay(object.optString("day_of_week"));
                            availabilityModal.setAvailability(jsonObject.optJSONArray("select_slote"));
                            availabilityModalArrayList.add(availabilityModal);
                        }
                        JSONArray selectedSlots = jsonObject.optJSONArray("select_slote");
                        for (int ar=0;ar<selectedSlots.length();ar++){
                            JSONObject object = selectedSlots.getJSONObject(ar);
                            selectedTimeDate.add(object.optString("timeslot_id"));
                        }
                       /* if(i==0) {
                            recyclerView.setAdapter(new AvailabilityAdapter(context,availabilityModalArrayList,selectedTimeDate,type));
                        }
                        else
                            recyclerView.getAdapter().notifyDataSetChanged();*/
                        recyclerView.setAdapter(new AvailabilityAdapter(context,availabilityModalArrayList,
                                selectedTimeDate,type,dayOfTheWeek));

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();;
                error.printStackTrace();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        context.registerReceiver(selectedTime,new IntentFilter("editSlot"));
    }

    BroadcastReceiver selectedTime= new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getStringExtra("type").equalsIgnoreCase("add"))
                selectedTimeDate.add(intent.getStringExtra("selectedTime"));
            else
                selectedTimeDate.remove(intent.getStringExtra("selectedTime"));
        }
    };
}
