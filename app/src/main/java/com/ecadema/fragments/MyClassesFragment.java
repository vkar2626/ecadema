package com.ecadema.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.androidbuts.multispinnerfilter.KeyPairBoolData;
import com.androidbuts.multispinnerfilter.MultiSpinnerSearch;
import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.exceptions.OutOfDateRangeException;
import com.ecadema.VerticalSpaceItemDecoration;
import com.ecadema.adapter.MyClassPastAdapter;
import com.ecadema.adapter.MyClassUpcomingAdapter;
import com.ecadema.adapter.MyPastAdapter;
import com.ecadema.adapter.MyUpcomingAdapter;
import com.ecadema.adapter.RequestedListAdapter;
import com.ecadema.adapter.SessionsPastAdapter;
import com.ecadema.adapter.SessionsUpcomingAdapter;
import com.ecadema.adapter.TrainerPastAdapter;
import com.ecadema.adapter.TrainerPastBookingAdapter;
import com.ecadema.adapter.TrainerSubmittedAdapter;
import com.ecadema.adapter.TrainerUpcomingAdapter;
import com.ecadema.adapter.TrainerUpcomingBookingAdapter;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.GetMethod;
import com.ecadema.apiMethods.PostMethod;
import com.ecadema.apiMethods.ResponseData;
import com.ecadema.app.MainActivity;
import com.ecadema.app.R;
import com.ecadema.modal.AttendeesModal;
import com.ecadema.modal.LanguageModal;
import com.ecadema.modal.PastModal;
import com.ecadema.modal.RequestedModel;
import com.ecadema.modal.SubmittedModal;
import com.ecadema.modal.TagsModal;
import com.ecadema.modal.TimeModal;
import com.ecadema.modal.UpcomingModal;
import com.even.mricheditor.ActionType;
import com.even.mricheditor.RichEditorAction;
import com.even.mricheditor.RichEditorCallback;
import com.even.mricheditor.ui.ActionImageView;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class MyClassesFragment extends Fragment {

    RecyclerView upcomingRecyclerView,pastRecyclerView,submittedRecyclerView,upcomingRecyclerView1,
            pastRecyclerView1,requestedRecyclerView;
    MyClassUpcomingAdapter upcomingAdapter;
    MyClassPastAdapter pastAdapter;
    SessionsPastAdapter sessionsPastAdapter;
    SessionsUpcomingAdapter sessionsUpcomingAdapter;

    ArrayList<UpcomingModal> upcomingModalArrayList=new ArrayList<>();
    ArrayList<PastModal> pastModalArrayList=new ArrayList<>();
    ArrayList<SubmittedModal> submittedModalArrayList=new ArrayList<>();
    ArrayList<RequestedModel> requestedModelArrayList = new ArrayList<>();

    Context context;
    SharedPreferences sharedPreferences;
    TextView upcoming,past,noPastText,noUpcomingText,header,submitted,noSubmittedText,header2,create,request,requested,noRequestedText,submitRQ;

    RelativeLayout upcomingSessionView,pastSessionView,submittedSessionView;
    LinearLayout ll2,ll1;


    MyUpcomingAdapter upcomingAdapter1;
    MyPastAdapter pastAdapter1;

    ArrayList<UpcomingModal> upcomingModalArrayList1=new ArrayList<>();
    ArrayList<PastModal> pastModalArrayList1=new ArrayList<>();

    TextView upcoming1,past1,noPastText1,noUpcomingText1,passwordText,when,totalHours,submit;

    RelativeLayout upcomingSessionView1,pastSessionView1,requestedView;
    ProgressDialog progressDialog;

    String lang;
    DateTimeFormatter f;
    ScrollView createForm,requestForm;
    AutoCompleteTextView type,status,setPassword,language,maxParticipants,startTime,endTime,aboutRQ,whenRQ,languageRQ,noOfParticipants,
            totalPriceRQ,detailsAboutReq;
    EditText about,ticket;
    CardView passwordCard;
    ArrayList<String> typeList = new ArrayList<>();
    ArrayList<String> statusList = new ArrayList<>();

    ArrayList<TimeModal> startTimeModalArrayList=new ArrayList<>();
    String startTimeIds="", endTimeId="";
    ArrayList<TimeModal> endTimeModalArrayList=new ArrayList<>();

    ArrayList<LanguageModal> languageList=new ArrayList<>();
    String languageIds,languageIdsRQ;

    ArrayList<String> tempList;
    ArrayList<String> tempStartList;
    ArrayList<String> tempEndList;

    Dialog dialog;
    CalendarView calendarView;

    WebView mWebView,mWebView1,mWebView2;
    LinearLayout llActionBarContainer,llActionBarContainer1,llActionBarContainer2;
    ImageView iv_action_insert_link,iv_action_insert_link1,iv_action_insert_link2;

    private String htmlContent = "",htmlContent1 = "",htmlContent2 = "";

    private RichEditorAction mRichEditorAction,mRichEditorAction1,mRichEditorAction2;

    private final List<ActionType> mActionTypeList =
        Arrays.asList(ActionType.BOLD, ActionType.ITALIC, ActionType.UNDERLINE,
            ActionType.STRIKETHROUGH, ActionType.SUBSCRIPT, ActionType.SUPERSCRIPT,
            ActionType.NORMAL, ActionType.H1, ActionType.H2, ActionType.H3, ActionType.H4,
            ActionType.H5, ActionType.H6, ActionType.INDENT, ActionType.OUTDENT,
            ActionType.JUSTIFY_LEFT, ActionType.JUSTIFY_CENTER, ActionType.JUSTIFY_RIGHT,
            ActionType.JUSTIFY_FULL, ActionType.ORDERED, ActionType.UNORDERED, ActionType.LINE,
            ActionType.BLOCK_CODE, ActionType.BLOCK_QUOTE, ActionType.CODE_VIEW);

    private final List<Integer> mActionTypeIconList =
        Arrays.asList(com.even.rich.R.drawable.ic_format_bold, com.even.rich.R.drawable.ic_format_italic,
            com.even.rich.R.drawable.ic_format_underlined, com.even.rich.R.drawable.ic_format_strikethrough,
            com.even.rich.R.drawable.ic_format_subscript, com.even.rich.R.drawable.ic_format_superscript,
            com.even.rich.R.drawable.ic_format_para, com.even.rich.R.drawable.ic_format_h1, com.even.rich.R.drawable.ic_format_h2,
            com.even.rich.R.drawable.ic_format_h3, com.even.rich.R.drawable.ic_format_h4, com.even.rich.R.drawable.ic_format_h5,
            com.even.rich.R.drawable.ic_format_h6, com.even.rich.R.drawable.ic_format_indent_decrease,
            com.even.rich.R.drawable.ic_format_indent_increase, com.even.rich.R.drawable.ic_format_align_left,
            com.even.rich.R.drawable.ic_format_align_center, com.even.rich.R.drawable.ic_format_align_right,
            com.even.rich.R.drawable.ic_format_align_justify, com.even.rich.R.drawable.ic_format_list_numbered,
            com.even.rich.R.drawable.ic_format_list_bulleted, com.even.rich.R.drawable.ic_line, com.even.rich.R.drawable.ic_code_block,
            com.even.rich.R.drawable.ic_format_quote, com.even.rich.R.drawable.ic_code_review);
    int width,padding;
    private RichEditorCallback mRichEditorCallback,mRichEditorCallback1,mRichEditorCallback2;
    View root;

    ArrayList<TagsModal> tagsModalArrayList=new ArrayList<>();
    ArrayList<String> tagIds=new ArrayList<>();
    ArrayList<String> tagNames=new ArrayList<>();
    ArrayList<String> tagTempNames=new ArrayList<>();
    MultiSpinnerSearch multiSelectSpinnerWithSearch;
    List<KeyPairBoolData> listArray1 = new ArrayList<>();
    private boolean is_mentor;
    TimeZone timeZoneId=TimeZone.getDefault();

    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        context= getContext();

        sharedPreferences= PreferenceManager.getDefaultSharedPreferences(context);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        Resources res=getResources();
        DisplayMetrics dr=res.getDisplayMetrics();
        Configuration configuration=res.getConfiguration();
        configuration.setLocale(new Locale(sharedPreferences.getString("lang","").toLowerCase()));
        res.updateConfiguration(configuration,dr);

        Resources activityRes = context.getResources();
        Configuration activityConf = activityRes.getConfiguration();
        Locale newLocale = new Locale(sharedPreferences.getString("lang",""));
        activityConf.setLocale(newLocale);
        activityRes.updateConfiguration(activityConf, activityRes.getDisplayMetrics());

        Resources applicationRes = context.getResources();
        Configuration applicationConf = applicationRes.getConfiguration();
        applicationConf.setLocale(newLocale);
        applicationRes.updateConfiguration(applicationConf, applicationRes.getDisplayMetrics());

        if(sharedPreferences.getString("lang","").equalsIgnoreCase("ar")) {
            lang="SA";
        }
        else
            lang="US";

        root = inflater.inflate(R.layout.fragment_my_classes, container, false);
        llActionBarContainer = root.findViewById(R.id.ll_action_bar_container);
        llActionBarContainer1 = root.findViewById(R.id.ll_action_bar_container1);
        llActionBarContainer2 = root.findViewById(R.id.ll_action_bar_container2);
        iv_action_insert_link = root.findViewById(R.id.iv_action_insert_link);
        iv_action_insert_link1 = root.findViewById(R.id.iv_action_insert_link1);
        iv_action_insert_link2 = root.findViewById(R.id.iv_action_insert_link2);

        initView();

        width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, getResources().getDisplayMetrics());
        padding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 9, getResources().getDisplayMetrics());

        setActionBar1();
        setActionBar2();
        setActionBar3();

        iv_action_insert_link.setOnClickListener(v->{
            androidx.appcompat.app.AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
            LayoutInflater inflater1 =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            @SuppressLint("InflateParams") final View dialogView =
                inflater1.inflate(R.layout.fragment_edit_hyperlink, null);
            alertDialog.setView(dialogView);
            Dialog dialog=alertDialog.create();
            EditText etAddress;
            EditText etDisplayText;
            Button btn_ok;

            etAddress=dialogView.findViewById(com.even.rich.R.id.et_address);
            etDisplayText=dialogView.findViewById(com.even.rich.R.id.et_display_text);
            btn_ok=dialogView.findViewById(com.even.rich.R.id.btn_ok);

            btn_ok.setOnClickListener(v2->{
                if (mRichEditorAction!=null){
                    mRichEditorAction.createLink(etDisplayText.getText().toString(), etAddress.getText().toString());
                }
                dialog.dismiss();
            });

            dialog.create();
            dialog.show();
        });

        iv_action_insert_link1.setOnClickListener(v->{
            androidx.appcompat.app.AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
            LayoutInflater inflater1 =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            @SuppressLint("InflateParams") final View dialogView =
                inflater1.inflate(R.layout.fragment_edit_hyperlink, null);
            alertDialog.setView(dialogView);
            Dialog dialog=alertDialog.create();
            EditText etAddress;
            EditText etDisplayText;
            Button btn_ok;

            etAddress=dialogView.findViewById(com.even.rich.R.id.et_address);
            etDisplayText=dialogView.findViewById(com.even.rich.R.id.et_display_text);
            btn_ok=dialogView.findViewById(com.even.rich.R.id.btn_ok);

            btn_ok.setOnClickListener(v2->{
                if (mRichEditorAction1!=null){
                    mRichEditorAction1.createLink(etDisplayText.getText().toString(), etAddress.getText().toString());
                }
                dialog.dismiss();
            });

            dialog.create();
            dialog.show();
        });

        iv_action_insert_link2.setOnClickListener(v->{
            androidx.appcompat.app.AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
            LayoutInflater inflater1 =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            @SuppressLint("InflateParams") final View dialogView =
                inflater1.inflate(R.layout.fragment_edit_hyperlink, null);
            alertDialog.setView(dialogView);
            Dialog dialog=alertDialog.create();
            EditText etAddress;
            EditText etDisplayText;
            Button btn_ok;

            etAddress=dialogView.findViewById(com.even.rich.R.id.et_address);
            etDisplayText=dialogView.findViewById(com.even.rich.R.id.et_display_text);
            btn_ok=dialogView.findViewById(com.even.rich.R.id.btn_ok);

            btn_ok.setOnClickListener(v2->{
                if (mRichEditorAction2!=null){
                    mRichEditorAction2.createLink(etDisplayText.getText().toString(), etAddress.getText().toString());
                }
                dialog.dismiss();
            });

            dialog.create();
            dialog.show();
        });

        progressDialog=new ProgressDialog(context);

        multiSelectSpinnerWithSearch = root.findViewById(R.id.multipleItemSelectionSpinner);
        //tagsAutocomplete= root.findViewById(R.id.tagsAutocomplete);

        aboutRQ= root.findViewById(R.id.aboutRQ);
        whenRQ= root.findViewById(R.id.whenRQ);
        languageRQ= root.findViewById(R.id.languageRQ);
        noOfParticipants= root.findViewById(R.id.noOfParticipants);
        totalPriceRQ= root.findViewById(R.id.totalPriceRQ);
        detailsAboutReq= root.findViewById(R.id.detailsAboutReq);

        submitRQ= root.findViewById(R.id.submitRQ);
        noRequestedText= root.findViewById(R.id.noRequestedText);
        requestedRecyclerView= root.findViewById(R.id.requestedRecyclerView);
        requestedView= root.findViewById(R.id.requestedView);
        submit= root.findViewById(R.id.submit);
        passwordText= root.findViewById(R.id.passwordText);
        when= root.findViewById(R.id.when);
        startTime= root.findViewById(R.id.startTime);
        endTime= root.findViewById(R.id.endTime);
        totalHours= root.findViewById(R.id.totalHours);
        setPassword= root.findViewById(R.id.setPassword);
        language= root.findViewById(R.id.language);
        maxParticipants= root.findViewById(R.id.maxParticipants);
        about= root.findViewById(R.id.about);
        ticket= root.findViewById(R.id.ticket);
        passwordCard= root.findViewById(R.id.passwordCard);

        type= root.findViewById(R.id.type);
        status= root.findViewById(R.id.status);

        typeList.add(getResources().getString(R.string.workshop));
        typeList.add(getResources().getString(R.string.course));

        statusList.add(getResources().getString(R.string.Private));
        statusList.add(getResources().getString(R.string.Public));

        type.setAdapter(new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, typeList));
        status.setAdapter(new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, statusList));

        type.setOnTouchListener((view, motionEvent) -> {
            type.showDropDown();
            return true;
        });

        status.setOnTouchListener((view, motionEvent) -> {
            status.showDropDown();
            return true;
        });

        status.setOnItemClickListener((parent, view, position, id) -> {
            String selection = (String) parent.getItemAtPosition(position);
            if (selection.equalsIgnoreCase(getResources().getString(R.string.Private))){
                passwordCard.setVisibility(View.VISIBLE);
                passwordText.setVisibility(View.VISIBLE);
            }else {
                passwordCard.setVisibility(View.GONE);
                passwordText.setVisibility(View.GONE);
            }
        });

        create= root.findViewById(R.id.create);
        createForm= root.findViewById(R.id.createForm);
        upcomingRecyclerView1= root.findViewById(R.id.upcomingRecyclerView1);
        pastRecyclerView1= root.findViewById(R.id.pastRecyclerView1);
        upcoming1= root.findViewById(R.id.upcoming1);
        past1= root.findViewById(R.id.past1);
        noPastText1= root.findViewById(R.id.noPastText1);
        noUpcomingText1= root.findViewById(R.id.noUpcomingText1);
        upcomingSessionView1= root.findViewById(R.id.upcomingSessionView1);
        pastSessionView1= root.findViewById(R.id.pastSessionView1);

        submitted= root.findViewById(R.id.submitted);
        submittedRecyclerView= root.findViewById(R.id.submittedRecyclerView);
        noSubmittedText= root.findViewById(R.id.noSubmittedText);
        submittedSessionView= root.findViewById(R.id.submittedSessionView);

        upcomingSessionView= root.findViewById(R.id.upcomingSessionView);
        ll2= root.findViewById(R.id.ll2);
        ll1= root.findViewById(R.id.ll1);
        header2= root.findViewById(R.id.header2);
        header= root.findViewById(R.id.header);
        request= root.findViewById(R.id.request);
        requested= root.findViewById(R.id.requested);
        requestForm= root.findViewById(R.id.requestForm);
        pastSessionView= root.findViewById(R.id.pastSessionView);
        noUpcomingText= root.findViewById(R.id.noUpcomingText);
        noPastText= root.findViewById(R.id.noPastText);
        upcoming= root.findViewById(R.id.upcoming);
        past= root.findViewById(R.id.past);

        upcomingRecyclerView= root.findViewById(R.id.upcomingRecyclerView);
        pastRecyclerView= root.findViewById(R.id.pastRecyclerView);

        upcomingRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        pastRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        submittedRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));

        VerticalSpaceItemDecoration verticalSpaceItemDecoration=new VerticalSpaceItemDecoration(0,0,30,10);
        upcomingRecyclerView.addItemDecoration(new VerticalSpaceItemDecoration(0,0,20,10));
        pastRecyclerView.addItemDecoration(verticalSpaceItemDecoration);
        submittedRecyclerView.addItemDecoration(verticalSpaceItemDecoration);

        Upcoming(sharedPreferences.getString("user_type",""));

        upcoming.setOnClickListener(view -> {
            Upcoming(sharedPreferences.getString("user_type",""));
        });

        past.setOnClickListener(view -> {
            progressDialog.setTitle(context.getResources().getString(R.string.processing));
            progressDialog.show();
            Past(sharedPreferences.getString("user_type",""));
        });

        requested.setOnClickListener(v->{
            requested.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            past.setTextColor(context.getResources().getColor(R.color.dark_grey));
            request.setTextColor(context.getResources().getColor(R.color.dark_grey));
            upcoming.setTextColor(context.getResources().getColor(R.color.dark_grey));
            pastSessionView.setVisibility(View.GONE);
            upcomingSessionView.setVisibility(View.GONE);
            requestForm.setVisibility(View.GONE);
            requestedView.setVisibility(View.VISIBLE);
            getRequestedList();
        });
        request.setOnClickListener(v->{
            request.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            past.setTextColor(context.getResources().getColor(R.color.dark_grey));
            requested.setTextColor(context.getResources().getColor(R.color.dark_grey));
            upcoming.setTextColor(context.getResources().getColor(R.color.dark_grey));
            pastSessionView.setVisibility(View.GONE);
            upcomingSessionView.setVisibility(View.GONE);
            requestedView.setVisibility(View.GONE);
            requestForm.setVisibility(View.VISIBLE);
        });
        submitted.setOnClickListener(view -> {
            submitted.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            past1.setTextColor(context.getResources().getColor(R.color.dark_grey));
            create.setTextColor(context.getResources().getColor(R.color.dark_grey));
            upcoming1.setTextColor(context.getResources().getColor(R.color.dark_grey));
            submittedSessionView.setVisibility(View.VISIBLE);
            pastSessionView1.setVisibility(View.GONE);
            createForm.setVisibility(View.GONE);
            upcomingSessionView1.setVisibility(View.GONE);
        });

        create.setOnClickListener(view -> {
            create.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            submitted.setTextColor(context.getResources().getColor(R.color.dark_grey));
            past1.setTextColor(context.getResources().getColor(R.color.dark_grey));
            upcoming1.setTextColor(context.getResources().getColor(R.color.dark_grey));
            createForm.setVisibility(View.VISIBLE);
            submittedSessionView.setVisibility(View.GONE);
            pastSessionView1.setVisibility(View.GONE);
            upcomingSessionView1.setVisibility(View.GONE);
            multiSelectSpinnerWithSearch.setItems(listArray1, items -> {
                for (int i = 0; i < items.size(); i++) {
                    if (items.get(i).isSelected()) {
                        tagIds.add(""+items.get(i).getId());
                        //Log.i("TAG", items.get(i).getId() + " : " + items.get(i).getName() + " : " + items.get(i).isSelected());
                    }
                }
            });

        });

        header2.setOnClickListener(v->{
            header2.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
            header.setBackgroundColor(context.getResources().getColor(R.color.white));

            header.setTextColor(context.getResources().getColor(R.color.dark_grey));
            header2.setTextColor(context.getResources().getColor(R.color.white));
            ll1.setVisibility(View.GONE);
            ll2.setVisibility(View.VISIBLE);
            if(sharedPreferences.getString("user_type","").equalsIgnoreCase("3")) {
                if (is_mentor) {
                    submitted.setVisibility(View.VISIBLE);
                    create.setVisibility(View.VISIBLE);
                }
                else {
                    submitted.setVisibility(View.GONE);
                    create.setVisibility(View.GONE);
                }

            }

        });
        header.setOnClickListener(v->{
            header.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
            header2.setBackgroundColor(context.getResources().getColor(R.color.white));

            header.setTextColor(context.getResources().getColor(R.color.white));
            header2.setTextColor(context.getResources().getColor(R.color.dark_grey));
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.GONE);
        });

        upcomingRecyclerView1.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        pastRecyclerView1.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));

        upcomingRecyclerView1.addItemDecoration(new VerticalSpaceItemDecoration(0,0,20,10));
        pastRecyclerView1.addItemDecoration(verticalSpaceItemDecoration);

        upcoming1.setOnClickListener(view -> {
            upcoming1.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            past1.setTextColor(context.getResources().getColor(R.color.dark_grey));
            pastSessionView1.setVisibility(View.GONE);
            upcomingSessionView1.setVisibility(View.VISIBLE);
            submitted.setTextColor(context.getResources().getColor(R.color.dark_grey));
            create.setTextColor(context.getResources().getColor(R.color.dark_grey));
            submittedSessionView.setVisibility(View.GONE);
            createForm.setVisibility(View.GONE);
        });

        past1.setOnClickListener(view -> {
            past1.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            upcoming1.setTextColor(context.getResources().getColor(R.color.dark_grey));
            pastSessionView1.setVisibility(View.VISIBLE);
            upcomingSessionView1.setVisibility(View.GONE);
            submitted.setTextColor(context.getResources().getColor(R.color.dark_grey));
            create.setTextColor(context.getResources().getColor(R.color.dark_grey));
            submittedSessionView.setVisibility(View.GONE);
            createForm.setVisibility(View.GONE);
        });


        if(sharedPreferences.getString("user_type","").equalsIgnoreCase("3")) {
            TrainerSessions("1");
            getMentorStatus();
        }
        if(sharedPreferences.getString("user_type","").equalsIgnoreCase("1"))
            TraineeSessions();

        getLanguages();
        startTimeList();

        language.setOnItemClickListener((parent, view, position, id) -> {
            String selection = (String) parent.getItemAtPosition(position);
            int pos = -1;
            for (int i = 0; i < languageList.size(); i++) {
                if (selection.contains(languageList.get(i).getLanguage())) {
                    pos = i;
                    break;
                }
            }
           languageIds=languageList.get(pos).getId();
        });
        languageRQ.setOnItemClickListener((parent, view, position, id) -> {
            String selection = (String) parent.getItemAtPosition(position);
            int pos = -1;
            for (int i = 0; i < languageList.size(); i++) {
                if (selection.contains(languageList.get(i).getLanguage())) {
                    pos = i;
                    break;
                }
            }
           languageIdsRQ=languageList.get(pos).getId();
        });

        startTime.setOnItemClickListener((parent, view, position, id) -> {
            String selection = (String) parent.getItemAtPosition(position);
            int pos = -1;
            for (int i = 0; i < startTimeModalArrayList.size(); i++) {
                if (selection.contains(startTimeModalArrayList.get(i).getTime())) {
                    pos = i;
                    break;
                }
            }
            startTimeIds= startTimeModalArrayList.get(pos).getId();
            endTime.getText().clear();
            totalHours.setText("");
            endTimeId="";
            when.setText("");
            endTimeList(startTimeIds);
        });
        endTime.setOnItemClickListener((parent, view, position, id) -> {
            String selection = (String) parent.getItemAtPosition(position);
            int pos = -1;
            for (int i = 0; i < endTimeModalArrayList.size(); i++) {
                if (selection.contains(endTimeModalArrayList.get(i).getTime())) {
                    pos = i;
                    break;
                }
            }
            endTimeId= endTimeModalArrayList.get(pos).getId();
            calculateHours();
        });

        languageRQ.setOnTouchListener((view, motionEvent) -> {
            languageRQ.showDropDown();
            return true;
        });
        language.setOnTouchListener((view, motionEvent) -> {
            language.showDropDown();
            return true;
        });
        startTime.setOnTouchListener((view, motionEvent) -> {
            startTime.showDropDown();
            return true;
        });

        endTime.setOnTouchListener((view, motionEvent) -> {
            if (endTimeModalArrayList.size()>0)
                endTime.showDropDown();
            return true;
        });

        when.setOnClickListener(v-> {
            if (!startTimeIds.equalsIgnoreCase("") && !endTimeId.equalsIgnoreCase(""))
                dialog.show();
            else if (startTimeIds.equalsIgnoreCase("")) {
                Toast.makeText(context, getResources().getString(R.string.select_start_time), Toast.LENGTH_SHORT).show();
            }
            else if (endTimeId.equalsIgnoreCase("")) {
                Toast.makeText(context, getResources().getString(R.string.select_end_time), Toast.LENGTH_SHORT).show();
            }
        });
        CalendarView();

        submit.setOnClickListener(v2->{
            ProgressDialog progressDialog = new ProgressDialog(context);
            progressDialog.show();
            progressDialog.setMessage(getResources().getString(R.string.processing));

            if (mRichEditorAction!=null)
                mRichEditorAction.refreshHtml(mRichEditorCallback, onGetHtmlListener);
            if (mRichEditorAction1!=null)
                mRichEditorAction1.refreshHtml(mRichEditorCallback1, onGetHtmlListener1);
            if (mRichEditorAction2!=null)
                mRichEditorAction2.refreshHtml(mRichEditorCallback2, onGetHtmlListener2);

            new Handler().postDelayed(() -> {
                progressDialog.dismiss();
                if (type.getText().toString().trim().matches("")){
                    Snackbar.make(submit,getResources().getString(R.string.pleaseSelectType),Snackbar.LENGTH_LONG).show();
                    return;
                }
                if (status.getText().toString().trim().matches("")){
                    Snackbar.make(submit,getResources().getString(R.string.pleaseSelectStatus),Snackbar.LENGTH_LONG).show();
                    return;
                }
                if (status.getText().toString().equalsIgnoreCase(getResources().getString(R.string.Private))){
                    if (passwordText.getText().toString().equalsIgnoreCase("")) {
                        passwordText.setError(getResources().getString(R.string.fieldRequired));
                        return;
                    }
                }
                if (about.getText().toString().trim().matches("")){
                    about.setError(getResources().getString(R.string.fieldRequired));
                    return;
                }
                if (language.getText().toString().trim().matches("")){
                    Snackbar.make(submit,getResources().getString(R.string.pleaseSelectLanguage),Snackbar.LENGTH_LONG).show();
                    return;
                }
                if (maxParticipants.getText().toString().trim().matches("")){
                    maxParticipants.setError(getResources().getString(R.string.fieldRequired));
                    return;
                }
                if (ticket.getText().toString().trim().matches("")){
                    ticket.setError(getResources().getString(R.string.fieldRequired));
                    return;
                }
                if (startTimeIds.equalsIgnoreCase("")) {
                    Snackbar.make(submit,getResources().getString(R.string.select_start_time),Snackbar.LENGTH_LONG).show();
                    return;
                }
                if (endTimeId.equalsIgnoreCase("")) {
                    Snackbar.make(submit,getResources().getString(R.string.select_end_time),Snackbar.LENGTH_LONG).show();
                    return;
                }
                if (when.getText().toString().trim().matches("")) {
                    when.setError(getResources().getString(R.string.fieldRequired));
                    return;
                }
                if (htmlContent.trim().matches("")){
                    Snackbar.make(submit,getResources().getString(R.string.pleaseWriteOverView),Snackbar.LENGTH_LONG).show();
                    return;
                }
                if (htmlContent1.trim().matches("")){
                    Snackbar.make(submit,getResources().getString(R.string.pleaseWriteWhyAttend),Snackbar.LENGTH_LONG).show();
                    return;
                }
                if (htmlContent2.trim().matches("")){
                    Snackbar.make(submit,getResources().getString(R.string.pleaseWriteWhoShouldAttend),Snackbar.LENGTH_LONG).show();
                    return;
                }

                if (htmlContent.trim().matches("<p><br></p>")){
                    Snackbar.make(submit,getResources().getString(R.string.pleaseWriteOverView),Snackbar.LENGTH_LONG).show();
                    return;
                }
                if (htmlContent1.trim().matches("<p><br></p>")){
                    Snackbar.make(submit,getResources().getString(R.string.pleaseWriteWhyAttend),Snackbar.LENGTH_LONG).show();
                    return;
                }
                if (htmlContent2.trim().matches("<p><br></p>")){
                    Snackbar.make(submit,getResources().getString(R.string.pleaseWriteWhoShouldAttend),Snackbar.LENGTH_LONG).show();
                    return;
                }

                submitSession();
                // close this activity
            }, 2000);

        });

        multiSelectSpinnerWithSearch.setSearchEnabled(true);

        multiSelectSpinnerWithSearch.setSearchHint(context.getResources().getString(R.string.search));

        multiSelectSpinnerWithSearch.setEmptyTitle(context.getResources().getString(R.string.noMatchFound));

        multiSelectSpinnerWithSearch.setShowSelectAllButton(true);

        multiSelectSpinnerWithSearch.setClearText(context.getResources().getString(R.string.close));

        /**
         * If you want to set limit as maximum item should be selected is 2.
         * For No limit -1 or do not call this method.
         *
         */
        multiSelectSpinnerWithSearch.setLimit(5, data -> Toast.makeText(context, getResources().getString(R.string.selectOnlyFive), Toast.LENGTH_LONG).show());

        getTags();

        submitRQ.setOnClickListener(view -> {
            if (aboutRQ.getText().toString().trim().matches("")){
                aboutRQ.setError(getResources().getString(R.string.fieldRequired));
                return;
            }
            if (whenRQ.getText().toString().trim().matches("")){
                whenRQ.setError(getResources().getString(R.string.fieldRequired));
                return;
            }
            if (languageRQ.getText().toString().trim().matches("")){
                Snackbar.make(submit,getResources().getString(R.string.pleaseSelectLanguage),Snackbar.LENGTH_LONG).show();
                return;
            }
            if (noOfParticipants.getText().toString().trim().matches("")){
                noOfParticipants.setError(getResources().getString(R.string.fieldRequired));
                return;
            }
            if (totalPriceRQ.getText().toString().trim().matches("")){
                totalPriceRQ.setError(getResources().getString(R.string.fieldRequired));
                return;
            }
            if (detailsAboutReq.getText().toString().trim().matches("")){
                detailsAboutReq.setError(getResources().getString(R.string.fieldRequired));
                return;
            }
            sendRequestForWorkshop(aboutRQ.getText().toString(),whenRQ.getText().toString(),languageRQ.getText().toString(),
                    totalPriceRQ.getText().toString(),detailsAboutReq.getText().toString(),noOfParticipants.getText().toString());
        });
        return root;
    }

    public void sendRequestForWorkshop(String about, String when, String language, String totalPrice, String detailsAbout, String noOfParticipant) {
        progressDialog.show();
        Map<String,String> param=new HashMap<>();
        param.put("user_id",sharedPreferences.getString("userID",""));
        param.put("about",about);
        param.put("workshope_webinar_time",when);
        param.put("language",language);
        param.put("budget",totalPrice);
        param.put("details_about_your_request",detailsAbout);
        param.put("max_participants",noOfParticipant);

        Log.e("RequestForWorkshopParam",""+param);
        new PostMethod(Api.RequestForWorkshop,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                progressDialog.dismiss();
                Log.e("RequestForWorkshopResp",data);
                try {
                    JSONObject jsonObject=new JSONObject(data);
                    if (jsonObject.optBoolean("status")){
                        aboutRQ.getText().clear();
                        whenRQ.getText().clear();
                        languageRQ.getText().clear();
                        noOfParticipants.getText().clear();
                        totalPriceRQ.getText().clear();
                        detailsAboutReq.getText().clear();
                        aboutRQ.requestFocus();
                    }
                    Toast.makeText(context, jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.dismiss();
                error.printStackTrace();
            }
        });
    }

    private void getMentorStatus() {
        Map<String,String> param=new HashMap<>();
        param.put("trainer_id",sharedPreferences.getString("userID",""));

        Log.e("mentorStatusParam",""+param);
        new PostMethod(Api.getMentorById,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                progressDialog.dismiss();
                Log.e("mentorStatusResp",data);
                try {
                    JSONObject jsonObject=new JSONObject(data);
                    is_mentor = jsonObject.optString("is_mentor").equalsIgnoreCase("1");

                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.dismiss();
                error.printStackTrace();
            }
        });
    }

    private void getTags() {
        new GetMethod(Api.allTags+sharedPreferences.getString("lang",""), context).startmethod(new ResponseData() {
            @Override
            public void response(String data) {
                Log.e("allTags",data);
                try {
                    JSONObject dataObject = new JSONObject(data);
                    JSONArray tag = dataObject.optJSONArray("data");
                    tagsModalArrayList.clear();
                    tagTempNames.clear();
                    for (int me=0; me<tag.length(); me++){

                        TagsModal tagsModal = new TagsModal();
                        JSONObject jsonObject = tag.getJSONObject(me);
                        tagsModal.setTagId(jsonObject.optString("id"));
                        tagsModal.setTagName(jsonObject.optString("tag"));
                        tagsModalArrayList.add(tagsModal);
                        tagTempNames.add(jsonObject.optString("tag"));

                        KeyPairBoolData h = new KeyPairBoolData();
                        h.setId(Long.parseLong(jsonObject.optString("id")));
                        h.setName(jsonObject.optString("tag"));
                        //h.setSelected(me < 5);
                        listArray1.add(h);
                    }
                    //tagsAutocomplete.setAdapter(new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, tagTempNames));
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                error.printStackTrace();
            }
        });

    }

    private void submitSession() {

        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.show();
        progressDialog.setMessage(getResources().getString(R.string.processing));
        Map<String,String> param=new HashMap<>();
        param.put("start_time",startTimeIds);
        param.put("trainer_id",sharedPreferences.getString("userID",""));
        param.put("lang",sharedPreferences.getString("lang",""));
        param.put("end_time",endTimeId);
        param.put("tags", TextUtils.join(",", tagIds));
        param.put("languages",languageIds);
        param.put("status_type",status.getText().toString().toLowerCase());
        param.put("type",type.getText().toString().toLowerCase());
        param.put("about",about.getText().toString());
        param.put("workshope_webinar_time",when.getText().toString());
        param.put("hours",totalHours.getText().toString());
        param.put("ticket_cost",ticket.getText().toString());
        param.put("max_participants",maxParticipants.getText().toString());
        param.put("overview",htmlContent);
        param.put("why_attend",htmlContent1);
        param.put("who_should_attend",htmlContent2);
        param.put("password",setPassword.getText().toString());

        Log.e("createSessionParam",""+param);
        new PostMethod(Api.AddWorkshop,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                progressDialog.dismiss();
                Log.e("createSessionResp",data);
                try {
                    JSONObject jsonObject=new JSONObject(data);
                    if (jsonObject.getBoolean("status")){
                        type.getText().clear();
                        status.getText().clear();
                        about.getText().clear();
                        language.getText().clear();
                        languageIds="";
                        maxParticipants.getText().clear();
                        ticket.getText().clear();
                        startTime.getText().clear();
                        endTime.getText().clear();
                        startTimeIds="";
                        endTimeId="";
                        totalHours.setText("");
                        setPassword.setText("");
                        when.setText("");
                        mRichEditorAction.clearHtml();
                        mRichEditorAction1.clearHtml();
                        mRichEditorAction2.clearHtml();
                        type.requestFocus();
                        htmlContent="";
                        htmlContent1="";
                        htmlContent2="";

                        submitted.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                        past1.setTextColor(context.getResources().getColor(R.color.dark_grey));
                        create.setTextColor(context.getResources().getColor(R.color.dark_grey));
                        upcoming1.setTextColor(context.getResources().getColor(R.color.dark_grey));
                        submittedSessionView.setVisibility(View.VISIBLE);
                        pastSessionView1.setVisibility(View.GONE);
                        createForm.setVisibility(View.GONE);
                        upcomingSessionView1.setVisibility(View.GONE);
                    }
                    Toast.makeText(context, jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.dismiss();
                error.printStackTrace();
            }
        });
    }

    private void setActionBar3() {
        for (int i = 0, size = mActionTypeList.size(); i < size; i++) {

            final ActionImageView actionImageView2 = new ActionImageView(getContext());
            actionImageView2.setLayoutParams(new LinearLayout.LayoutParams(width, width));
            actionImageView2.setPadding(padding, padding, padding, padding);
            actionImageView2.setActionType(mActionTypeList.get(i));
            actionImageView2.setTag(mActionTypeList.get(i));
            actionImageView2.setActivatedColor(R.color.colorAccent);
            actionImageView2.setDeactivatedColor(R.color.tintColor);
            actionImageView2.setRichEditorAction(mRichEditorAction2);
            actionImageView2.setBackgroundResource(R.drawable.btn_colored_material);
            actionImageView2.setImageResource(mActionTypeIconList.get(i));
            actionImageView2.setOnClickListener(v2 -> actionImageView2.command());
            llActionBarContainer2.addView(actionImageView2);
        }
    }

    private void setActionBar2() {
        for (int i = 0, size = mActionTypeList.size(); i < size; i++) {

            final ActionImageView actionImageView1 = new ActionImageView(getContext());
            actionImageView1.setLayoutParams(new LinearLayout.LayoutParams(width, width));
            actionImageView1.setPadding(padding, padding, padding, padding);
            actionImageView1.setActionType(mActionTypeList.get(i));
            actionImageView1.setTag(mActionTypeList.get(i));
            actionImageView1.setActivatedColor(R.color.colorAccent);
            actionImageView1.setDeactivatedColor(R.color.tintColor);
            actionImageView1.setRichEditorAction(mRichEditorAction1);
            actionImageView1.setBackgroundResource(R.drawable.btn_colored_material);
            actionImageView1.setImageResource(mActionTypeIconList.get(i));
            actionImageView1.setOnClickListener(v1 -> actionImageView1.command());
            llActionBarContainer1.addView(actionImageView1);

        }
    }

    private void setActionBar1() {
        for (int i = 0, size = mActionTypeList.size(); i < size; i++) {
            final ActionImageView actionImageView = new ActionImageView(getContext());
            actionImageView.setLayoutParams(new LinearLayout.LayoutParams(width, width));
            actionImageView.setPadding(padding, padding, padding, padding);
            actionImageView.setActionType(mActionTypeList.get(i));
            actionImageView.setTag(mActionTypeList.get(i));
            actionImageView.setActivatedColor(R.color.colorAccent);
            actionImageView.setDeactivatedColor(R.color.tintColor);
            actionImageView.setRichEditorAction(mRichEditorAction);
            actionImageView.setBackgroundResource(R.drawable.btn_colored_material);
            actionImageView.setImageResource(mActionTypeIconList.get(i));
            actionImageView.setOnClickListener(v -> actionImageView.command());
            llActionBarContainer.addView(actionImageView);
        }
    }

    private void initView() {
        LinearLayout linearLayout = root.findViewById(R.id.wv_container);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        mWebView = new WebView(context);
        mWebView.setLayoutParams(layoutParams);
        linearLayout.addView(mWebView);

        mWebView.setWebViewClient(new WebViewClient() {
            @Override public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                return super.shouldOverrideUrlLoading(view, request);
            }
        });

        mWebView.setWebChromeClient(new CustomWebChromeClient());
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setDomStorageEnabled(true);
        mRichEditorCallback = new MRichEditorCallback();
        mWebView.addJavascriptInterface(mRichEditorCallback, "MRichEditor");
        mWebView.loadUrl("file:///android_asset/richEditor.html");
        mRichEditorAction = new RichEditorAction(mWebView);

        LinearLayout linearLayout1 = root.findViewById(R.id.wv_container1);
        LinearLayout.LayoutParams layoutParams1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        mWebView1 = new WebView(context);
        mWebView1.setLayoutParams(layoutParams1);
        linearLayout1.addView(mWebView1);

        mWebView1.setWebViewClient(new WebViewClient() {
            @Override public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                return super.shouldOverrideUrlLoading(view, request);
            }
        });

        mWebView1.setWebChromeClient(new CustomWebChromeClient1());
        mWebView1.getSettings().setJavaScriptEnabled(true);
        mWebView1.getSettings().setDomStorageEnabled(true);
        mRichEditorCallback1 = new MRichEditorCallback1();
        mWebView1.addJavascriptInterface(mRichEditorCallback1, "MRichEditor");
        mWebView1.loadUrl("file:///android_asset/richEditor.html");
        mRichEditorAction1 = new RichEditorAction(mWebView1);

        LinearLayout linearLayout2 = root.findViewById(R.id.wv_container2);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        mWebView2 = new WebView(context);
        mWebView2.setLayoutParams(layoutParams2);
        linearLayout2.addView(mWebView2);

        mWebView2.setWebViewClient(new WebViewClient() {
            @Override public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                return super.shouldOverrideUrlLoading(view, request);
            }
        });

        mWebView2.setWebChromeClient(new CustomWebChromeClient2());
        mWebView2.getSettings().setJavaScriptEnabled(true);
        mWebView2.getSettings().setDomStorageEnabled(true);
        mRichEditorCallback2 = new MRichEditorCallback2();
        mWebView2.addJavascriptInterface(mRichEditorCallback2, "MRichEditor");
        mWebView2.loadUrl("file:///android_asset/richEditor.html");
        mRichEditorAction2 = new RichEditorAction(mWebView2);
    }

    private class CustomWebChromeClient extends WebChromeClient {
        @Override public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);
            if (newProgress == 100) {
                if (!TextUtils.isEmpty(htmlContent)) {
                    mRichEditorAction.insertHtml(htmlContent);
                }
                //KeyboardUtils.showSoftInput((MainActivity)context);
            }
        }

        @Override public void onReceivedTitle(WebView view, String title) {
            super.onReceivedTitle(view, title);
        }
    }
    private class CustomWebChromeClient1 extends WebChromeClient {
        @Override public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);
            if (newProgress == 100) {
                if (!TextUtils.isEmpty(htmlContent1)) {
                    mRichEditorAction1.insertHtml(htmlContent1);
                }
                //KeyboardUtils.showSoftInput((MainActivity)context);
            }
        }

        @Override public void onReceivedTitle(WebView view, String title) {
            super.onReceivedTitle(view, title);
        }
    }
    private class CustomWebChromeClient2 extends WebChromeClient {
        @Override public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);
            if (newProgress == 100) {
                if (!TextUtils.isEmpty(htmlContent2)) {
                    mRichEditorAction2.insertHtml(htmlContent2);
                }
                //KeyboardUtils.showSoftInput((MainActivity)context);
            }
        }

        @Override public void onReceivedTitle(WebView view, String title) {
            super.onReceivedTitle(view, title);
        }
    }

    private final RichEditorCallback.OnGetHtmlListener onGetHtmlListener = html -> {
        if (TextUtils.isEmpty(html)) {
            htmlContent="";
            Snackbar.make(submit,getResources().getString(R.string.fieldRequired),Snackbar.LENGTH_LONG).show();
            return;
        }
        htmlContent =html;
    };
    private final RichEditorCallback.OnGetHtmlListener onGetHtmlListener1 = html -> {
        if (TextUtils.isEmpty(html)) {

            htmlContent1="";
            Snackbar.make(submit,getResources().getString(R.string.pleaseWriteWhyAttend),Snackbar.LENGTH_LONG).show();
            return;
        }
        htmlContent1 =html;
    };
    private final RichEditorCallback.OnGetHtmlListener onGetHtmlListener2 = html -> {
        if (TextUtils.isEmpty(html)) {

            htmlContent2="";
            Snackbar.make(submit,getResources().getString(R.string.pleaseWriteWhoShouldAttend),Snackbar.LENGTH_LONG).show();
        }else
            htmlContent2 =html;

    };

    class MRichEditorCallback extends RichEditorCallback {
        @Override public void notifyFontStyleChange(ActionType type, final String value) {
            ActionImageView actionImageView = llActionBarContainer.findViewWithTag(type);
            if (actionImageView != null) {
                actionImageView.notifyFontStyleChange(type, value);
            }
        }
    }
    class MRichEditorCallback1 extends RichEditorCallback {
        @Override public void notifyFontStyleChange(ActionType type, final String value) {
            ActionImageView actionImageView = llActionBarContainer1.findViewWithTag(type);
            if (actionImageView != null) {
                actionImageView.notifyFontStyleChange(type, value);
            }
        }
    }
    class MRichEditorCallback2 extends RichEditorCallback {
        @Override public void notifyFontStyleChange(ActionType type, final String value) {
            ActionImageView actionImageView = llActionBarContainer2.findViewWithTag(type);
            if (actionImageView != null) {
                actionImageView.notifyFontStyleChange(type, value);
            }
        }
    }

    private void CalendarView() {
        Resources activityRes = context.getResources();
        Configuration activityConf = activityRes.getConfiguration();
        Locale newLocale = new Locale(sharedPreferences.getString("lang",""));
        activityConf.setLocale(newLocale);
        activityRes.updateConfiguration(activityConf, activityRes.getDisplayMetrics());

        Resources applicationRes = context.getResources();
        Configuration applicationConf = applicationRes.getConfiguration();
        applicationConf.setLocale(newLocale);
        applicationRes.updateConfiguration(applicationConf, applicationRes.getDisplayMetrics());

        AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
        LayoutInflater inflater1 = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("InflateParams") final View dialogView = inflater1.inflate(R.layout.multiple_selection_calendar, null);
        alertDialog.setView(dialogView);
        alertDialog.setCancelable(false);
        dialog=alertDialog.create();
        calendarView = dialogView.findViewById(R.id.calendarView);
        TextView submit = dialogView.findViewById(R.id.submit);
        TextView cancel = dialogView.findViewById(R.id.cancel);

        cancel.setOnClickListener(v5-> dialog.dismiss());

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, 30);

        calendarView.setMinimumDate(calendar);
        try {
            calendarView.setDate(calendar.getTime());
        } catch (OutOfDateRangeException e) {
            e.printStackTrace();
        }

        submit.setOnClickListener(v->{
            List<Calendar> selectedDates = calendarView.getSelectedDates();
            String selectedDate="";
            for (int i=0; i<selectedDates.size(); i++){
                Log.e("selectedDate",""+new SimpleDateFormat("dd-MM-yyyy").format(selectedDates.get(i).getTime()));
                if (selectedDate.equalsIgnoreCase(""))
                    selectedDate = new SimpleDateFormat("dd-MM-yyyy").format(selectedDates.get(i).getTime());
                else
                    selectedDate = selectedDate+", "+new SimpleDateFormat("dd-MM-yyyy").format(selectedDates.get(i).getTime());
            }

            if (selectedDate.equalsIgnoreCase(""))
                dialog.dismiss();
            else
                checkDates(selectedDate);
        });


        /*calendarView.setupCalendar(new CalendarDate(calendar.getTime()),new CalendarDate(calendar.getTime()),
            new CalendarDate(calendar1.getTime()), SelectionMode.MULTIPLE,preselectedDates,java.util.Calendar.MONDAY,
            true);*/
    }

    private void checkDates(final String selectedDate) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.show();
        progressDialog.setMessage(getResources().getString(R.string.processing));
        Map<String,String> param=new HashMap<>();
        param.put("start_time",startTimeIds);
        param.put("user_id",sharedPreferences.getString("userID",""));
        param.put("lang",sharedPreferences.getString("lang",""));
        param.put("end_time",endTimeId);
        param.put("no_day",selectedDate);
        param.put("timezone",TimeZone.getDefault().getID());

        Log.e("checkDateParam",""+param);
        new PostMethod(Api.getWorkshopTimeValidate,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                progressDialog.dismiss();
                Log.e("checkDateResp",data);
                try {
                    JSONObject jsonObject=new JSONObject(data);
                    if (jsonObject.getBoolean("status")){
                        when.setText(selectedDate);
                        dialog.dismiss();
                    }else {
                        AlertDialog.Builder alertbuilder = new AlertDialog.Builder(context);
                        alertbuilder.setCancelable(false);
                        alertbuilder.setIcon(context.getResources().getDrawable(R.mipmap.app_icon));
                        alertbuilder.setMessage(jsonObject.optString("message"));
                        alertbuilder.setPositiveButton(getResources().getString(R.string.ok),
                            (dialogInterface, i) -> dialogInterface.dismiss());
                        alertbuilder.create();
                        alertbuilder.show();
                        //dialog.dismiss();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.dismiss();
                error.printStackTrace();
            }
        });
    }

    private void endTimeList(final String startTimeIds) {
        Map<String,String> param=new HashMap<>();
        param.put("start_time",startTimeIds);
        param.put("lang",sharedPreferences.getString("lang",""));
        param.put("timezone",TimeZone.getDefault().getID());

        new PostMethod(Api.WorkshopEndTime,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                Log.e("EndTIme List",data);
                try {
                    JSONObject jsonObject=new JSONObject(data);
                    endTimeModalArrayList.clear();
                    if(jsonObject.optBoolean("status")){
                        JSONArray jsonArray=jsonObject.optJSONArray("data");
                        tempEndList=new ArrayList<>();
                        for (int ar=0;ar<jsonArray.length();ar++){
                            JSONObject object=jsonArray.getJSONObject(ar);
                            TimeModal timeModal=new TimeModal();
                            timeModal.setId(object.optString("id"));
                            timeModal.setTime(object.getString("end_time"));

                            tempEndList.add(object.getString("end_time"));
                            endTimeModalArrayList.add(timeModal);

                        }
                    }
                    endTime.setAdapter(new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, tempEndList));

                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                error.printStackTrace();
            }
        });
    }
    private void calculateHours() {
        Map<String,String> param=new HashMap<>();
        param.put("start_time",startTimeIds);
        param.put("lang",sharedPreferences.getString("lang",""));
        param.put("end_time",endTimeId);

        Log.e("hourParam",""+param);
        new PostMethod(Api.getWorkshopHours,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                Log.e("workshopHour",data);
                try {
                    JSONObject jsonObject=new JSONObject(data);
                    if (jsonObject.getBoolean("status")){
                        totalHours.setText(jsonObject.optString("total_hours"));
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                error.printStackTrace();
            }
        });
    }

    private void startTimeList() {
        Map<String,String> param=new HashMap<>();
        param.put("lang",sharedPreferences.getString("lang",""));
        param.put("timezone",TimeZone.getDefault().getID());

        new PostMethod(Api.WorkshopStartTime,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                Log.e("Start List",data);
                try {
                    JSONObject jsonObject=new JSONObject(data);
                    startTimeModalArrayList.clear();
                    if(jsonObject.optBoolean("status")){
                        JSONArray jsonArray=jsonObject.optJSONArray("data");
                        tempStartList=new ArrayList<>();
                        for (int ar=0;ar<jsonArray.length();ar++){
                            JSONObject object=jsonArray.getJSONObject(ar);
                            TimeModal timeModal=new TimeModal();
                            timeModal.setId(object.optString("id"));
                            timeModal.setTime(object.getString("star_time"));

                            tempStartList.add(object.getString("star_time"));
                            startTimeModalArrayList.add(timeModal);
                        }
                        startTime.setAdapter(new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, tempStartList));

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                error.printStackTrace();
            }
        });
    }

    private void getLanguages() {
        Map<String,String> param=new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));

        new PostMethod(Api.SpokenLanguages,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                Log.e("Language List",data);
                try {
                    JSONObject jsonObject=new JSONObject(data);
                    languageList.clear();
                    if(jsonObject.optBoolean("status")){
                        JSONArray jsonArray=jsonObject.optJSONArray("data");
                        tempList=new ArrayList<>();
                        for (int ar=0;ar<jsonArray.length();ar++){
                            JSONObject object=jsonArray.getJSONObject(ar);
                            LanguageModal languageModal=new LanguageModal();
                            languageModal.setId(object.optString("id"));
                            languageModal.setLanguage(object.getString("language"));

                            tempList.add(object.getString("language"));
                            languageList.add(languageModal);
                        }
                        languageRQ.setAdapter(new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, tempList));
                        language.setAdapter(new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, tempList));

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                error.printStackTrace();
            }
        });
    }

    private void TraineeSessions() {

        Map<String,String> param=new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));
        param.put("trainee_id",sharedPreferences.getString("userID",""));
        //param.put("time_zone",TimeZone.getDefault().getID());

        new PostMethod(Api.TraineeUpcomingSessions,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                Log.e("TraineeUpcomingSession",data);
                progressDialog.cancel();
                try {
                    JSONObject jsonObject=new JSONObject(data);
                    Calendar cal = Calendar.getInstance();

                    if(jsonObject.optBoolean("status")){
                        upcomingModalArrayList1.clear();
                        upcomingRecyclerView1.setVisibility(View.VISIBLE);
                        noUpcomingText1.setVisibility(View.GONE);
                        JSONArray jsonArray=jsonObject.optJSONArray("data");
                        for (int ar=0;ar<jsonArray.length();ar++){
                            JSONObject dataObject=jsonArray.getJSONObject(ar);

                            UpcomingModal upcomingModal=new UpcomingModal();
                            upcomingModal.setID(dataObject.optString("id"));
                            upcomingModal.setBookedDateId(dataObject.optString("booked_date"));
                            upcomingModal.setImage(dataObject.optString("profile_image"));
                            upcomingModal.setBannerImage(dataObject.optString("image"));
                            upcomingModal.setName(dataObject.optString("teacher_name"));
                            upcomingModal.setSessionType(dataObject.optString("type"));
                            upcomingModal.setBookingId(dataObject.optString("booking_id"));
                            upcomingModal.setSessionName(dataObject.optString("about"));
                            upcomingModal.setDuration(dataObject.optString("hours"));
                            upcomingModal.setRate(dataObject.optString("ticket_cost"));
                            upcomingModal.setLanguage(dataObject.optString("lang_name"));
                            upcomingModal.setWizIQ(dataObject.optString("student_url"));

                            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                            SimpleDateFormat sdfOutPutToSend = new SimpleDateFormat("hh:mm a");
                            String time="";
                            try {
                                Date start  = sdf.parse(dataObject.optString("start_time"));
                                String start_time = sdfOutPutToSend.format(start);

                                time=start_time;
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            String dateStr = dataObject.optString("workshope_time")+","+dataObject.optString("start_time");
                            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd,HH:mm:ss",  new Locale(sharedPreferences.getString("lang",""),lang));
                            df.setTimeZone(TimeZone.getTimeZone("UTC"));

                            Date date = df.parse(dateStr);
                            df.setTimeZone(TimeZone.getDefault());
                            String formattedDate = df.format(date);
                            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd,HH:mm:ss");

                            SimpleDateFormat sdfOutPutToSend1 = new SimpleDateFormat("hh:mm a, EEEE-dd MMM, yyyy");
                            Date start  = sdf1.parse(formattedDate);
                            String csv = sdfOutPutToSend1.format(start);
                            Log.e("time1",""+csv);
                            upcomingModal.setDateTime(csv);

                            SimpleDateFormat sdf21 = new SimpleDateFormat("yyyy-MM-dd,hh:mm a");
                            sdf21.setTimeZone(TimeZone.getTimeZone("UTC"));

                            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd,hh:mm a");
                            df.setTimeZone(TimeZone.getTimeZone("UTC"));
                            Date tempDate  = sdf2.parse(sdf2.format(df.parse(dateStr)));
                            Log.e("time2",""+sdf2.format(df.parse(dateStr)));
                            long timeInMilliseconds = tempDate.getTime();

                            /*SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd,hh:mm a");
                            Date tempDate  = sdf2.parse(dataObject.optString("workshope_time")+","+time);
                            long timeInMilliseconds = tempDate.getTime();*/

                            cal.setTimeZone(TimeZone.getTimeZone("UTC"));
                            @SuppressLint("SimpleDateFormat") SimpleDateFormat outputFmt = new SimpleDateFormat("yyyy-MM-dd,hh:mm a");
                            outputFmt.setTimeZone(TimeZone.getTimeZone("UTC"));
                            Date time1 = sdf2.parse(outputFmt.format(cal.getTime()));
                            Log.i("time3","timeInMilliseconds: "+timeInMilliseconds+" ===== "+"currentTime: "+time1.getTime());
                            cal.setTimeZone(timeZoneId);

                            Date now = new Date();
                            TimeZone.setDefault(TimeZone.getTimeZone("UTC"));

                            upcomingModal.setTimer(timeInMilliseconds-now.getTime());
                            TimeZone.setDefault(timeZoneId);
                            upcomingModal.setSortingDate(timeInMilliseconds);
                            Date currentDate = sdf2.parse(outputFmt.format(cal.getTime()));

                            int dateDifference = (int) TimeUnit.HOURS.convert(tempDate.getTime()-currentDate.getTime(),TimeUnit.MILLISECONDS);
                            upcomingModal.setIsReschedule(dateDifference > 24);

                            int dateDifferenceInMin = (int) TimeUnit.MINUTES.convert(tempDate.getTime()-currentDate.getTime(),TimeUnit.MILLISECONDS);
                            upcomingModal.setIsSendPDF(dateDifferenceInMin <= 30 && dateDifferenceInMin >= -30);

                            upcomingModalArrayList1.add(upcomingModal);
                        }
                        upcomingAdapter1=new MyUpcomingAdapter(context,upcomingModalArrayList1,MyClassesFragment.this);
                        upcomingRecyclerView1.setAdapter(upcomingAdapter1);
                    }
                    else {
                        upcomingRecyclerView1.setVisibility(View.GONE);
                        noUpcomingText1.setVisibility(View.VISIBLE);
                        noUpcomingText1.setText(context.getResources().getString(R.string.noUpcomingSession));
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();
                error.printStackTrace();
            }
        });

        new PostMethod(Api.TraineePastSessions,param,context).startPostMethod(new ResponseData() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void response(String data) {
                Log.e("TraineePastSession",data);
                try {
                    JSONObject jsonObject = new JSONObject(data);

                    if (jsonObject.optBoolean("status")) {
                        pastModalArrayList1.clear();
                        pastRecyclerView1.setVisibility(View.VISIBLE);
                        noPastText1.setVisibility(View.GONE);
                        JSONArray jsonArray = jsonObject.optJSONArray("data");
                        for (int ar = 0; ar < jsonArray.length(); ar++) {
                            JSONObject dataObject=jsonArray.getJSONObject(ar);
                            PastModal pastModal=new PastModal();
                            pastModal.setID(dataObject.optString("booking_id"));
                            pastModal.setImage(dataObject.optString("profile_image"));
                            pastModal.setBanner(dataObject.optString("image"));
                            pastModal.setName(dataObject.optString("teacher_name"));
                            pastModal.setSessionType(dataObject.optString("type"));
                            pastModal.setSessionName(dataObject.optString("about"));
                            pastModal.setTeacherID(dataObject.optString("teacher_id"));

                            pastModal.setButtonName(dataObject.optString("rating_to_rated_workshop"));
                            pastModal.setURL(dataObject.optString("download_url"));

                            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                            SimpleDateFormat sdfOutPutToSend = new SimpleDateFormat("hh:mm a");
                            String time="";
                            try {
                                Date start  = sdf.parse(dataObject.optString("start_time"));
                                String start_time = sdfOutPutToSend.format(start);

                                time=start_time;

                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            //String dateStr = dataObject.optJSONArray("workshop_dates").getJSONObject(0).optString("workshope_time")+","+dataObject.optString("start_time");
                            String dateStr =dataObject.optString("start_time");
                            //SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd,HH:mm:ss", new Locale(sharedPreferences.getString("lang",""),lang));
                            SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss", new Locale(sharedPreferences.getString("lang",""),lang));
                            df.setTimeZone(TimeZone.getTimeZone("UTC"));

                            Date date = df.parse(dateStr);
                            df.setTimeZone(TimeZone.getDefault());
                            String formattedDate = df.format(date);
                            //SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd,HH:mm:ss");
                            SimpleDateFormat sdf1 = new SimpleDateFormat("HH:mm:ss");

                            //SimpleDateFormat sdfOutPutToSend1 = new SimpleDateFormat("hh:mm a, EEEE-dd MMM, yyyy");
                            SimpleDateFormat sdfOutPutToSend1 = new SimpleDateFormat("hh:mm a");

                            Date start  = sdf1.parse(formattedDate);
                            sdfOutPutToSend1.setTimeZone(TimeZone.getDefault());
                            //String csv = sdfOutPutToSend1.format(start);
                            String csv = dataObject.optString("start_time_android");

                            sdf1 = new SimpleDateFormat("yyyy-MM-dd");
                            sdfOutPutToSend1 = new SimpleDateFormat("MMMM-EEEE dd, yyyy");

                            JSONArray workshopDates=dataObject.optJSONArray("workshop_dates");
                            ArrayList<String> dateArray=new ArrayList<>();
                            for(int i=0;i<workshopDates.length();i++){
                                try {
                                    JSONObject object=workshopDates.optJSONObject(i);
                                    String workshopDate = sdfOutPutToSend1.format(sdf1.parse(object.optString("workshope_time")));
                                    dateArray.add(workshopDate);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }

                            csv = csv+"\n"+android.text.TextUtils.join("\n", dateArray);
                            pastModal.setDateTime(csv);

                            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd,hh:mm a");
                            Date tempDate  = sdf2.parse(dataObject.optString("workshope_time")+","+time);
                            long timeInMilliseconds = tempDate.getTime();
                            Calendar cal = Calendar.getInstance();
                            cal.setTimeZone(TimeZone.getTimeZone("UTC"));
                            @SuppressLint("SimpleDateFormat") SimpleDateFormat outputFmt = new SimpleDateFormat("yyyy-MM-dd,HH:mm a");
                            outputFmt.setTimeZone(TimeZone.getTimeZone("UTC"));
                            pastModal.setSortingDate(timeInMilliseconds);
                            cal.setTimeZone(timeZoneId);

                            pastModal.setDuration(dataObject.optString("hours"));
                            pastModal.setRate(dataObject.optString("ticket_cost"));
                            pastModal.setRateByTrainee(dataObject.optString("rate_by_trainee"));
                            pastModal.setLanguage(dataObject.optString("lang_name"));

                            pastModalArrayList1.add(pastModal);
                        }
                        if(pastModalArrayList1.size()>0){
                            Collections.sort(pastModalArrayList1, (pastModal, pastModal1) -> {
                                long date = pastModal.getSortingDate();
                                long date1 = pastModal1.getSortingDate();
                                if(date > date1) {
                                    return -1;
                                } else {
                                    return 1;
                                }
                            });
                        }

                        pastAdapter1=new MyPastAdapter(context,pastModalArrayList1);
                        pastRecyclerView1.setAdapter(pastAdapter1);
                    }else {
                        pastRecyclerView1.setVisibility(View.GONE);
                        noPastText1.setVisibility(View.VISIBLE);
                        noPastText1.setText(context.getResources().getString(R.string.noPastSession));
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                error.printStackTrace();
            }
        });
    }

    private void TrainerSessions(String type) {
        past1.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        upcoming1.setTextColor(context.getResources().getColor(R.color.dark_grey));
        submitted.setTextColor(context.getResources().getColor(R.color.dark_grey));
        pastSessionView1.setVisibility(View.VISIBLE);
        upcomingSessionView1.setVisibility(View.GONE);
        submittedSessionView.setVisibility(View.GONE);
        createForm.setVisibility(View.GONE);
        String api="";

        Map<String,String> param=new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));
        param.put("trainer_id",sharedPreferences.getString("userID",""));
        param.put("time_zone",TimeZone.getDefault().getID());
        Log.e("TrainerPastSession","Param===>"+param);

        new PostMethod(Api.TrainerPastSessions,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                Log.e("TrainerPastSession",data);
                try {
                    JSONObject jsonObject = new JSONObject(data);

                    if (jsonObject.optBoolean("status")) {
                        pastModalArrayList1.clear();
                        pastRecyclerView1.setVisibility(View.VISIBLE);
                        noPastText1.setVisibility(View.GONE);
                        JSONArray jsonArray = jsonObject.optJSONArray("data");
                        for (int ar = 0; ar < jsonArray.length(); ar++) {
                            JSONObject dataObject=jsonArray.getJSONObject(ar);
                            PastModal pastModal=new PastModal();
                            pastModal.setID(dataObject.optString("id"));
                            pastModal.setImage(dataObject.optString("profile_image"));
                            pastModal.setBanner(dataObject.optString("image"));
                            pastModal.setName(dataObject.optString("teacher_name"));
                            pastModal.setTeacherID(dataObject.optString("teacher_id"));
                            pastModal.setBookingID(dataObject.optString("booking_id"));
                            pastModal.setSessionType(dataObject.optString("type"));
                            pastModal.setSessionName(dataObject.optString("about"));

                            pastModal.setButtonName(dataObject.optString("rating_to_rated_workshop"));
                            pastModal.setURL(dataObject.optString("download_url"));

                            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                            SimpleDateFormat sdfOutPutToSend = new SimpleDateFormat("hh:mm a");
                            String time="";
                            try {
                                Date start  = sdf.parse(dataObject.optString("start_time"));
                                String start_time = sdfOutPutToSend.format(start);

                                time=start_time;
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            //String dateStr = dataObject.optString("workshope_time")+","+dataObject.optString("start_time");
                            //SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd,HH:mm:ss", new Locale(sharedPreferences.getString("lang",""),lang));
                            //SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd,HH:mm:ss");
                            //SimpleDateFormat sdfOutPutToSend1 = new SimpleDateFormat("hh:mm a, EEEE-dd MMM, yyyy");

                            String dateStr = dataObject.optString("start_time");
                            SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss", new Locale(sharedPreferences.getString("lang",""),lang));
                            df.setTimeZone(TimeZone.getTimeZone("UTC"));

                            Date date = df.parse(dateStr);
                            df.setTimeZone(TimeZone.getDefault());
                            String formattedDate = df.format(date);
                            SimpleDateFormat sdf1 = new SimpleDateFormat("HH:mm:ss");

                            SimpleDateFormat sdfOutPutToSend1 = new SimpleDateFormat("hh:mm a");
                            Date start  = sdf1.parse(formattedDate);
                            //String csv = sdfOutPutToSend1.format(start);

                            String csv = dataObject.optString("start_time_android");
                            pastModal.setTime(dateStr);


                            sdf1 = new SimpleDateFormat("yyyy-MM-dd");
                            sdfOutPutToSend1 = new SimpleDateFormat("MMMM-EEEE dd, yyyy");

                            JSONArray workshopDates=dataObject.optJSONArray("workshop_dates");
                            ArrayList<String> dateArray=new ArrayList<>();
                            for(int i=0;i<workshopDates.length();i++){
                                try {
                                    JSONObject object=workshopDates.optJSONObject(i);
                                    String workshopDate = sdfOutPutToSend1.format(sdf1.parse(object.optString("workshope_time")));
                                    dateArray.add(workshopDate);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }

                            csv = csv+"\n"+android.text.TextUtils.join("\n", dateArray);

                            pastModal.setDateTime(csv);

                            pastModal.setDuration(dataObject.optString("hours"));
                            pastModal.setRate(dataObject.optString("ticket_cost"));

                            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd,hh:mm a");
                            Date tempDate  = sdf2.parse(dataObject.optString("workshope_time")+","+time);
                            long timeInMilliseconds = tempDate.getTime();
                            Calendar cal = Calendar.getInstance();
                            cal.setTimeZone(TimeZone.getTimeZone("UTC"));
                            @SuppressLint("SimpleDateFormat") SimpleDateFormat outputFmt = new SimpleDateFormat("yyyy-MM-dd,HH:mm a");
                            outputFmt.setTimeZone(TimeZone.getTimeZone("UTC"));
                            pastModal.setSortingDate(timeInMilliseconds);
                            cal.setTimeZone(timeZoneId);

                            pastModalArrayList1.add(pastModal);
                        }
                        if(pastModalArrayList1.size()>0){
                            Collections.sort(pastModalArrayList1, (pastModal, pastModal1) -> {
                                long date = pastModal.getSortingDate();
                                long date1 = pastModal1.getSortingDate();
                                if(date > date1) {
                                    return -1;
                                } else {
                                    return 1;
                                }
                            });
                        }

                        TrainerPastAdapter trainerPastAdapter=new TrainerPastAdapter(context,pastModalArrayList1);
                        pastRecyclerView1.setAdapter(trainerPastAdapter);
                    }else {
                        pastRecyclerView1.setVisibility(View.GONE);
                        noPastText1.setVisibility(View.VISIBLE);
                        noPastText1.setText(context.getResources().getString(R.string.noPastSession));
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                error.printStackTrace();
            }
        });


        upcoming1.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        past1.setTextColor(context.getResources().getColor(R.color.dark_grey));
        submitted.setTextColor(context.getResources().getColor(R.color.dark_grey));
        pastSessionView1.setVisibility(View.GONE);
        submittedSessionView.setVisibility(View.GONE);
        createForm.setVisibility(View.GONE);
        upcomingSessionView1.setVisibility(View.VISIBLE);

        new PostMethod(Api.TrainerUpcomingSessions,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                Log.e("TrainerUpcomingSession",data);
                progressDialog.cancel();
                try {
                    JSONObject jsonObject=new JSONObject(data);
                    Calendar cal = Calendar.getInstance();

                    if(jsonObject.optBoolean("status")){
                        upcomingModalArrayList1.clear();
                        upcomingRecyclerView1.setVisibility(View.VISIBLE);
                        noUpcomingText1.setVisibility(View.GONE);
                        JSONArray jsonArray=jsonObject.optJSONArray("data");
                        for (int ar=0;ar<jsonArray.length();ar++){
                            JSONObject dataObject=jsonArray.getJSONObject(ar);

                            UpcomingModal upcomingModal=new UpcomingModal();
                            upcomingModal.setID(dataObject.optString("id"));
                            upcomingModal.setBookedDateId(dataObject.optString("booked_date_id"));
                            upcomingModal.setImage(dataObject.optString("profile_image"));
                            upcomingModal.setBannerImage(dataObject.optString("image"));
                            upcomingModal.setName(dataObject.optString("teacher_name"));
                            upcomingModal.setTeacherID(dataObject.optString("teacher_id"));
                            upcomingModal.setSessionType(dataObject.optString("type"));
                            upcomingModal.setSessionName(dataObject.optString("about"));
                            upcomingModal.setDuration(dataObject.optString("hours"));
                            upcomingModal.setRate(dataObject.optString("ticket_cost"));
                            upcomingModal.setWizIQ(dataObject.optString("teacher_url"));
                            upcomingModal.setAttend(dataObject.optString("attend"));

                            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                            SimpleDateFormat sdfOutPutToSend = new SimpleDateFormat("hh:mm a");
                            String time="";
                            try {
                                Date start  = sdf.parse(dataObject.optString("start_time"));
                                String start_time = sdfOutPutToSend.format(start);

                                time=start_time;
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            String dateStr = dataObject.optString("workshope_time")+","+dataObject.optString("start_time");
                            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd,HH:mm:ss", new Locale(sharedPreferences.getString("lang",""),lang));
                            df.setTimeZone(TimeZone.getTimeZone("UTC"));

                            Date date = df.parse(dateStr);
                            df.setTimeZone(TimeZone.getDefault());
                            String formattedDate = df.format(date);
                            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd,HH:mm:ss");

                            SimpleDateFormat sdfOutPutToSend1 = new SimpleDateFormat("hh:mm a, EEEE-dd MMM, yyyy");
                            Date start  = sdf1.parse(formattedDate);
                            String csv = sdfOutPutToSend1.format(start);
                            //Log.e("time1",""+csv);
                            upcomingModal.setDateTime(csv);

                            SimpleDateFormat sdf21 = new SimpleDateFormat("yyyy-MM-dd,hh:mm a");
                            sdf21.setTimeZone(TimeZone.getTimeZone("UTC"));

                            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd,hh:mm a");
                            df.setTimeZone(TimeZone.getTimeZone("UTC"));
                            Date tempDate  = sdf2.parse(sdf2.format(df.parse(dateStr)));
                            //Log.i("time2",""+sdf2.format(df.parse(dateStr)));
                            long timeInMilliseconds = tempDate.getTime();

                            /*SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd,hh:mm a");
                            Date tempDate  = sdf2.parse(dataObject.optString("workshope_time")+","+time);
                            long timeInMilliseconds = tempDate.getTime();*/

                            Date now = new Date();
                            TimeZone.setDefault(TimeZone.getTimeZone("UTC"));

                            cal.setTimeZone(TimeZone.getTimeZone("UTC"));
                            @SuppressLint("SimpleDateFormat") SimpleDateFormat outputFmt = new SimpleDateFormat("yyyy-MM-dd,HH:mm a");
                            outputFmt.setTimeZone(TimeZone.getTimeZone("UTC"));
                            Date time1 = sdf2.parse(outputFmt.format(cal.getTime()));
                            upcomingModal.setTimer(timeInMilliseconds-now.getTime());
                            upcomingModal.setSortingDate(timeInMilliseconds);
                            Date currentDate = sdf2.parse(outputFmt.format(cal.getTime()));

                            cal.setTimeZone(timeZoneId);
                            TimeZone.setDefault(timeZoneId);

                            int dateDifference = (int) TimeUnit.HOURS.convert(tempDate.getTime()-currentDate.getTime(),TimeUnit.MILLISECONDS);
                            upcomingModal.setIsReschedule(dateDifference > 24);

                            int dateDifferenceInMin = (int) TimeUnit.MINUTES.convert(tempDate.getTime()-currentDate.getTime(),TimeUnit.MILLISECONDS);
                            upcomingModal.setIsSendPDF(dateDifferenceInMin <= 30 && dateDifferenceInMin >= -30);

                            upcomingModalArrayList1.add(upcomingModal);
                        }
                        if(upcomingModalArrayList1.size()>0){
                            Collections.sort(upcomingModalArrayList1, (upcomingModal, upcomingModal1) -> {
                                long date = upcomingModal.getSortingDate();
                                long date1 = upcomingModal1.getSortingDate();
                                if(date < date1) {
                                    return -1;
                                } else {
                                    return 1;
                                }
                            });
                        }

                        TrainerUpcomingAdapter trainerUpcomingAdapter =new TrainerUpcomingAdapter(context,upcomingModalArrayList1,MyClassesFragment.this);
                        upcomingRecyclerView1.setAdapter(trainerUpcomingAdapter);
                    }
                    else {
                        upcomingRecyclerView1.setVisibility(View.GONE);
                        noUpcomingText1.setVisibility(View.VISIBLE);
                        noUpcomingText1.setText(context.getResources().getString(R.string.noUpcomingSession));
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();
                error.printStackTrace();
            }
        });

        new PostMethod(Api.TrainerSubmittedSessions,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                Log.e("TrainerSubmitted",data);
                try {
                    JSONObject jsonObject=new JSONObject(data);
                    if(jsonObject.optBoolean("status")){
                        submittedModalArrayList.clear();
                        submittedRecyclerView.setVisibility(View.VISIBLE);
                        noSubmittedText.setVisibility(View.GONE);
                        JSONArray jsonArray=jsonObject.optJSONArray("data");
                        for (int vk=0;vk<jsonArray.length();vk++){
                            JSONObject dataObject=jsonArray.getJSONObject(vk);

                            SubmittedModal submittedModal=new SubmittedModal();
                            submittedModal.setID(dataObject.optString("id"));
                            submittedModal.setBanner(dataObject.optString("image"));
                            submittedModal.setName(dataObject.optString("teacher_name"));
                            submittedModal.setSessionType(dataObject.optString("type"));
                            submittedModal.setSessionName(dataObject.optString("about"));
                            submittedModal.setDuration(dataObject.optString("hours"));
                            submittedModal.setRate(dataObject.optString("ticket_cost"));

                            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
                            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

                            SimpleDateFormat sdfOutPutToSend = new SimpleDateFormat("hh:mm a");
                            sdfOutPutToSend.setTimeZone(TimeZone.getDefault());
                            String time="";
                            try {
                                Date start  = sdf.parse(dataObject.optString("start_time"));
                                String start_time = sdfOutPutToSend.format(start);

                                time=start_time;
                                //Log.e("time",time);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
                            //sdf1.setTimeZone(TimeZone.getTimeZone("UTC"));

                            SimpleDateFormat sdfOutPutToSend1 = new SimpleDateFormat("MMMM-EEEE dd, yyyy");
                            //sdfOutPutToSend1.setTimeZone(TimeZone.getDefault());

                            JSONArray workshopDates=dataObject.optJSONArray("workshope_time");
                            ArrayList<String>dateArray=new ArrayList<>();
                            for(int ar=0;ar<workshopDates.length();ar++){
                                try {
                                    JSONObject object=workshopDates.optJSONObject(ar);
                                    Date start  = sdf1.parse(object.optString("workshope_time"));
                                    String workshopDate = sdfOutPutToSend1.format(start);
                                    dateArray.add(workshopDate);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }
                            String csv = dataObject.optString("start_time_android")+"\n"+android.text.TextUtils.join("\n", dateArray);

                            submittedModal.setDateTime(csv);
                            submittedModalArrayList.add(submittedModal);
                        }

                        TrainerSubmittedAdapter submittedAdapter=new TrainerSubmittedAdapter(context,submittedModalArrayList);
                        submittedRecyclerView.setAdapter(submittedAdapter);
                    }else {
                        submittedRecyclerView.setVisibility(View.GONE);
                        noSubmittedText.setVisibility(View.VISIBLE);
                        noSubmittedText.setText(context.getResources().getString(R.string.noSubmitBooking));
                    }
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                error.printStackTrace();
            }
        });
    }

    private void Past(String user_type) {
        past.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        upcoming.setTextColor(context.getResources().getColor(R.color.dark_grey));
        request.setTextColor(context.getResources().getColor(R.color.dark_grey));
        requested.setTextColor(context.getResources().getColor(R.color.dark_grey));
        pastSessionView.setVisibility(View.VISIBLE);
        upcomingSessionView.setVisibility(View.GONE);
        requestForm.setVisibility(View.GONE);
        requestedView.setVisibility(View.GONE);


        String api="";
        Map<String,String> param=new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));
        if(user_type.equalsIgnoreCase("2")) {
            api=Api.CorporatePastSessions;
            param.put("corporate_id",sharedPreferences.getString("userID",""));
            param.put("time_zone",TimeZone.getDefault().getID());
        }else  if(user_type.equalsIgnoreCase("3")) {
            api=Api.TrainerOneToOnePastBooking;
            param.put("trainer_id",sharedPreferences.getString("userID",""));
            param.put("time_zone",TimeZone.getDefault().getID());

        }else {
            api=Api.TraineeOneToOnePastBooking;
            param.put("trainee_id",sharedPreferences.getString("userID",""));
            param.put("time_zone",TimeZone.getDefault().getID());
        }

        Log.e("PastSession","Param===>"+param);
        new PostMethod(api,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                Log.e("pastResp",data);
                progressDialog.cancel();
                try {
                    Calendar cal = Calendar.getInstance();
                    JSONObject jsonObject=new JSONObject(data);
                    if(jsonObject.optBoolean("status")){
                        pastModalArrayList.clear();
                        JSONArray jsonArray=jsonObject.optJSONArray("data");
                        for (int ar=0;ar<jsonArray.length();ar++){
                            JSONObject dataObj=jsonArray.getJSONObject(ar);
                            ArrayList<AttendeesModal> attendees=new ArrayList<>();
                            PastModal pastModal=new PastModal();
                            pastModal.setID(dataObj.optString("id"));
                            //Log.e("booking ID",dataObj.optString("id"));
                            pastModal.setTeacherID(dataObj.optString("teacher_id"));
                            pastModal.setName(dataObj.optString("name"));
                            pastModal.setBanner(dataObj.optString("image"));
                            pastModal.setSessionType(dataObj.optString("course_name")+"-"+dataObj.optString("subject_name"));
                            pastModal.setBookingType(dataObj.optString("booking_type"));
                            pastModal.setTagUrl(dataObj.optString("booking_tag"));
                            if(sharedPreferences.getString("user_type","").equalsIgnoreCase("2")) {
                                pastModal.setSessionType(dataObj.optString("type"));
                                pastModal.setSessionName(dataObj.optString("about"));
                                for(int vk=0;vk<dataObj.optJSONArray("list_of_attendees").length();vk++){
                                    JSONObject obj = dataObj.optJSONArray("list_of_attendees").getJSONObject(vk);
                                    AttendeesModal attendeesModal=new AttendeesModal();
                                    attendeesModal.setName(obj.optString("name"));
                                    attendeesModal.setAttendance(obj.optString("notify"));

                                    attendees.add(attendeesModal);
                                }
                                pastModal.setAttendeesList(attendees);
                            }

                            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
                            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

                            SimpleDateFormat sdfOutPutToSend = new SimpleDateFormat("hh:mm a");
                            sdfOutPutToSend.setTimeZone(TimeZone.getDefault());
                            String time="";
                            try {
                                Date start  = sdf.parse(dataObj.optString("start"));
                                String start_time = sdfOutPutToSend.format(start);

                                time=start_time;
                                //Log.e("time",time);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd,hh:mm a");
                            Date tempDate  = sdf2.parse(dataObj.optString("booking_date")+","+time);
                            long timeInMilliseconds = tempDate.getTime();
                            pastModal.setSortingDate(timeInMilliseconds);

                            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
                            //sdf1.setTimeZone(TimeZone.getTimeZone("UTC"));

                            SimpleDateFormat sdfOutPutToSend1 = new SimpleDateFormat("EEEE-dd MMM, yyyy");
                            //sdfOutPutToSend1.setTimeZone(TimeZone.getDefault());

                            Date start  = sdf1.parse(dataObj.optString("booking_date"));
                            String workshopDate = sdfOutPutToSend1.format(start);

                            String csv = dataObj.optString("start_time_android")+", "+workshopDate;

                            pastModal.setDateTime(csv);
                            pastModal.setRateByTrainee(dataObj.optString("rate_by_trainee"));
                            pastModal.setDuration(dataObj.optString("hours"));
                            pastModal.setRate(dataObj.optString("payable_amount"));
                            pastModalArrayList.add(pastModal);

                        }
                        if(pastModalArrayList.size()>0){
                            Collections.sort(pastModalArrayList, (pastModal, pastModal1) -> {
                                long date = pastModal.getSortingDate();
                                long date1 = pastModal1.getSortingDate();
                                if(date > date1) {
                                    return -1;
                                } else {
                                    return 1;
                                }
                            });
                        }

                        if(sharedPreferences.getString("user_type","").equalsIgnoreCase("2")) {
                            sessionsPastAdapter=new SessionsPastAdapter(context,pastModalArrayList);
                            pastRecyclerView.setAdapter(sessionsPastAdapter);
                        }else if(sharedPreferences.getString("user_type","").equalsIgnoreCase("3")) {
                            TrainerPastBookingAdapter trainerPastAdapter=new TrainerPastBookingAdapter(context,pastModalArrayList);
                            //TrainerPastAdapter trainerPastAdapter=new TrainerPastAdapter(context,pastModalArrayList);
                            pastRecyclerView.setAdapter(trainerPastAdapter);
                        }else {
                            pastAdapter=new MyClassPastAdapter(context,pastModalArrayList);
                            pastRecyclerView.setAdapter(pastAdapter);
                        }
                        pastRecyclerView.setVisibility(View.VISIBLE);
                        noPastText.setVisibility(View.GONE);
                    }else {
                        pastRecyclerView.setVisibility(View.GONE);
                        noPastText.setVisibility(View.VISIBLE);
                        noPastText.setText(context.getResources().getString(R.string.noPastBooking));
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();
                error.printStackTrace();
            }
        });
    }

    public void Upcoming(String user_type) {
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();

        upcoming.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        request.setTextColor(context.getResources().getColor(R.color.dark_grey));
        past.setTextColor(context.getResources().getColor(R.color.dark_grey));
        pastSessionView.setVisibility(View.GONE);
        requestForm.setVisibility(View.GONE);
        requestedView.setVisibility(View.GONE);
        upcomingSessionView.setVisibility(View.VISIBLE);

        String api="";
        Map<String,String> param=new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));
        if(user_type.equalsIgnoreCase("2")) {
            api=Api.CorporateUpcomingSessions;
            param.put("corporate_id",sharedPreferences.getString("userID",""));
        }else  if(user_type.equalsIgnoreCase("3")) {
            api=Api.TrainerOneToOneUpcomingBooking;
            param.put("trainer_id",sharedPreferences.getString("userID",""));
        }else {
            api=Api.TraineeOneToOneUpcomingBooking;
            param.put("trainee_id",sharedPreferences.getString("userID",""));
        }

        Log.e("UpcomingParam",""+param);
        new PostMethod(api,param,context).startPostMethod(new ResponseData() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void response(String data) {
                Log.e("PrivateResp",data);
                try {
                    progressDialog.cancel();
                    JSONObject jsonObject=new JSONObject(data);
                    if(jsonObject.optBoolean("status")){
                        upcomingModalArrayList.clear();
                        Calendar cal=Calendar.getInstance();
                        JSONArray jsonArray=jsonObject.optJSONArray("data");
                        for (int ar=0;ar<jsonArray.length();ar++){
                            JSONObject dataObj=jsonArray.getJSONObject(ar);
                            ArrayList<AttendeesModal> attendees=new ArrayList<>();
                            UpcomingModal upcomingModal=new UpcomingModal();
                            upcomingModal.setID(dataObj.optString("id"));
                            upcomingModal.setName(dataObj.optString("name"));
                            upcomingModal.setSessionType(dataObj.optString("course_name")+"-"+dataObj.optString("subject_name"));

                            if (dataObj.has("student_url")) {
                                upcomingModal.setShowAttend(true);
                                upcomingModal.setWizIQ(dataObj.optString("student_url"));
                            }

                            if (dataObj.has("teacher_url")) {
                                upcomingModal.setShowAttend(false);
                                upcomingModal.setWizIQ(dataObj.optString("teacher_url"));
                            }

                            /*if(sharedPreferences.getString("user_type","").equalsIgnoreCase("3")) {
                                upcomingModal.setWizIQ(dataObj.optString("teacher_url"));
                            }*/
                            if(sharedPreferences.getString("user_type","").equalsIgnoreCase("2")) {
                                upcomingModal.setURL(dataObj.optString("corporate_student_url"));
                                upcomingModal.setSessionType(dataObj.optString("type"));
                                upcomingModal.setSessionName(dataObj.optString("about"));
                                for(int vk=0;vk<dataObj.optJSONArray("list_of_attendees").length();vk++){
                                    JSONObject obj = dataObj.optJSONArray("list_of_attendees").getJSONObject(vk);
                                    AttendeesModal attendeesModal=new AttendeesModal();
                                    attendeesModal.setName(obj.optString("name"));
                                    attendeesModal.setAttendance("3");
                                    attendees.add(attendeesModal);
                                }
                                upcomingModal.setAttendeesList(attendees);
                            }

                            /*SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                            SimpleDateFormat sdfOutPutToSend = new SimpleDateFormat("hh:mm a");
                            String time="";*/
                           /* try {
                                Date start  = sdf.parse(dataObj.optString("start"));
                                String start_time = sdfOutPutToSend.format(start);

                                time=start_time;
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }*/

                            String dateStr = dataObj.optString("booking_date")+","+dataObj.optString("start");
                            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd,HH:mm:ss", new Locale(sharedPreferences.getString("lang",""),lang));
                            df.setTimeZone(TimeZone.getTimeZone("UTC"));

                            SimpleDateFormat sdf21 = new SimpleDateFormat("yyyy-MM-dd,hh:mm a");
                            sdf21.setTimeZone(TimeZone.getTimeZone("UTC"));

                            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd,hh:mm a");
                            Date tempDate  = sdf2.parse(sdf21.format(df.parse(dateStr)));
                            Log.i("time2",""+sdf2.format(df.parse(dateStr)));
                            long timeInMilliseconds = tempDate.getTime();

                            Date date = df.parse(dateStr);
                            df.setTimeZone(TimeZone.getDefault());
                            String formattedDate = df.format(date);
                            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd,HH:mm:ss");
                            Date start  = sdf1.parse(formattedDate);

                            SimpleDateFormat sdfOutPutToSend1 = new SimpleDateFormat("hh:mm a, EEEE-dd MMM, yyyy");
                            String csv = sdfOutPutToSend1.format(start);
                            Log.e("time1",""+csv);


                            cal.setTimeZone(TimeZone.getTimeZone("UTC"));
                            @SuppressLint("SimpleDateFormat") SimpleDateFormat outputFmt = new SimpleDateFormat("yyyy-MM-dd,hh:mm a");
                            outputFmt.setTimeZone(TimeZone.getTimeZone("UTC"));
                            Date time1 = sdf2.parse(outputFmt.format(cal.getTime()));
                            upcomingModal.setTimer(timeInMilliseconds-time1.getTime());
                            Log.e("time3",""+outputFmt.format(cal.getTime()));

                            upcomingModal.setSortingDate(timeInMilliseconds);
                            Date currentDate = sdf2.parse(outputFmt.format(cal.getTime()));

                            cal.setTimeZone(timeZoneId);
                            int dateDifference = (int) TimeUnit.HOURS.convert(tempDate.getTime()-currentDate.getTime(),TimeUnit.MILLISECONDS);
                            upcomingModal.setIsReschedule(dateDifference > 24);
                            upcomingModal.setTraineeIsReschedule(dateDifference > 12);

                            upcomingModal.setDateTime(csv);
                            upcomingModal.setBookingType(dataObj.optString("booking_type"));
                            upcomingModal.setTagUrl(dataObj.optString("booking_tag"));
                            upcomingModal.setImage(dataObj.optString("profile_image"));
                            upcomingModal.setIsRescheduleResp(dataObj.optString("reschedule_trainee_response"));
                            upcomingModal.setStudentID(dataObj.optString("student_id"));
                            upcomingModal.setTeacherID(dataObj.optString("teacher_id"));
                            upcomingModal.setDuration(dataObj.optString("hours"));
                            upcomingModal.setRate(dataObj.optString("payable_amount"));
                            upcomingModal.setRescheduleRequest(dataObj.optString("reschedule_trainee_response"));
                            upcomingModal.setRescheduleReason(dataObj.optString("reschedule_trainer_reason"));
                            upcomingModal.setBookingSlotID(dataObj.optString("booking_slots_id"));
                            upcomingModal.setCourseID(dataObj.optString("course_id"));
                            upcomingModal.setBanner(dataObj.optString("image"));

                            SimpleDateFormat sdf5 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                            sdf5.setTimeZone(TimeZone.getTimeZone("UTC"));

                            SimpleDateFormat sdf4 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
                            sdf4.setTimeZone(TimeZone.getDefault());
                            upcomingModal.setOldDate(sdf4.format(sdf5.parse(dataObj.optString("booking_date")+" "+dataObj.optString("start"))));

                            upcomingModalArrayList.add(upcomingModal);
                        }
                        if(upcomingModalArrayList.size()>0){
                            Collections.sort(upcomingModalArrayList, (upcomingModal, upcomingModal1) -> {
                                long date = upcomingModal.getSortingDate();
                                long date1 = upcomingModal1.getSortingDate();
                                if(date < date1) {
                                    return -1;
                                } else {
                                    return 1;
                                }
                            });
                        }
                        if(sharedPreferences.getString("user_type","").equalsIgnoreCase("2")) {
                            sessionsUpcomingAdapter=new SessionsUpcomingAdapter(context,upcomingModalArrayList,MyClassesFragment.this);
                            upcomingRecyclerView.setAdapter(sessionsUpcomingAdapter);
                        }else if(sharedPreferences.getString("user_type","").equalsIgnoreCase("3")) {
                            TrainerUpcomingBookingAdapter trainerUpcomingAdapter =new TrainerUpcomingBookingAdapter(context,upcomingModalArrayList,MyClassesFragment.this);
                            upcomingRecyclerView.setAdapter(trainerUpcomingAdapter);
                        }else {
                            upcomingAdapter=new MyClassUpcomingAdapter(context,upcomingModalArrayList,MyClassesFragment.this);
                            upcomingRecyclerView.setAdapter(upcomingAdapter);
                        }
                    }else {
                        upcomingRecyclerView.setVisibility(View.GONE);
                        noUpcomingText.setVisibility(View.VISIBLE);
                        noUpcomingText.setText(context.getResources().getString(R.string.noUpcomingBooking));
                    }
                }catch (Exception e){
                    progressDialog.cancel();
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();
                error.printStackTrace();
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        if(sharedPreferences.getString("user_type","").equalsIgnoreCase("2")) {
            header2.setVisibility(View.GONE);
            /** make them visible **/
            /*request.setVisibility(View.VISIBLE);
            requested.setVisibility(View.VISIBLE);*/
            //getRequestedList();
            /** **/
            header.setGravity(Gravity.START);
            header.setText(getResources().getString(R.string.groupSessions));
        }
        /*else if(sharedPreferences.getString("user_type","").equalsIgnoreCase("3"))
            header.setText("Bookings");*/

        ((MainActivity)context).showBottomMenu();
        context.registerReceiver(traineeCancelBooking,new IntentFilter("traineeCancelBooking"));
        context.registerReceiver(updateMyClassPast,new IntentFilter("updateMyClassPast"));
        context.registerReceiver(updateWorkshop,new IntentFilter("updateWorkshop"));
        context.registerReceiver(trainerWorkshopUpdate,new IntentFilter("trainerWorkshopUpdate"));
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    private void getRequestedList() {
        progressDialog.show();
        Map<String,String> param=new HashMap<>();
        param.put("user_id",sharedPreferences.getString("userID",""));

        Log.e("RequestedSessionsParam",""+param);
        new PostMethod(Api.RequestedSessions,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                progressDialog.dismiss();
                Log.e("RequestedSessionsResp",data);
                try {
                    JSONObject jsonObject=new JSONObject(data);
                    JSONArray dataArray = jsonObject.optJSONArray("data");
                    for (int i=0; i<dataArray.length(); i++){
                        RequestedModel requestedModel = new RequestedModel();
                        JSONObject object = dataArray.getJSONObject(i);
                        requestedModel.setID(object.optString("id"));
                        requestedModel.setAbout(object.optString("about"));
                        requestedModel.setWhen(object.optString("workshope_webinar_time"));
                        requestedModel.setLanguage(object.optString("language"));
                        requestedModel.setMaxPart(object.optString("max_participants"));
                        requestedModel.setPrice(object.optString("budget"));
                        requestedModel.setDetails(object.optString("details_about_your_request"));
                        requestedModel.setStatus(object.optString("status"));
                        requestedModelArrayList.add(requestedModel);
                    }
                    RequestedListAdapter requestedListAdapter = new RequestedListAdapter(context,requestedModelArrayList,MyClassesFragment.this);
                    requestedRecyclerView.setAdapter(requestedListAdapter);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.dismiss();
                error.printStackTrace();
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        context.unregisterReceiver(traineeCancelBooking);
        context.unregisterReceiver(trainerWorkshopUpdate);
    }

    BroadcastReceiver traineeCancelBooking= new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Upcoming(sharedPreferences.getString("user_type",""));
        }
    };
    BroadcastReceiver updateMyClassPast= new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Past(sharedPreferences.getString("user_type",""));
        }
    };
    BroadcastReceiver updateWorkshop= new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            TraineeSessions();
        }
    };

    BroadcastReceiver trainerWorkshopUpdate= new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Map<String,String> param=new HashMap<>();
            param.put("lang_token",sharedPreferences.getString("lang",""));
            param.put("trainer_id",sharedPreferences.getString("userID",""));
            param.put("time_zone",TimeZone.getDefault().getID());

            Log.e("TrainerPastSession","Param===>"+param);
            new PostMethod(Api.TrainerPastSessions,param,context).startPostMethod(new ResponseData() {
                @Override
                public void response(String data) {
                    Log.e("TrainerPastSession",data);
                    try {
                        JSONObject jsonObject = new JSONObject(data);

                        if (jsonObject.optBoolean("status")) {
                            pastModalArrayList1.clear();
                            pastRecyclerView1.setVisibility(View.VISIBLE);
                            noPastText1.setVisibility(View.GONE);
                            JSONArray jsonArray = jsonObject.optJSONArray("data");
                            for (int ar = 0; ar < jsonArray.length(); ar++) {
                                JSONObject dataObject=jsonArray.getJSONObject(ar);
                                PastModal pastModal=new PastModal();
                                pastModal.setID(dataObject.optString("id"));
                                pastModal.setImage(dataObject.optString("profile_image"));
                                pastModal.setBanner(dataObject.optString("image"));
                                pastModal.setName(dataObject.optString("teacher_name"));
                                pastModal.setTeacherID(dataObject.optString("teacher_id"));
                                pastModal.setBookingID(dataObject.optString("booking_id"));
                                pastModal.setSessionType(dataObject.optString("type"));
                                pastModal.setSessionName(dataObject.optString("about"));

                                pastModal.setButtonName(dataObject.optString("rating_to_rated_workshop"));
                                pastModal.setURL(dataObject.optString("download_url"));

                                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                                SimpleDateFormat sdfOutPutToSend = new SimpleDateFormat("hh:mm a");
                                String time="";
                                try {
                                    Date start  = sdf.parse(dataObject.optString("start_time"));
                                    String start_time = sdfOutPutToSend.format(start);

                                    time=start_time;
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                                String dateStr = dataObject.optString("workshope_time")+","+dataObject.optString("start_time");
                                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd,HH:mm:ss", new Locale(sharedPreferences.getString("lang",""),lang));
                                df.setTimeZone(TimeZone.getTimeZone("UTC"));

                                Date date = df.parse(dateStr);
                                df.setTimeZone(TimeZone.getDefault());
                                String formattedDate = df.format(date);
                                SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd,HH:mm:ss");

                                //SimpleDateFormat sdfOutPutToSend1 = new SimpleDateFormat("hh:mm a, EEEE-dd MMM, yyyy");
                                SimpleDateFormat sdfOutPutToSend1 = new SimpleDateFormat("hh:mm a");
                                Date start  = sdf1.parse(formattedDate);

                                pastModal.setDuration(dataObject.optString("hours"));
                                pastModal.setRate(dataObject.optString("ticket_cost"));

                                SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd,hh:mm a");
                                Date tempDate  = sdf2.parse(dataObject.optString("workshope_time")+","+time);
                                long timeInMilliseconds = tempDate.getTime();
                                Calendar cal = Calendar.getInstance();
                                cal.setTimeZone(TimeZone.getTimeZone("UTC"));
                                @SuppressLint("SimpleDateFormat") SimpleDateFormat outputFmt = new SimpleDateFormat("yyyy-MM-dd,HH:mm a");
                                outputFmt.setTimeZone(TimeZone.getTimeZone("UTC"));
                                pastModal.setSortingDate(timeInMilliseconds);

                                cal.setTimeZone(timeZoneId);
                                sdf1 = new SimpleDateFormat("yyyy-MM-dd");
                                sdfOutPutToSend1 = new SimpleDateFormat("MMMM-EEEE dd, yyyy");

                                JSONArray workshopDates=dataObject.optJSONArray("workshop_dates");
                                ArrayList<String> dateArray=new ArrayList<>();
                                for(int i=0;i<workshopDates.length();i++){
                                    try {
                                        JSONObject object=workshopDates.optJSONObject(i);
                                        String workshopDate = sdfOutPutToSend1.format(sdf1.parse(object.optString("workshope_time")));
                                        dateArray.add(workshopDate);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                }

                                String csv = sdfOutPutToSend1.format(start)+"\n"+android.text.TextUtils.join("\n", dateArray);

                                pastModal.setDateTime(csv);

                                pastModalArrayList1.add(pastModal);
                            }
                            if(pastModalArrayList1.size()>0){
                                Collections.sort(pastModalArrayList1, (pastModal, pastModal1) -> {
                                    long date = pastModal.getSortingDate();
                                    long date1 = pastModal1.getSortingDate();
                                    if(date > date1) {
                                        return -1;
                                    } else {
                                        return 1;
                                    }
                                });
                            }

                            TrainerPastAdapter trainerPastAdapter=new TrainerPastAdapter(context,pastModalArrayList1);
                            pastRecyclerView1.setAdapter(trainerPastAdapter);
                            pastRecyclerView1.scrollToPosition(intent.getIntExtra("pos",0));
                        }else {
                            pastRecyclerView1.setVisibility(View.GONE);
                            noPastText1.setVisibility(View.VISIBLE);
                            noPastText1.setText(context.getResources().getString(R.string.noPastSession));
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }

                @Override
                public void error(VolleyError error) {
                    error.printStackTrace();
                }
            });
        }
    };

    public void RemoveOneToOne(String bookingId){
        Map<String, String> param = new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));
        param.put("booking_slots_id",bookingId);

        Log.e("RemoveOneToOne",""+param);

        new PostMethod(Api.RemoveExpiredBookingOneToOne,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                Log.e("OnetoOneResp",data);
            }

            @Override
            public void error(VolleyError error) {
                error.printStackTrace();
            }
        });
    }
    public void RemoveGroupSession(String booked_date){
        Map<String, String> param = new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));
        param.put("booked_date",booked_date);

        Log.e("RemoveGroup",""+param);

        new PostMethod(Api.RemoveExpiredBookingWorkshop,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                Log.e("GroupSession",data);
            }

            @Override
            public void error(VolleyError error) {
                error.printStackTrace();
            }
        });
    }
}
