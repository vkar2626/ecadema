package com.ecadema.fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.ecadema.adapter.BillingAdapter;
import com.ecadema.adapter.CreditAdapter;
import com.ecadema.adapter.SavedCardsBillingAdapter;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.PostMethod;
import com.ecadema.apiMethods.ResponseData;
import com.ecadema.app.MainActivity;
import com.ecadema.app.R;
import com.ecadema.modal.BillingModal;
import com.ecadema.modal.CreditModal;
import com.ecadema.modal.SavedCardModal;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

public class PaymentSettingFragment extends Fragment {

    Context context;
    SharedPreferences sharedPreferences;
    View root;
    ImageView close;

    RelativeLayout billingRL;
    LinearLayout billingLL,creditLL,creditHistoryLL;
    TextView billing,credit,refill,noBilling,creditAmount;
    Dialog dialog;
    RecyclerView billingRecyclerView,creditRecycler;
    BillingAdapter billingAdapter;
    ArrayList<BillingModal> billingModalArrayList=new ArrayList<>();

    CreditAdapter creditAdapter;
    ArrayList<CreditModal> creditModalArrayList = new ArrayList<>();
    int creditNumber;
    ProgressDialog progressDialog;
    CardView card;
    TextView text_home,cardDetails,billingLayout;
    RecyclerView recyclerView;
    ArrayList<SavedCardModal>savedCardModalArrayList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.payment_setting_frag, container, false);

        context=getActivity();
        sharedPreferences= PreferenceManager.getDefaultSharedPreferences(context);
        progressDialog = new ProgressDialog(context);

        billingLayout=root.findViewById(R.id.billingLayout);
        recyclerView=root.findViewById(R.id.recyclerView);
        cardDetails=root.findViewById(R.id.cardDetails);
        text_home=root.findViewById(R.id.text_home);
        card=root.findViewById(R.id.card);
        creditRecycler=root.findViewById(R.id.creditRecycler);
        billingRecyclerView=root.findViewById(R.id.billingRecyclerView);
        creditAmount=root.findViewById(R.id.creditAmount);
        billingRL=root.findViewById(R.id.billingRL);
        noBilling=root.findViewById(R.id.noBilling);
        refill=root.findViewById(R.id.refill);
        billingLL=root.findViewById(R.id.billingLL);
        creditLL=root.findViewById(R.id.creditLL);
        billing=root.findViewById(R.id.billing);
        credit=root.findViewById(R.id.credit);
        close=root.findViewById(R.id.close);
        creditHistoryLL=root.findViewById(R.id.creditHistoryLL);

        billingRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        creditRecycler.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));

        getAllSaveCards();
        if(getArguments().getString("open").equalsIgnoreCase("billing")){
            creditLL.setVisibility(View.GONE);
            billingRL.setVisibility(View.VISIBLE);
            cardDetails.setVisibility(View.VISIBLE);
            billingLayout.setTextColor(context.getResources().getColor(R.color.colorAccent));
        }
        else {
            cardDetails.setVisibility(View.INVISIBLE);
            creditHistoryLL.setVisibility(View.GONE);
            billingRL.setVisibility(View.GONE);
            creditLL.setVisibility(View.VISIBLE);
        }

        billingLayout.setOnClickListener(v->{
            if (billingLayout.getText().toString().equalsIgnoreCase(getResources().getString(R.string.billing))){
                billingRL.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
                billingLayout.setTextColor(context.getResources().getColor(R.color.colorAccent));
                cardDetails.setTextColor(context.getResources().getColor(R.color.eGray));
            }
        });

        cardDetails.setOnClickListener(v->{
            billingRL.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            billingLayout.setTextColor(context.getResources().getColor(R.color.eGray));
            cardDetails.setTextColor(context.getResources().getColor(R.color.colorAccent));
        });

        close.setOnClickListener(v-> getParentFragmentManager().popBackStackImmediate());
        credit.setOnClickListener(v->{
            billingRL.setVisibility(View.GONE);
            creditLL.setVisibility(View.VISIBLE);

            credit.setTextColor(Color.WHITE);
            credit.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));

            billing.setBackgroundColor(context.getResources().getColor(R.color.white));
            billing.setTextColor(context.getResources().getColor(R.color.dark_grey));
        });
        billing.setOnClickListener(v->{
            creditLL.setVisibility(View.GONE);
            billingRL.setVisibility(View.VISIBLE);

            billing.setTextColor(Color.WHITE);
            billing.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));

            credit.setBackgroundColor(context.getResources().getColor(R.color.white));
            credit.setTextColor(context.getResources().getColor(R.color.dark_grey));
        });

        refill.setOnClickListener(v->{
            creditNumber=1;
            AlertDialog.Builder builder=new AlertDialog.Builder(context);

            LayoutInflater inflater1 = getLayoutInflater();
            @SuppressLint("InflateParams") final View dialogView = inflater1.inflate(R.layout.purchase_credit, null);
            builder.setView(dialogView);
            dialog=builder.create();

            ImageView add,minus,close;
            EditText credit;
            TextView creditRate,purchase;

            close=dialogView.findViewById(R.id.close);
            add=dialogView.findViewById(R.id.add);
            minus=dialogView.findViewById(R.id.minus);
            credit=dialogView.findViewById(R.id.credit);
            creditRate=dialogView.findViewById(R.id.creditRate);
            purchase=dialogView.findViewById(R.id.purchase);

            credit.setText(""+creditNumber);
            add.setOnClickListener(v1->{
                if(creditNumber<100){
                    creditNumber++;
                    credit.setText(""+creditNumber);
                    creditRate.setText("$ "+(creditNumber*100));
                }
            });

            minus.setOnClickListener(v1->{
                if(creditNumber>1){
                    creditNumber--;
                    credit.setText(""+creditNumber);
                    creditRate.setText("$ "+(creditNumber*100));
                }
            });

            close.setOnClickListener(v1->dialog.dismiss());
            purchase.setOnClickListener(v1-> purchaseCredit(dialog,creditRate.getText().toString().replace("$ ","")));

            dialog.show();
        });
        return root;
    }

    private void getAllSaveCards() {
        Map<String,String> param = new HashMap<>();
        param.put("lang",sharedPreferences.getString("lang",""));
        param.put("user_id",sharedPreferences.getString("userID",""));

        Log.e("allCards",""+param);
        new PostMethod(Api.GetUserCardInfo,param,context).startPostMethod(
            new ResponseData() {
                @Override
                public void response(final String data) {
                    Log.e("saveInfo",data);
                    try {
                        savedCardModalArrayList.clear();
                        JSONObject jsonObject = new JSONObject(data);
                        if (jsonObject.optBoolean("status")){
                            JSONArray array = jsonObject.optJSONArray("data");
                            for (int ar=0 ; ar<array.length(); ar++){
                                SavedCardModal savedCardModal = new SavedCardModal();
                                JSONObject object = array.getJSONObject(ar);
                                savedCardModal.setLastFour(object.optString("card_number").substring(object.optString("card_number").length() - 4));
                                savedCardModal.setCardNumber(object.optString("card_number"));
                                savedCardModal.setMonth(object.optString("month"));
                                savedCardModal.setYear(object.optString("year"));
                                savedCardModal.setCVC(object.optString("cvc"));
                                savedCardModal.setBrand(object.optString("brand"));
                                savedCardModal.setId(object.optString("id"));
                                savedCardModalArrayList.add(savedCardModal);
                            }
                            SavedCardsBillingAdapter savedCardsAdapter = new SavedCardsBillingAdapter(savedCardModalArrayList,context);
                            recyclerView.setAdapter(savedCardsAdapter);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }

                @Override
                public void error(final VolleyError error) {
                    error.printStackTrace();
                }
            });
    }

    @Override
    public void onResume() {
        super.onResume();
        if(getArguments().getString("open").equalsIgnoreCase("billing")){
            getBillingDetails();
            billingLayout.setText(getResources().getString(R.string.billing));
        }
        else {
            getCredits();
            billingLayout.setText(getResources().getString(R.string.credit));

        }
        ((MainActivity)context).hideBottomMenu();
        /*if(!sharedPreferences.getString("user_type","").equalsIgnoreCase("2"))
            getCredits();
        else {
            //billing.setGravity(Gravity.START);
            card.setVisibility(View.GONE);
        }*/
    }

    private void purchaseCredit(Dialog dialog, String price) {
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();
        Map<String,String> param = new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));
        param.put("user_id",sharedPreferences.getString("userID",""));
        param.put("refill_amount",price);

        new PostMethod(Api.AddRefill,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                progressDialog.cancel();;
                Log.e("add refill response", data);
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    if(jsonObject.optBoolean("status")){
                        Bundle bundle=new Bundle();
                        bundle.putString("from","credit");
                        bundle.putString("price",price);
                        bundle.putString("trans_id",jsonObject.optString("trans_id"));

                        CheckOut checkOut=new CheckOut();
                        checkOut.setArguments(bundle);
                        ((MainActivity) context).addFragment2(checkOut, true, false, 0);
                        dialog.dismiss();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();;
                error.printStackTrace();
            }
        });
    }

    private void getCredits() {
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();
        Map<String,String> param=new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));
        param.put("user_id",sharedPreferences.getString("userID",""));

        new PostMethod(Api.EcademaCredit,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                progressDialog.cancel();;
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    if (jsonObject.optBoolean("status")){
                        creditModalArrayList.clear();
                        creditHistoryLL.setVisibility(View.VISIBLE);
                        creditAmount.setText("$ "+jsonObject.optString("credit_amount"));
                        JSONArray dataArray = jsonObject.optJSONArray("data");
                        for (int ar=0;ar<dataArray.length();ar++){
                            JSONObject dataObject= dataArray.getJSONObject(ar);
                            CreditModal creditModal=new CreditModal();

                            creditModal.setImage(dataObject.optString("images"));
                            creditModal.setCreditDebit(dataObject.optString("credit_debit"));
                            creditModal.setPrice(dataObject.optString("amount"));

                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

                            SimpleDateFormat sdfOutPutToSend = new SimpleDateFormat("dd-MM-yyyy\nhh:mm a");
                            sdfOutPutToSend.setTimeZone(TimeZone.getDefault());
                            String time="";
                            try {
                                Date start  = sdf.parse(dataObject.optString("date"));
                                String start_time = sdfOutPutToSend.format(start);

                                time=start_time;
                                //Log.e("time",time);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            creditModal.setDate(time);

                            creditModalArrayList.add(creditModal);
                        }
                        creditAdapter = new CreditAdapter(context,creditModalArrayList);
                        creditRecycler.setAdapter(creditAdapter);
                    }else {
                        creditHistoryLL.setVisibility(View.GONE);
                        creditAmount.setText("$ 0");
                    }

                }catch (Exception e){
                    progressDialog.cancel();;
                    e.printStackTrace();
                }
                Log.e("CreditData",data);
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();;
                error.printStackTrace();
            }
        });
    }

    private void getBillingDetails() {
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();
        Map<String,String> param=new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));

        String api="";
        if(sharedPreferences.getString("user_type","").equalsIgnoreCase("2")) {
            api=Api.CorporateBillingList;
            param.put("corporate_id",sharedPreferences.getString("userID",""));
        }else  if(sharedPreferences.getString("user_type","").equalsIgnoreCase("1")) {
            api=Api.TraineeBillingList;
            param.put("trainee_id",sharedPreferences.getString("userID",""));
        }else{
            api=Api.TrainerBillingList;
            param.put("trainer_id",sharedPreferences.getString("userID",""));
        }
        new PostMethod(api,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                progressDialog.cancel();;
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    if (jsonObject.optBoolean("status")){
                        billingModalArrayList.clear();
                        billingLL.setVisibility(View.VISIBLE);
                        noBilling.setVisibility(View.GONE);
                        JSONArray dataArray = jsonObject.optJSONArray("data");
                        for (int ar=0;ar<dataArray.length();ar++){
                            JSONObject dataObject= dataArray.getJSONObject(ar);
                            BillingModal billingModal=new BillingModal();

                            billingModal.setID(dataObject.optString("id"));
                            billingModal.setItem(dataObject.optString("session_name"));
                            billingModal.setPrice(dataObject.optString("payable_amount"));
                            billingModal.setDate(dataObject.optString("created_at"));
                            billingModal.setBookingCode(dataObject.optString("booking_code"));
                            billingModal.setBillingType(dataObject.optString("billing_type"));

                            billingModalArrayList.add(billingModal);
                        }
                        billingAdapter = new BillingAdapter(context,billingModalArrayList);
                        billingRecyclerView.setAdapter(billingAdapter);
                    }else {
                        noBilling.setVisibility(View.VISIBLE);
                        billingLL.setVisibility(View.GONE);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                Log.e("BillingData",data);
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();;
                error.printStackTrace();
            }
        });
    }
}
