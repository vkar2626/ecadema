package com.ecadema.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SwitchCompat;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.android.volley.VolleyError;
import com.ecadema.EmojiFilter1;
import com.ecadema.app.LoginActivity;
import com.ecadema.app.MainActivity;
import com.ecadema.app.R;
import com.ecadema.app.SplashActivity;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.PostMethod;
import com.ecadema.apiMethods.ResponseData;
import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class SettingsFragment extends Fragment implements CompoundButton.OnCheckedChangeListener {
    Context context;
    SharedPreferences sharedPreferences;

    ImageView close;
    LinearLayout languageLayout,passwordLayout,notificationLayout,myProfile,myTrainers,groupSessions,terms_conditions,privacyPolicy,logout,traineeOptions,
            teamMates,earningLayout,myEarnings,paymentSetting,myAvailability,creditSetting,myPost,support,dashboard,myCompany;

    TextView logoutText;
    CardView deactivateAccount;
    RadioButton radioenglish,radioarabic;
    SharedPreferences.Editor editor;

    String API="",message_notification="0",session_reminders="0",other_notification="0";
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        context = getContext();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        setAppLocale1(sharedPreferences.getString("lang", ""));

        View root = inflater.inflate(R.layout.activity_settings, container, false);

        editor=sharedPreferences.edit();

        close=root.findViewById(R.id.close);

        myCompany=root.findViewById(R.id.myCompany);
        dashboard=root.findViewById(R.id.dashboard);

        /** make them visible**/
        dashboard.setVisibility(View.GONE);
        myCompany.setVisibility(View.GONE);
        /** make them visible**/

        support=root.findViewById(R.id.support);
        myPost=root.findViewById(R.id.myPost);
        myAvailability=root.findViewById(R.id.myAvailability);
        creditSetting=root.findViewById(R.id.creditSetting);
        paymentSetting=root.findViewById(R.id.paymentSetting);
        earningLayout=root.findViewById(R.id.earningLayout);
        myEarnings=root.findViewById(R.id.myEarnings);
        logoutText=root.findViewById(R.id.logoutText);
        teamMates=root.findViewById(R.id.teamMates);
        traineeOptions=root.findViewById(R.id.traineeOptions);
        deactivateAccount=root.findViewById(R.id.deactivateAccount);
        languageLayout=root.findViewById(R.id.languageLayout);
        passwordLayout=root.findViewById(R.id.passwordLayout);
        notificationLayout=root.findViewById(R.id.notificationLayout);
        myProfile=root.findViewById(R.id.myProfile);
        myTrainers=root.findViewById(R.id.myTrainers);
        terms_conditions=root.findViewById(R.id.terms_conditions);
        groupSessions=root.findViewById(R.id.groupSessions);
        privacyPolicy=root.findViewById(R.id.privacyPolicy);
        logout=root.findViewById(R.id.logout);
        ((MainActivity)context).hideBottomMenu();

        close.setOnClickListener(v-> {
            ((MainActivity)context).onBackPressed();
        });

        languageLayout.setOnClickListener(v-> {
            androidx.appcompat.app.AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
            LayoutInflater inflater1 =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            @SuppressLint("InflateParams") final View dialogView =
                    inflater1.inflate(R.layout.language_dialog, null);
            alertDialog.setView(dialogView);
            alertDialog.setCancelable(false);
            Dialog dialog=alertDialog.create();
            ImageView close=dialogView.findViewById(R.id.close);
            RadioGroup radioGroup=dialogView.findViewById(R.id.radioGroup);
            CardView update=dialogView.findViewById(R.id.update);

            radioenglish=dialogView.findViewById(R.id.radioenglish);
            radioarabic=dialogView.findViewById(R.id.radioarabic);

            update.setOnClickListener(v1->{
                if (radioGroup.getCheckedRadioButtonId()==-1){
                    Toast.makeText(getActivity(),getResources().getString(R.string.selectLanguages),Toast.LENGTH_LONG).show();
                }
                else {
                    if (radioenglish.isChecked()){
                        setAppLocale("en");
                        editor.putString("lang","en");
                    }
                    else{
                        setAppLocale("ar");
                        editor.putString("lang","ar");
                    }
                    dialog.dismiss();
                }

                editor.apply();

            });

            close.setOnClickListener(v1->dialog.dismiss());
            dialog.create();
            dialog.show();
        });
        passwordLayout.setOnClickListener(v-> {
            androidx.appcompat.app.AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
            LayoutInflater inflater1 =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            @SuppressLint("InflateParams") final View dialogView =
                    inflater1.inflate(R.layout.password_dialog, null);
            alertDialog.setView(dialogView);
            alertDialog.setCancelable(false);
            Dialog dialog=alertDialog.create();
            ImageView close=dialogView.findViewById(R.id.close);
            CardView update=dialogView.findViewById(R.id.update);
            EditText oldPassword,newPassword,reEnterPassword;

            oldPassword=dialogView.findViewById(R.id.oldPassword);
            newPassword=dialogView.findViewById(R.id.newPassword);
            reEnterPassword=dialogView.findViewById(R.id.reEnterPassword);

            oldPassword.setFilters(EmojiFilter1.getFilter());
            newPassword.setFilters(EmojiFilter1.getFilter());
            reEnterPassword.setFilters(EmojiFilter1.getFilter());

            update.setOnClickListener(v1->{
                if(oldPassword.getText().toString().trim().matches("")){
                    oldPassword.setError(context.getResources().getString(R.string.enterOldPass));
                    return;
                }
                if(newPassword.getText().toString().trim().matches("")){
                    newPassword.setError(context.getResources().getString(R.string.enterNewPass));
                    return;
                }
                if(reEnterPassword.getText().toString().trim().matches("")){
                    reEnterPassword.setError(context.getResources().getString(R.string.confirmPass));
                    return;
                }
                if(!reEnterPassword.getText().toString().trim().equals(newPassword.getText().toString().trim())){
                    reEnterPassword.setError(context.getResources().getString(R.string.passwordNotMatched));
                    return;
                }
                updatePassword(oldPassword.getText().toString(),newPassword.getText().toString(),reEnterPassword.getText().toString(),dialog,newPassword,oldPassword);
            });

            close.setOnClickListener(v1->dialog.dismiss());
            dialog.create();
            dialog.show();
        });
        notificationLayout.setOnClickListener(v-> {
            androidx.appcompat.app.AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
            LayoutInflater inflater1 = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            @SuppressLint("InflateParams") final View dialogView = inflater1.inflate(R.layout.notification_dialog, null);
            alertDialog.setView(dialogView);
            alertDialog.setCancelable(false);
            Dialog dialog=alertDialog.create();
            ImageView close=dialogView.findViewById(R.id.close);
            CardView update=dialogView.findViewById(R.id.update);
            SwitchCompat msgNotification,sessionReminders,otherNotifications;

            msgNotification = dialogView.findViewById(R.id.msgNotification);
            sessionReminders = dialogView.findViewById(R.id.sessionReminders);
            otherNotifications = dialogView.findViewById(R.id.otherNotifications);

            msgNotification.setOnCheckedChangeListener(this);
            sessionReminders.setOnCheckedChangeListener(this);
            otherNotifications.setOnCheckedChangeListener(this);

            if(message_notification.equalsIgnoreCase("1"))
                msgNotification.setChecked(true);

            if(session_reminders.equalsIgnoreCase("1"))
                sessionReminders.setChecked(true);

            if(other_notification.equalsIgnoreCase("1"))
                otherNotifications.setChecked(true);

            update.setOnClickListener(v1->{
                updateNotification(dialog);
                //dialog.dismiss();
            });

            close.setOnClickListener(v1->dialog.dismiss());
            dialog.create();
            dialog.show();
        });
        dashboard.setOnClickListener(v-> {
            ((MainActivity)context).addFragment2(new DashboardFragment(),true,false,0);
        });
        myCompany.setOnClickListener(v-> {
            ((MainActivity)context).addFragment2(new CompanyDetailFragment(),true,false,0);
        });
        teamMates.setOnClickListener(v-> {
            ((MainActivity)context).addFragment2(new TeamMateFragment(),true,false,0);
        });
        myPost.setOnClickListener(v-> {
            ((MainActivity)context).addFragment2(new MyPostFragment(),true,false,0);
        });
        myProfile.setOnClickListener(v-> {
            if(sharedPreferences.getString("userID","").equalsIgnoreCase("-1")){
                Intent intent=new Intent(context, LoginActivity.class);
                startActivity(intent);
                return;
            }
            if(sharedPreferences.getString("user_type","").equalsIgnoreCase("1"))
                ((MainActivity)context).addFragment2(new MyProfileFragment(),true,false,0);
            else  if(sharedPreferences.getString("user_type","").equalsIgnoreCase("2"))
                ((MainActivity)context).addFragment2(new MyCorporateProfileFragment(),true,false,0);
            else
                ((MainActivity)context).addFragment2(new TrainerProfileFragment(),true,false,0);
        });
        myTrainers.setOnClickListener(v-> {
            ((MainActivity)context).addFragment2(new MyTrainersFragment(),true,false,0);
        });
        myEarnings.setOnClickListener(v-> {
            ((MainActivity)context).addFragment2(new TrainerEarningFragment(),true,false,0);
        });
        terms_conditions.setOnClickListener(v-> {
            Uri uri = Uri.parse("https://ecadema.com/terms"); // missing 'http://' will cause crashed
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        });
        support.setOnClickListener(v-> {
            Uri uri = Uri.parse("https://support.ecadema.com/"); // missing 'http://' will cause crashed
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        });
        groupSessions.setOnClickListener(v-> {
           /* Intent intent=new Intent(context, MyGroupSessions.class);
            startActivity(intent);*/
            ((MainActivity)context).addFragment2(new MyGroupSessionsFragment(),true,false,0);

        });
        privacyPolicy.setOnClickListener(v-> {
            Uri uri = Uri.parse("https://ecadema.com/privacy"); // missing 'http://' will cause crashed
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        });
        logout.setOnClickListener(v-> {
            if(sharedPreferences.getString("userID","").equalsIgnoreCase("-1")){
                Intent intent=new Intent(context, LoginActivity.class);
                startActivity(intent);
                return;
            }

            androidx.appcompat.app.AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
            LayoutInflater inflater1 =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            @SuppressLint("InflateParams") final View dialogView =
                    inflater1.inflate(R.layout.logout_dialog, null);
            alertDialog.setView(dialogView);
            alertDialog.setCancelable(false);
            Dialog dialog=alertDialog.create();
            ImageView close=dialogView.findViewById(R.id.close);
            TextView cancel=dialogView.findViewById(R.id.cancel);
            CardView logout=dialogView.findViewById(R.id.logout);

            logout.setOnClickListener(v1->{
                ProgressDialog progressDialog = new ProgressDialog(context);
                progressDialog.setTitle(context.getResources().getString(R.string.processing));
                progressDialog.show();

                Map<String,String> param = new HashMap<>();
                param.put("user_id",sharedPreferences.getString("userID",""));
                param.put("lang_token",sharedPreferences.getString("lang",""));
                param.put("device_id",sharedPreferences.getString("device_id",""));

                new PostMethod(Api.LogOut,param,context).startPostMethod(new ResponseData() {
                    @Override
                    public void response(String data) {
                        try {
                            Log.e("logoutResp",data);
                            progressDialog.cancel();;
                            JSONObject jsonObject = new JSONObject(data);
                            if (jsonObject.optBoolean("status")){
                                try{
                                    GoogleSignInOptions gso = new GoogleSignInOptions.
                                            Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).
                                            build();

                                    GoogleSignInClient googleSignInClient= GoogleSignIn.getClient(context,gso);
                                    googleSignInClient.signOut();
                                }catch (Exception e){
                                    e.printStackTrace();
                                }

                                try{
                                    FacebookSdk.sdkInitialize(context);
                                    if (AccessToken.getCurrentAccessToken() != null) {
                                        LoginManager.getInstance().logOut();
                                        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/",
                                                null, HttpMethod.DELETE, response -> LoginManager.getInstance().logOut()).executeAsync();
                                    }
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                                sharedPreferences.edit().putString("userID","-1").apply();
                                sharedPreferences.edit().remove("user_type").apply();
                                sharedPreferences.edit().remove("profile_image").apply();
                                Toast.makeText(context, jsonObject.optJSONObject("message").optString("success"), Toast.LENGTH_SHORT).show();
                                Intent intent=new Intent(context,MainActivity.class);
                                context.startActivity(intent);
                                dialog.dismiss();
                                ((MainActivity) context).finish();
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void error(VolleyError error) {
                        progressDialog.cancel();;
                        error.printStackTrace();
                    }
                });
            });

            close.setOnClickListener(v1->dialog.dismiss());
            cancel.setOnClickListener(v1->dialog.dismiss());

            dialog.create();
            dialog.show();
        });
        deactivateAccount.setOnClickListener(v-> {
            androidx.appcompat.app.AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
            LayoutInflater inflater1 =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            @SuppressLint("InflateParams") final View dialogView =
                    inflater1.inflate(R.layout.deactivate_dialog, null);
            alertDialog.setView(dialogView);
            alertDialog.setCancelable(false);
            Dialog dialog=alertDialog.create();
            ImageView close=dialogView.findViewById(R.id.close);
            TextView cancel=dialogView.findViewById(R.id.cancel);
            EditText reason=dialogView.findViewById(R.id.reason);
            CardView deactivate=dialogView.findViewById(R.id.deactivate);

            deactivate.setOnClickListener(v1->{
                if (reason.getText().toString().trim().matches("")){
                    reason.setError(context.getResources().getString(R.string.fieldRequired));
                    return;
                }
                ProgressDialog progressDialog = new ProgressDialog(context);
                progressDialog.setTitle(context.getResources().getString(R.string.processing));
                progressDialog.show();

                if(sharedPreferences.getString("user_type","").equalsIgnoreCase("1"))
                    API = Api.StudentDeactivate;
                else  if(sharedPreferences.getString("user_type","").equalsIgnoreCase("2"))
                    API = Api.CorporateDeactive;
                else
                    API = Api.TeacherDeactivate;

                Map<String,String> param = new HashMap<>();
                param.put("user_id",sharedPreferences.getString("userID",""));
                param.put("lang_token",sharedPreferences.getString("lang",""));
                param.put("reason",reason.getText().toString());

                Log.e("API",API);
                new PostMethod(API,param,context).startPostMethod(new ResponseData() {
                    @Override
                    public void response(String data) {
                        try {
                            Log.e("deactivateResp",data);
                            progressDialog.cancel();;
                            JSONObject jsonObject = new JSONObject(data);
                            if (jsonObject.optBoolean("status")){
                                try{
                                    GoogleSignInOptions gso = new GoogleSignInOptions.
                                            Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).
                                            build();

                                    GoogleSignInClient googleSignInClient= GoogleSignIn.getClient(context,gso);
                                    googleSignInClient.signOut();
                                }catch (Exception e){
                                    e.printStackTrace();
                                }

                                try{
                                    FacebookSdk.sdkInitialize(context);
                                    if (AccessToken.getCurrentAccessToken() != null) {
                                        LoginManager.getInstance().logOut();
                                        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/",
                                                null, HttpMethod.DELETE, response -> LoginManager.getInstance().logOut()).executeAsync();
                                    }
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                                sharedPreferences.edit().putString("userID","-1").apply();
                                sharedPreferences.edit().remove("user_type").apply();
                                sharedPreferences.edit().remove("profile_image").apply();
                                Toast.makeText(context, jsonObject.optJSONObject("message").optString("success"), Toast.LENGTH_SHORT).show();
                                Intent intent=new Intent(context,MainActivity.class);
                                context.startActivity(intent);
                                dialog.dismiss();
                                ((MainActivity) context).finish();
                            }else
                                Toast.makeText(context, jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void error(VolleyError error) {
                        progressDialog.cancel();;
                        error.printStackTrace();
                    }
                });
                dialog.dismiss();
            });

            close.setOnClickListener(v1->dialog.dismiss());
            cancel.setOnClickListener(v1->dialog.dismiss());

            dialog.create();
            dialog.show();
        });

        paymentSetting.setOnClickListener(view -> {
            Bundle bundle=new Bundle();
            bundle.putString("open","billing");
            PaymentSettingFragment paymentSettingFragment=new PaymentSettingFragment();
            paymentSettingFragment.setArguments(bundle);
            ((MainActivity)context).addFragment2(paymentSettingFragment,true,false,0);
        });
        creditSetting.setOnClickListener(view -> {
            Bundle bundle=new Bundle();
            bundle.putString("open","credit");
            PaymentSettingFragment paymentSettingFragment=new PaymentSettingFragment();
            paymentSettingFragment.setArguments(bundle);
            ((MainActivity)context).addFragment2(paymentSettingFragment,true,false,0);
        });
        myAvailability.setOnClickListener(view -> {
            ((MainActivity)context).addFragment2(new MyAvailabilityFragment(),true,false,0);
        });
        getNotificationStatus();
        return root;
    }

    private void updateNotification(Dialog dialog) {
        ProgressDialog progressDialog=new ProgressDialog(context);
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();

        Map<String,String> param=new HashMap<>();
        param.put("message_notification",message_notification);
        param.put("session_reminders",session_reminders);
        param.put("other_notification",other_notification);
        param.put("user_id",sharedPreferences.getString("userID",""));
        param.put("lang_token",sharedPreferences.getString("lang",""));

        Log.e("notification",""+param);
        new PostMethod(Api.NotificationStatus,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                progressDialog.cancel();;
                Log.e("Notification Resp",data);
                try {
                    JSONObject jsonObject=new JSONObject(data);
                    if(jsonObject.optBoolean("status")){
                        Toast.makeText(context, jsonObject.optString("message"), Toast.LENGTH_LONG).show();
                        getNotificationStatus();
                        dialog.dismiss();
                    }else {
                        Toast.makeText(context, jsonObject.optString("message"), Toast.LENGTH_LONG).show();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();;
                error.printStackTrace();
            }
        });
    }

    private void getNotificationStatus() {
        Map<String,String> param=new HashMap<>();
        param.put("user_id",sharedPreferences.getString("userID",""));
        param.put("lang_token",sharedPreferences.getString("lang",""));

        Log.e("notificationStatus",""+param);
        new PostMethod(Api.UserNotificationStatus,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                Log.e("UserNotificationStatus",data);
                try {
                    JSONObject jsonObject=new JSONObject(data);
                    if(jsonObject.optBoolean("status")){
                        message_notification = jsonObject.optJSONObject("data").optString("message_notification");
                        session_reminders = jsonObject.optJSONObject("data").optString("session_reminders");
                        other_notification = jsonObject.optJSONObject("data").optString("other_notification");
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                error.printStackTrace();
            }
        });
    }


    private void setAppLocale(String LocaleCode)
    {
        Resources res=getResources();
        DisplayMetrics dr=res.getDisplayMetrics();
        Configuration configuration=res.getConfiguration();
        if (Build.VERSION.SDK_INT >=Build.VERSION_CODES.JELLY_BEAN_MR1){
            configuration.setLocale(new Locale(LocaleCode.toLowerCase()));
        }
        else
        {
            configuration.locale=new Locale(LocaleCode.toLowerCase());
        }
        res.updateConfiguration(configuration,dr);

        Intent intent=new Intent(getActivity(), SplashActivity.class);
        TaskStackBuilder.create(getActivity()).addNextIntentWithParentStack(intent).startActivities();
        getActivity().finish();
    }
    private void setAppLocale1(String LocaleCode)
    {
        Resources res=getResources();
        DisplayMetrics dr=res.getDisplayMetrics();
        Configuration configuration=res.getConfiguration();
        if (Build.VERSION.SDK_INT >=Build.VERSION_CODES.JELLY_BEAN_MR1){
            configuration.setLocale(new Locale(LocaleCode.toLowerCase()));
        }
        else
        {
            configuration.locale=new Locale(LocaleCode.toLowerCase());
        }
        res.updateConfiguration(configuration,dr);
    }
    private void updatePassword(String old, String newPass, String confirmPass, Dialog dialog, EditText newPassword, EditText oldPassword) {
        ProgressDialog progressDialog=new ProgressDialog(context);
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();

        Map<String,String> param=new HashMap<>();
        param.put("old_password",old);
        param.put("new_password",newPass);
        param.put("cpassword",confirmPass);
        param.put("user_id",sharedPreferences.getString("userID",""));
        param.put("lang_token",sharedPreferences.getString("lang",""));

        Log.e("PassParam",""+param);
        new PostMethod(Api.ChangePassword,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                progressDialog.cancel();;
                Log.e("paswResp",data);
                try {
                    JSONObject jsonObject=new JSONObject(data);
                    if(jsonObject.optBoolean("status")){
                        Toast.makeText(context, jsonObject.optJSONObject("message").optString("password"), Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                    }else {
                        JSONObject error=jsonObject.optJSONObject("message");
                        if(error.has("new_password"))
                            newPassword.setError(error.optString("new_password"));
                        if(error.has("password"))
                            oldPassword.setError(error.optString("password"));
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();;
                error.printStackTrace();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        if(sharedPreferences.getString("userID","").equalsIgnoreCase("-1")){
            traineeOptions.setVisibility(View.GONE);
            teamMates.setVisibility(View.GONE);
            deactivateAccount.setVisibility(View.GONE);
            passwordLayout.setVisibility(View.GONE);
            earningLayout.setVisibility(View.GONE);
            creditSetting.setVisibility(View.GONE);
            paymentSetting.setVisibility(View.GONE);
            myCompany.setVisibility(View.GONE);
            dashboard.setVisibility(View.GONE);
            myPost.setVisibility(View.GONE);
            logoutText.setText(getResources().getString(R.string.logIn));
            return;
        }
        if(sharedPreferences.getString("user_type","").equalsIgnoreCase("2")) {
            traineeOptions.setVisibility(View.GONE);
            earningLayout.setVisibility(View.GONE);
            myPost.setVisibility(View.GONE);
            myCompany.setVisibility(View.GONE);
            teamMates.setVisibility(View.VISIBLE);
            //dashboard.setVisibility(View.VISIBLE);
        }
        else if(sharedPreferences.getString("user_type","").equalsIgnoreCase("3")) {
            traineeOptions.setVisibility(View.GONE);
            myPost.setVisibility(View.VISIBLE);
            earningLayout.setVisibility(View.VISIBLE);
            //myCompany.setVisibility(View.VISIBLE);
        }
        else{
            myPost.setVisibility(View.GONE);
            traineeOptions.setVisibility(View.VISIBLE);
            earningLayout.setVisibility(View.GONE);
            teamMates.setVisibility(View.GONE);
            //myCompany.setVisibility(View.VISIBLE);
        }

        creditSetting.setVisibility(View.VISIBLE);
        deactivateAccount.setVisibility(View.VISIBLE);
        passwordLayout.setVisibility(View.VISIBLE);
        paymentSetting.setVisibility(View.VISIBLE);
        logoutText.setText(getResources().getString(R.string.logout));
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        switch (compoundButton.getId()) {
            case R.id.msgNotification:
                if(b)
                    message_notification="1";
                else
                    message_notification="0";

                break;
            case R.id.sessionReminders:
                if(b)
                    session_reminders="1";
                else
                    session_reminders="0";
                break;
            case R.id.otherNotifications:
                if(b)
                    other_notification="1";
                else
                    other_notification="0";
                break;
        }
    }
}
