package com.ecadema.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.ecadema.app.R;
import com.ecadema.adapter.SupportAdapter;
import com.ecadema.apiMethods.GetMethod;
import com.ecadema.apiMethods.ResponseData;
import com.ecadema.modal.SupportModal;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class SupportFragment extends Fragment {

    LinearLayout trainers,trainees,business;
    Context context;
    String api;
    RecyclerView recyclerView;
    AutoCompleteTextView search;
    ArrayList <SupportModal> supportModalArrayList = new ArrayList<SupportModal>();
    SupportAdapter supportAdapter;
    TextView noItem;
    SharedPreferences sharedPreferences;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        context= getContext();

        sharedPreferences= PreferenceManager.getDefaultSharedPreferences(context);

        Resources res=getResources();
        DisplayMetrics dr=res.getDisplayMetrics();
        Configuration configuration=res.getConfiguration();
        if (Build.VERSION.SDK_INT >=Build.VERSION_CODES.JELLY_BEAN_MR1){
            configuration.setLocale(new Locale(sharedPreferences.getString("lang","").toLowerCase()));
        }
        else
        {
            configuration.locale=new Locale(sharedPreferences.getString("lang","").toLowerCase());
        }
        res.updateConfiguration(configuration,dr);
        View root = inflater.inflate(R.layout.fragment_support, container, false);
        noItem=root.findViewById(R.id.noItem);
        search=root.findViewById(R.id.search);
        recyclerView=root.findViewById(R.id.recyclerView);
        trainers=root.findViewById(R.id.trainers);
        trainees=root.findViewById(R.id.trainees);
        business=root.findViewById(R.id.business);

        api="https://support.ecadema.com/wp-json/wp/v2/posts/?categories=1&per_page=100";

        trainers.setOnClickListener(v->{
            trainers.setBackground(context.getResources().getDrawable(R.drawable.bg_round_corner2));
            trainees.setBackground(null);
            business.setBackground(null);
            api="https://support.ecadema.com/wp-json/wp/v2/posts/?categories=1&per_page=100";
            getDetails();
        });
        trainees.setOnClickListener(v->{
            trainees.setBackground(context.getResources().getDrawable(R.drawable.bg_round_corner2));
            business.setBackground(null);
            trainers.setBackground(null);
            api="https://support.ecadema.com/wp-json/wp/v2/posts/?categories=2&per_page=100";
            getDetails();
        });
        business.setOnClickListener(v->{
            business.setBackground(context.getResources().getDrawable(R.drawable.bg_round_corner2));
            trainees.setBackground(null);
            trainers.setBackground(null);
            api="https://support.ecadema.com/wp-json/wp/v2/posts/?categories=18&per_page=100";
            getDetails();
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(context,RecyclerView.VERTICAL,false));
        getDetails();

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                //On text changed in Edit text start filtering the list
                if(charSequence.length()==0)
                    recyclerView.setVisibility(View.GONE);
                else
                    recyclerView.setVisibility(View.VISIBLE);

                if(supportAdapter!=null) {
                    supportAdapter.filter(charSequence.toString());
                    if(supportAdapter.getItemCount()==0) {
                        recyclerView.setVisibility(View.GONE);
                        noItem.setVisibility(View.VISIBLE);
                        return;
                    }
                    noItem.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {}
        });
        return root;
    }

    private void getDetails() {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();
        new GetMethod(api,context).startmethod(new ResponseData() {
            @Override
            public void response(String data) {
                progressDialog.cancel();;
                try {
                    JSONArray jsonArray = new JSONArray(data);
                    supportModalArrayList.clear();
                    for (int ar = 0; ar<jsonArray.length();ar++){
                        JSONObject jsonObject = jsonArray.getJSONObject(ar);
                        SupportModal supportModal = new SupportModal();
                        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd,hh:mm:ss");
                        SimpleDateFormat sdf1 = new SimpleDateFormat("EEE, dd MMM, yyyy");

                        Date tempDate  = sdf2.parse(jsonObject.optString("modified_gmt").replace("T",","));

                        supportModal.setDetails(jsonObject.optJSONObject("_links").optJSONArray("self").optJSONObject(0).optString("href"));
                        supportModal.setDate(sdf1.format(tempDate));
                        supportModal.setWrittenBy("ecadema");
                        supportModal.setTitle(jsonObject.optJSONObject("title").optString("rendered"));
                        supportModal.setContent(jsonObject.optJSONObject("content").optString("rendered")
                                .replace("<blockquote>","").replace("</blockquote>",""));

                        supportModalArrayList.add(supportModal);
                    }
                    supportAdapter= new SupportAdapter(context,supportModalArrayList);
                    recyclerView.setAdapter(supportAdapter);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();;
                error.printStackTrace();
            }
        });
    }

}
