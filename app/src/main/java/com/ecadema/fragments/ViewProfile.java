package com.ecadema.fragments;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.ecadema.adapter.ArticleAdapter;
import com.ecadema.app.ChatRoom;
import com.ecadema.app.LoginActivity;
import com.ecadema.app.MainActivity;
import com.ecadema.app.R;
import com.ecadema.adapter.CoursePriceAdapter;
import com.ecadema.adapter.EducationAdapter;
import com.ecadema.adapter.ReviewAdapter;
import com.ecadema.adapter.SessionAdapter;
import com.ecadema.adapter.SessionListAdapter;
import com.ecadema.adapter.SkillAdapter;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.PostMethod;
import com.ecadema.apiMethods.ResponseData;
import com.ecadema.modal.CommunityModal;
import com.ecadema.modal.CoursePriceModal;
import com.ecadema.modal.IconModal;
import com.ecadema.modal.ReviewModal;
import com.ecadema.modal.SessionModal;
import com.ecadema.modal.UpcomingModal;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class ViewProfile extends Fragment implements YouTubePlayer.OnInitializedListener{

    Context context;
    SharedPreferences sharedPreferences;
    CardView bookCard,watchMeCard,chatCard;
    String trainerID;
    ProgressDialog progressDialog;

    ImageView mentor;
    CircleImageView profile_pic;

    LinearLayout articleLayout;
    TextView name,location,languages,watchMe,chat,funCount,creativeCount,patientCount,organizedCount,articulateCount,expertCount,bookNow
            ,about, upcomingText, expText,eduText,reviewText;

    RecyclerView educationList,experienceList,sessionList,allSessions,priceRecyclerView,reviewRecyclerView;
    RatingBar ratings;

    ArrayList<String> educationArrayList=new ArrayList<>();
    ArrayList<String> expArrayList=new ArrayList<>();
    ArrayList<UpcomingModal> sessionArrayList=new ArrayList<>();
    ArrayList<ReviewModal> reviewModalArrayList=new ArrayList<>();
    ArrayList<SessionModal> sessionModalArrayList=new ArrayList<>();
    ArrayList<CoursePriceModal> coursePriceModalArrayList=new ArrayList<>();
    ArrayList<IconModal> iconModalArrayList = new ArrayList<>();
    ArrayList<String> selectedSkills= new ArrayList<>();
    RecyclerView skills;
    String courseID="",imageurl="",ticketPrice="",loadURL="",frameVideo;
    WebView webView;
    int webViewWidth,webViewHeight;

    public static final String DEVELOPER_KEY = "AIzaSyA0dSJ53Fvxhg7T4FSmgeKHkNuK5EM3Pnc";

    // YouTube video id
    public String YOUTUBE_VIDEO_CODE = "PTrNHW4BhSo";
    private static final int RECOVERY_DIALOG_REQUEST = 1;
    YouTubePlayer YPlayer;
    private YouTubePlayerSupportFragment youTubePlayerFragment;
    RelativeLayout videoView;
    NestedScrollView scrollView;
    JSONArray coursesArray;

    RelativeLayout controller;
    TextView next,prev;
    int reviewCounter = 1;
    int articleCounter = 1;
    private RecyclerView articleRecyclerView;
    private RelativeLayout articleController;
    private TextView nextArticle;
    private TextView prevArticle;
    ArrayList<CommunityModal>articleModalArrayList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_view_profile, container, false);

        context= getContext();
        sharedPreferences= PreferenceManager.getDefaultSharedPreferences(context);
        ((MainActivity)context).showBottomMenu();

        articleRecyclerView=root.findViewById(R.id.articleRecyclerView);
        articleLayout=root.findViewById(R.id.articleLayout);
        articleController=root.findViewById(R.id.articleController);
        nextArticle=root.findViewById(R.id.nextArticle);
        prevArticle=root.findViewById(R.id.prevArticle);

        controller=root.findViewById(R.id.controller);
        next=root.findViewById(R.id.next);
        prev=root.findViewById(R.id.prev);

        chatCard=root.findViewById(R.id.chatCard);
        mentor=root.findViewById(R.id.mentor);
        reviewText=root.findViewById(R.id.reviewText);
        upcomingText=root.findViewById(R.id.upcomingText);
        expText=root.findViewById(R.id.expText);
        eduText=root.findViewById(R.id.eduText);
        scrollView=root.findViewById(R.id.scrollView);
        watchMeCard=root.findViewById(R.id.watchMeCard);
        videoView=root.findViewById(R.id.videoView);
        skills=root.findViewById(R.id.skills);
        ratings=root.findViewById(R.id.ratings);
        profile_pic=root.findViewById(R.id.profile_pic);
        about=root.findViewById(R.id.about);
        name=root.findViewById(R.id.name);
        location=root.findViewById(R.id.location);
        languages=root.findViewById(R.id.languages);
        watchMe=root.findViewById(R.id.watchMe);
        chat=root.findViewById(R.id.chat);
        /*funCount=root.findViewById(R.id.funCount);
        creativeCount=root.findViewById(R.id.creativeCount);
        patientCount=root.findViewById(R.id.patientCount);
        organizedCount=root.findViewById(R.id.organizedCount);
        articulateCount=root.findViewById(R.id.articulateCount);
        expertCount=root.findViewById(R.id.expertCount);*/
        bookNow=root.findViewById(R.id.bookNow);
        educationList=root.findViewById(R.id.educationList);
        experienceList=root.findViewById(R.id.experienceList);
        sessionList=root.findViewById(R.id.sessionList);
        allSessions=root.findViewById(R.id.allSessions);
        priceRecyclerView=root.findViewById(R.id.priceRecyclerView);
        reviewRecyclerView=root.findViewById(R.id.reviewRecyclerView);

        bookCard=root.findViewById(R.id.bookCard);
        progressDialog=new ProgressDialog(context);

        skills.setLayoutManager(new GridLayoutManager(context,3));
        skills.setNestedScrollingEnabled(false);
        educationList.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        experienceList.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        sessionList.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        reviewRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        allSessions.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        priceRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));

        bookCard.setOnClickListener(v->{
            if(sharedPreferences.getString("userID","").equalsIgnoreCase("-1")){
                Intent intent=new Intent(context, LoginActivity.class);
                startActivity(intent);
                return;
            }
            Bundle bundle=new Bundle();
            bundle.putString("trainerID",trainerID);
            bundle.putString("name",name.getText().toString());
            bundle.putString("courseID",courseID);
            bundle.putString("imageurl",imageurl);
            bundle.putString("ticketPrice",ticketPrice);
            bundle.putString("from","booking");
            CalendarFragment calendarFragment =new CalendarFragment();
            calendarFragment.setArguments(bundle);
            FragmentManager fragmentManager=((MainActivity)context).getSupportFragmentManager();
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.replace(R.id.frag_container, calendarFragment, calendarFragment.getClass().getSimpleName());
            ft.addToBackStack(null);
            ft.commit();
        });
        trainerID=getArguments().getString("trainerID");

        if(trainerID.equalsIgnoreCase(sharedPreferences.getString("userID","")) &&
                sharedPreferences.getString("user_type","").equalsIgnoreCase("3"))
            bookCard.setVisibility(View.GONE);

        chat.setOnClickListener(v->{
            if (sharedPreferences.getString("userID","").equalsIgnoreCase("-1")){
                Intent intent = new Intent(context, LoginActivity.class);
                context.startActivity(intent);
                return;
            }

            Intent intent=new Intent(context, ChatRoom.class);
            intent.putExtra("chatID","");
            intent.putExtra("userName",name.getText().toString());
            intent.putExtra("receiverID",trainerID);
            intent.putExtra("userImage",imageurl);
            if(null!=coursesArray)
                intent.putExtra("courses",coursesArray.toString());
            context.startActivity(intent);
        });

        youTubePlayerFragment = YouTubePlayerSupportFragment.newInstance();
        FragmentTransaction transaction = getParentFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_fragment, youTubePlayerFragment);
        transaction.commit();

        ImageView close=root.findViewById(R.id.close);
        close.setOnClickListener(v1-> videoView.setVisibility(View.GONE));
        watchMe.setOnClickListener(v->{
            videoView.setVisibility(View.VISIBLE);
        });
        if(trainerID.equalsIgnoreCase(sharedPreferences.getString("userID","")))
            chatCard.setVisibility(View.GONE);
        getProfileDetail();

        next.setOnClickListener(v->{
            reviewCounter++;
            prev.setVisibility(View.VISIBLE);
            getReviews();
        });

        prev.setOnClickListener(v->{
            reviewCounter--;
            if(reviewCounter==1) {
                prev.setVisibility(View.GONE);
            }
            getReviews();
        });

        nextArticle.setOnClickListener(v->{
            articleCounter++;
            prevArticle.setVisibility(View.VISIBLE);
            getArticles();
        });

        prevArticle.setOnClickListener(v->{
            articleCounter--;
            if(articleCounter==1) {
                prevArticle.setVisibility(View.GONE);
            }
            getArticles();
        });
        return root;
    }

    private void getArticles() {
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();

        Map<String,String> param=new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));
        param.put("pages_no",""+articleCounter);
        param.put("teacher_id",trainerID);

        new PostMethod(Api.ArticleAjax,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                Log.e("Article Data",data);
                articleModalArrayList.clear();
                progressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(data);

                    for (int i=0;i<jsonObject.optJSONArray("data").length();i++){
                        JSONObject Object=jsonObject.optJSONArray("data").getJSONObject(i);
                        CommunityModal communityModal=new CommunityModal();
                        communityModal.setName(Object.optString("teacher_name"));
                        communityModal.setId(Object.optString("id"));
                        communityModal.setImage(Object.optString("profile_image"));
                        communityModal.setDate(Object.optString("create_date"));
                        communityModal.setTitle(Object.optString("title"));
                        communityModal.setDescription(Object.optString("short_description"));
                        articleModalArrayList.add(communityModal);

                        if(!Object.optString("show_next_button").equalsIgnoreCase("1")){
                            nextArticle.setVisibility(View.GONE);
                        }else
                            nextArticle.setVisibility(View.VISIBLE);
                    }
                    articleRecyclerView.setAdapter(new ArticleAdapter(context,articleModalArrayList));
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.dismiss();
            }
        });
    }

    private void getReviews() {
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();

        Map<String,String> param=new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));
        param.put("pages_no",""+reviewCounter);
        param.put("teacher_id",trainerID);

        new PostMethod(Api.ReviewAjax,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                Log.e("Review Data",data);
                reviewModalArrayList.clear();
                progressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(data);


                    for (int i=0;i<jsonObject.optJSONArray("data").length();i++){
                        JSONObject Object=jsonObject.optJSONArray("data").getJSONObject(i);
                        ReviewModal reviewModal=new ReviewModal();
                        reviewModal.setName(Object.optString("student_name"));
                        reviewModal.setImage(Object.optString("profile_image_url"));
                        reviewModal.setReview(Object.optString("review"));
                        reviewModalArrayList.add(reviewModal);

                        if(!Object.optString("show_next_button").equalsIgnoreCase("1")){
                            next.setVisibility(View.GONE);
                        }else
                            next.setVisibility(View.VISIBLE);
                    }
                    reviewRecyclerView.setAdapter(new ReviewAdapter(context,reviewModalArrayList));
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.dismiss();
            }
        });
    }

    private void getProfileDetail() {
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();
        Map<String,String> param=new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));
        param.put("trainer",trainerID);

        Log.e("Param",param.toString());
        new PostMethod(Api.ViewProfile,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                Log.e("ProfileResp",data);
                iconModalArrayList.clear();
                educationArrayList.clear();
                expArrayList.clear();
                reviewModalArrayList.clear();
                sessionArrayList.clear();
                sessionModalArrayList.clear();
                articleModalArrayList.clear();
                progressDialog.cancel();;
                try {
                    JSONObject jsonObject=new JSONObject(data);
                    if(jsonObject.optBoolean("status")){
                        scrollView.setVisibility(View.VISIBLE);
                        JSONObject dataObject=jsonObject.getJSONObject("data");

                        if(!dataObject.optString("youtube_url").equalsIgnoreCase("")){
                            loadURL=dataObject.optString("youtube_url");
                            Log.e("videoCode",URLUtil.guessFileName(loadURL, null, null));
                            youTubePlayerFragment.initialize(DEVELOPER_KEY, new YouTubePlayer.OnInitializedListener() {
                                @Override
                                public void onInitializationSuccess(YouTubePlayer.Provider arg0, YouTubePlayer youTubePlayer, boolean b) {
                                    if (!b) {
                                        YPlayer = youTubePlayer;
                                        YPlayer.setFullscreen(false);
                                        YPlayer.cueVideo(URLUtil.guessFileName(loadURL, null, null).replace(".bin",""));
                                        YPlayer.setFullscreenControlFlags(0);
                                        YPlayer.setOnFullscreenListener(new YouTubePlayer.OnFullscreenListener() {
                                            @Override
                                            public void onFullscreen(boolean b) {
                                                YPlayer.setFullscreen(false);
                                                Toast.makeText(getContext(), context.getResources().getString(R.string.landscapeModeNotSupported),
                                                        Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                        //YPlayer.play();
                                    }
                                }

                                @Override
                                public void onInitializationFailure(YouTubePlayer.Provider arg0, YouTubeInitializationResult arg1) {
                                    // TODO Auto-generated method stub

                                }
                            });
                        }
                        else {
                            watchMeCard.setVisibility(View.GONE);
                        }

                        Glide.with(context).load(dataObject.optString("profile_image_url")).into(profile_pic);
                        imageurl=dataObject.optString("profile_image_url");
                        name.setText(dataObject.optString("trainer_name"));
                        about.setText(Html.fromHtml(dataObject.optString("about")));
                        ratings.setRating(dataObject.optInt("rating"));

                        languages.setText(dataObject.optString("language"));
                        location.setText(dataObject.optString("country"));

                        if(dataObject.optString("is_mentor").equalsIgnoreCase("1"))
                            mentor.setVisibility(View.VISIBLE);

                        if(dataObject.optJSONArray("education").length()>0)
                        {
                            for (int i=0;i<dataObject.optJSONArray("education").length();i++){
                                JSONObject eduObject=dataObject.optJSONArray("education").getJSONObject(i);
                                educationArrayList.add(eduObject.optString("course"));
                            }
                            educationList.setAdapter(new EducationAdapter(context,educationArrayList));
                        }else
                            eduText.setVisibility(View.GONE);

                        if(dataObject.optJSONArray("experience").length()>0)
                        {
                            for (int i=0;i<dataObject.optJSONArray("experience").length();i++){
                                JSONObject Object=dataObject.optJSONArray("experience").getJSONObject(i);
                                expArrayList.add(Object.optString("designation").trim()+" "+getResources().getString(R.string.at)+" "+
                                        Object.optString("organisation"));
                            }
                            experienceList.setAdapter(new EducationAdapter(context,expArrayList));
                        }else
                            expText.setVisibility(View.GONE);


                        if(dataObject.optJSONArray("upcoming_group_sessions").length()>0)
                        {
                            for (int i=0;i<dataObject.optJSONArray("upcoming_group_sessions").length();i++){
                                JSONObject Object=dataObject.optJSONArray("upcoming_group_sessions").getJSONObject(i);
                                UpcomingModal upcomingModal = new UpcomingModal();
                                upcomingModal.setAbout(Object.optString("about"));
                                upcomingModal.setID(Object.optString("id"));
                                upcomingModal.setTeacherID(Object.optString("teacher_id"));
                                sessionArrayList.add(upcomingModal);
                            }
                            sessionList.setAdapter(new SessionListAdapter(context,sessionArrayList));
                        }else
                            upcomingText.setVisibility(View.GONE);

                        if(dataObject.optJSONArray("trainer_reviews").length()>0){
                            int ar=0;

                            if(dataObject.optJSONArray("trainer_reviews").length()>2) {
                                ar=2;
                                controller.setVisibility(View.VISIBLE);
                                next.setVisibility(View.VISIBLE);
                                prev.setVisibility(View.GONE);
                            }
                            else {
                                controller.setVisibility(View.GONE);
                                ar = dataObject.optJSONArray("trainer_reviews").length();
                            }

                            for (int i=0;i<ar;i++){
                                JSONObject Object=dataObject.optJSONArray("trainer_reviews").getJSONObject(i);
                                ReviewModal reviewModal=new ReviewModal();
                                reviewModal.setName(Object.optString("student_name"));
                                reviewModal.setImage(Object.optString("profile_image_url"));
                                reviewModal.setReview(Object.optString("review"));
                                reviewModalArrayList.add(reviewModal);
                            }
                            reviewRecyclerView.setAdapter(new ReviewAdapter(context,reviewModalArrayList));
                        }
                        else {
                            controller.setVisibility(View.GONE);
                            reviewText.setVisibility(View.GONE);
                        }

                        if(dataObject.optJSONArray("communityList").length()>0){
                            int ar=0;
                            articleLayout.setVisibility(View.VISIBLE);

                            if(dataObject.optJSONArray("communityList").length()>2) {
                                ar=2;
                                articleController.setVisibility(View.VISIBLE);
                                nextArticle.setVisibility(View.VISIBLE);
                                prevArticle.setVisibility(View.GONE);
                            }
                            else {
                                articleController.setVisibility(View.GONE);
                                ar = dataObject.optJSONArray("communityList").length();
                            }

                            for (int i=0;i<ar;i++){
                                JSONObject Object=dataObject.optJSONArray("communityList").getJSONObject(i);
                                CommunityModal communityModal=new CommunityModal();
                                communityModal.setName(Object.optString("teacher_name"));
                                communityModal.setId(Object.optString("id"));
                                communityModal.setImage(Object.optString("profile_image"));
                                communityModal.setDate(Object.optString("create_date"));
                                communityModal.setTitle(Object.optString("title"));
                                communityModal.setDescription(Object.optString("short_description"));
                                articleModalArrayList.add(communityModal);
                            }
                            articleRecyclerView.setAdapter(new ArticleAdapter(context,articleModalArrayList));
                        }
                        else {
                            articleLayout.setVisibility(View.GONE);
                        }


                        if(dataObject.optJSONArray("courses").length()>0){
                            coursesArray=dataObject.optJSONArray("courses");
                            for (int i=0;i<dataObject.optJSONArray("courses").length();i++){
                                JSONObject Object=dataObject.optJSONArray("courses").getJSONObject(i);
                                SessionModal sessionModal=new SessionModal();
                                sessionModal.setCourseId(Object.optString("id"));
                                sessionModal.setSubId(Object.optString("subject_id"));
                                sessionModal.setSubject(Object.optString("subject"));
                                sessionModal.setCoursePrices(""+Object.optJSONArray("course_price"));
                                sessionModalArrayList.add(sessionModal);

                                if(i==0){
                                    ticketPrice=Object.optJSONArray("course_price").optJSONObject(0).optString("price_per_hour");
                                    Intent intent=new Intent("courseSelected");
                                    intent.putExtra("coursePrice", ""+Object.optJSONArray("course_price"));
                                    intent.putExtra("courseID", Object.optString("id"));
                                    context.sendBroadcast(intent);
                                }
                            }
                            allSessions.setAdapter(new SessionAdapter(context,sessionModalArrayList));
                        }

                        if(dataObject.optJSONArray("skill").length()>0){
                            for(int ar=0;ar<dataObject.optJSONArray("skill").length();ar++){
                                JSONObject skillObj=dataObject.optJSONArray("skill").getJSONObject(ar);
                                IconModal iconModal = new IconModal();
                                iconModal.setCount(skillObj.optString("no"));
                                iconModal.setName(skillObj.optString("skill"));
                                iconModal.setId(skillObj.optString("id"));
                                iconModal.setImage(skillObj.optString("icon_url"));
                                Log.e("iconURL",skillObj.optString("icon_url"));
                                iconModalArrayList.add(iconModal);
                            }
                            skills.setAdapter(new SkillAdapter(context,iconModalArrayList));
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                error.printStackTrace();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        context.registerReceiver(courseSelected,new IntentFilter("courseSelected"));

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        context.unregisterReceiver(courseSelected);
    }

    BroadcastReceiver courseSelected=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                String coursePrice=intent.getStringExtra("coursePrice");
                Log.e("coursePrice",coursePrice);
                courseID=intent.getStringExtra("courseID");
                coursePriceModalArrayList.clear();
                JSONArray jsonArray=new JSONArray(coursePrice);
                for (int ar=0;ar<jsonArray.length();ar++){
                    JSONObject object=jsonArray.optJSONObject(ar);
                    CoursePriceModal coursePriceModal=new CoursePriceModal();
                    coursePriceModal.setHours(object.optString("hours")+" "+context.getResources().getString(R.string.hours));
                    coursePriceModal.setPrice("$ "+object.optString("price_per_hour")+" /"+context.getResources().getString(R.string.hour));

                    coursePriceModalArrayList.add(coursePriceModal);
                }
                priceRecyclerView.setAdapter(new CoursePriceAdapter(context,coursePriceModalArrayList));
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    };

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean b) {
        if (!b) {

            // loadVideo() will auto play video
            // Use cueVideo() method, if you don't want to play it automatically
            player.cueVideo(YOUTUBE_VIDEO_CODE);

            // Hiding player controls
            player.setPlayerStyle(YouTubePlayer.PlayerStyle.CHROMELESS);
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult errorReason) {
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(getActivity(), RECOVERY_DIALOG_REQUEST).show();
        } else {
            String errorMessage = errorReason.toString();
            Toast.makeText(getContext(), errorMessage, Toast.LENGTH_LONG).show();
        }
    }
}
