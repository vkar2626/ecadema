package com.ecadema.modal;

public class PostModal {

  private String id;
  private String name;
  private String image;
  private String title;
  private String date;
  private String shortDecription;
  private String view;
  private String likes;
  private String dislike;
  private String categoryName;
  private String tagIds;
  private String tagName;
  private String postImage;
  private String videoUrl;
  private String videoCode;
  private String shareUrl;

  public void setId(final String id) {
    this.id = id;
  }

  public String getId() {
    return id;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public void setImage(final String image) {
    this.image = image;
  }

  public String getImage() {
    return image;
  }

  public void setTitle(final String title) {
    this.title = title;
  }

  public String getTitle() {
    return title;
  }

  public void setDate(final String date) {
    this.date = date;
  }

  public String getDate() {
    return date;
  }

  public void setShortDecription(final String shortDecription) {
    this.shortDecription = shortDecription;
  }

  public String getShortDecription() {
    return shortDecription;
  }

  public void setView(final String view) {
    this.view = view;
  }

  public String getView() {
    return view;
  }

  public void setLikes(final String likes) {
    this.likes = likes;
  }

  public String getLikes() {
    return likes;
  }

  public void setDislike(final String dislike) {
    this.dislike = dislike;
  }

  public String getDislike() {
    return dislike;
  }

  public void setCategoryName(final String categoryName) {
    this.categoryName = categoryName;
  }

  public String getCategoryName() {
    return categoryName;
  }

    public void setTagIds(String tagIds) {
        this.tagIds = tagIds;
    }

    public String getTagIds() {
        return tagIds;
    }

  public void setTagName(String tagName) {
    this.tagName = tagName;
  }

  public String getTagName() {
    return tagName;
  }

  public void setPostImage(String postImage) {
    this.postImage = postImage;
  }

  public String getPostImage() {
    return postImage;
  }

  public void setVideoUrl(String videoUrl) {
    this.videoUrl = videoUrl;
  }

  public String getVideoUrl() {
    return videoUrl;
  }

    public void setVideoCode(String videoCode) {
        this.videoCode = videoCode;
    }

    public String getVideoCode() {
        return videoCode;
    }

    public void setShareUrl(String shareUrl) {
        this.shareUrl = shareUrl;
    }

    public String getShareUrl() {
        return shareUrl;
    }
}
