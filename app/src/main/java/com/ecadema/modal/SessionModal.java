package com.ecadema.modal;

import org.json.JSONArray;

public class SessionModal {
    private String courseId;
    private String subId;
    private String subject;
    private String coursePrices;

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setSubId(String subId) {
        this.subId = subId;
    }

    public String getSubId() {
        return subId;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSubject() {
        return subject;
    }

    public void setCoursePrices(String coursePrices) {
        this.coursePrices = coursePrices;
    }

    public String getCoursePrices() {
        return coursePrices;
    }
}
