package com.ecadema.modal;

public class TimeModal {

  private String id;
  private String time;

  public void setId(final String id) {
    this.id = id;
  }

  public String getId() {
    return id;
  }

  public void setTime(final String time) {
    this.time = time;
  }

  public String getTime() {
    return time;
  }
}
